﻿using System;
using System.Reflection.Metadata.Ecma335;
using FilmickDataAccess.Context;
using FilmickRepositories;
using FilmickServices.Importer;
using Microsoft.EntityFrameworkCore;

namespace SynchDailyFiles
{
    class Program
    {
        static void Main(string[] args)
        {
            //if (args.Length == 0)
            //    return;

            var options = new DbContextOptionsBuilder<FilmickContext>()
                .UseSqlServer("Server=.\\SQLEXPRESS;DataBase=FilmickDB;Trusted_Connection=true;")
                .Options;

            var context = new FilmickContext(options);

            //if (args[0] == "0")
            //{

            var importer = new KeywordImporter(new UnitOfWork(context))
            {
                ExpressModeIgnoreKeywordsAlreadySavedInDb = true
            };
            importer.Import();
            //}
        }
    }
}
