﻿using Microsoft.Extensions.Options;
using System;

namespace Common
{
    public class AppConfig
    { 
        public string ApiKey { get; set; }
        public string SecretAuthKey { get; set; }
        public int TokenExpireMinutes { get; set; }
        public int RefreshTokenExpireMinutes { get; set; }
        public string EmailSender { get; set; }
        public string EmailServer { get; set; }
        public string EmailUsername { get; set; }
        public string EmailPassword { get; set; }
        public AppConfig() { }

        public AppConfig(IOptions<AppConfig> appConfig)
        {
            this.ApiKey = appConfig.Value.ApiKey;
            this.SecretAuthKey = appConfig.Value.SecretAuthKey;
            this.TokenExpireMinutes = appConfig.Value.TokenExpireMinutes;
            this.RefreshTokenExpireMinutes = appConfig.Value.RefreshTokenExpireMinutes;

            this.EmailSender = appConfig.Value.EmailSender;
            this.EmailServer = appConfig.Value.EmailServer;
            this.EmailUsername = appConfig.Value.EmailUsername;
            this.EmailPassword = appConfig.Value.EmailPassword;
        }
    }
}
