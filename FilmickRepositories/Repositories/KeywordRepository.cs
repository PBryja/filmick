﻿using FilmickDataAccess.Context;
using FilmickDataAccess.Model;
using FilmickDataAccess.Model.Assocs;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static FilmickDataAccess.Model.Relationships.TagQuestionAssociation;

namespace FilmickRepositories.Repositories
{
    public class KeywordRepository : GenericRepository<Keyword>
    {
        public KeywordRepository(FilmickContext context) : base(context)
        {
        }

        public IEnumerable<Keyword> GetNonAssignedKeywords()
        {
            return context.Keywords.Where(x => x.TagKeyword == null);
        }

        public IEnumerable<Keyword> GetAssignedKeywords()
        {
            return context.Keywords.Include(x=>x.TagKeyword).Where(x => x.TagKeyword != null);
        }
    }
}
