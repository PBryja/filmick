﻿using FilmickDataAccess.Context;
using FilmickDataAccess.Model;
using FilmickDataAccess.Model.Assocs;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static FilmickDataAccess.Model.Relationships.TagQuestionAssociation;

namespace FilmickRepositories.Repositories
{
    public class TagQuestionRepository : GenericRepository<TagQuestion>
    {
        public TagQuestionRepository(FilmickContext context) : base(context)
        {
        }

        public IEnumerable<KeyValuePair<TagQuestion, int>> GetAvailableQuestionsAndCount(List<KeyValuePair<TagQuestion, GameQuestion.AnswerEnum>> answers, IEnumerable<Movie> movies, bool excludeChildIfNoAnswer)
        {
            List<TagQuestion> tagQuestionsOfMovies = movies.SelectMany(x => x.MovieTags).Select(x => x.TagQuestion).ToList();
            var availableQuestions = GetAvailableQuestions(answers, movies, excludeChildIfNoAnswer).ToList();

            return availableQuestions.ToDictionary(x => x, y => tagQuestionsOfMovies.Count(z => z == y));
        }

        public IEnumerable<TagQuestion> GetAvailableQuestions(List<KeyValuePair<TagQuestion, GameQuestion.AnswerEnum>> answers, IEnumerable<Movie> movies, bool excludeChildIfNoAnswer)
        {
            List<TagQuestion> tagQuestionsOfMovies = movies.SelectMany(x => x.MovieTags).Select(x => x.TagQuestion).ToList();
            List<TagQuestion> askedQuestions = answers.Select(x => x.Key).ToList();

            IEnumerable<TagQuestion> availableQuestions = context.TagQuestions
                .Where(x => tagQuestionsOfMovies.Contains(x))
                .Where(x => !askedQuestions.Contains(x))
                .AsEnumerable()
                .Where(x => !x.DependentTo.Any()
                            || (!x.DependentTo.Any(x => (int)x.Type == (int)TypeEnum.Parent_child) &&
                                !askedQuestions.Any(y => x.DependentTo.Select(z => z.Main).Contains(y)))
                            || (!x.DependentTo.Any(y =>
                                y.IsDependentExcludedByMain(answers.FirstOrDefault(x => x.Key == y.Main).Value,
                                    excludeChildIfNoAnswer)))
                );

            return availableQuestions;
        }

        public IEnumerable<TagQuestion> GetAllNonParentAndNonChildQuestions(IEnumerable<Movie> ofMovies)
        {
            return context.TagQuestions.Where(x => x.DependentTo.Any(y => y.Type == TypeEnum.Parent_child) == false
                                                   && x.Dependents.Any(y => y.Type == TypeEnum.Parent_child) == false
                                                   && x.MovieTags.Any(y => ofMovies.Contains(y.Movie)));
        }

        public IEnumerable<TagQuestion> GetAllMainParentQuestions(IEnumerable<Movie> ofMovies)
        {
            return context.TagQuestions.Where(x => x.DependentTo.Any(y => y.Type == TypeEnum.Parent_child) == false
                                                   && x.Dependents.Any(y => y.Type == TypeEnum.Parent_child) == true
                                                   && x.MovieTags.Any(y => ofMovies.Contains(y.Movie)));

        }

        public IEnumerable<KeyValuePair<TagQuestion, int>> GetAllNonParentAndNonChildQuestionsGroupAndCount(IEnumerable<Movie> ofMovies)
        {
            var allTagsOfMovies = ofMovies.SelectMany(x => x.MovieTags).Select(x => x.TagQuestion).ToList();

            return context.TagQuestions.Where(x => x.DependentTo.Any(y => y.Type == TypeEnum.Parent_child) == false
                                                   && x.Dependents.Any(y => y.Type == TypeEnum.Parent_child) == false
                                                   && allTagsOfMovies.Contains(x)
                                                   )
                                                    .ToDictionary(x => x, y => allTagsOfMovies.Count(z => z == y))
                                                    .OrderByDescending(x => x.Value);
        }

        public IEnumerable<KeyValuePair<TagQuestion, int>> GetAllMainParentQuestionsGroupAndCount(IEnumerable<Movie> ofMovies)
        {
            var allTagsOfMovies = ofMovies.SelectMany(x => x.MovieTags).Select(x => x.TagQuestion).ToList();

            return context.TagQuestions.Where(x => x.DependentTo.Any(y => y.Type == TypeEnum.Parent_child) == false
                                                   && x.Dependents.Any(y => y.Type == TypeEnum.Parent_child) == true
                                                   && allTagsOfMovies.Contains(x)
                                                   )
                                                    .ToDictionary(x => x, y => allTagsOfMovies.Count(z => z == y))
                                                    .OrderByDescending(x => x.Value);

        }

        public IEnumerable<TagQuestion> GetAllNonParentAndNonChildQuestions()
        {
            return context.TagQuestions.Where(x => x.DependentTo.Any(y => y.Type == TypeEnum.Parent_child) == false
                                                   && x.Dependents.Any(y => y.Type == TypeEnum.Parent_child) == false);
        }

        public IEnumerable<TagQuestion> GetAllMainParentQuestions()
        {
            return context.TagQuestions.Where(x => x.DependentTo.Any(y => y.Type == TypeEnum.Parent_child) == false
                                                   && x.Dependents.Any(y => y.Type == TypeEnum.Parent_child) == true);

        }

        public void UpdateMoviesWithTagQuestionConnectedByKeyword(int keywordId, List<int> newListWithMovieIds)
        {
            var tagFound = context.TagQuestions
                .Include(x => x.MovieTags)
                .SingleOrDefault(x => x.TagKeyword.KeywordId == keywordId);

            if (tagFound == default)
                return;

            var newMoviesList = context.Movies
                .Where(x => newListWithMovieIds.Contains(x.ApiId))
                .Select(x => new MovieTag() { Movie = x, MovieId = x.MovieId, TagQuestionId = tagFound.TagQuestionId });

            tagFound.MovieTags = newMoviesList.ToList();

            context.SaveChanges();
        }
    }
}
