﻿using FilmickDataAccess.Context;
using FilmickDataAccess.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FilmickRepositories.Repositories
{
    public class GameQuestionRepository : GenericRepository<GameQuestion>
    {
        public GameQuestionRepository(FilmickContext context) : base(context)
        {
        }

        public IEnumerable<GameQuestion> GetAllQuestions(Guid idGame, bool filterQuestions)
        {
            if (filterQuestions)
                return context.GameQuestions
                    .Include(x => x.FilterQuestion).ThenInclude(x => x.Dependents).ThenInclude(x => x.Dependent)
                    .Include(x => x.FilterQuestion).ThenInclude(x => x.DependentTo).ThenInclude(x => x.Main)
                    .Where(x => x.GameId == idGame && x.FilterQuestion != null);

            return context.GameQuestions
                .Include(x => x.TagQuestion).ThenInclude(x => x.Dependents).ThenInclude(x => x.Dependent)
                .Include(x => x.TagQuestion).ThenInclude(x => x.DependentTo).ThenInclude(x => x.Main)
                .Where(x => x.GameId == idGame && x.TagQuestion != null);
        }

        public IEnumerable<GameQuestion> GetAllQuestionsAnsweredYes(Guid idGame, bool filterQuestions)
        {
            return GetAllQuestionsAnsweredAs(idGame, filterQuestions, GameQuestion.AnswerEnum.Yes);
        }

        public IEnumerable<GameQuestion> GetAllQuestionsAnsweredNo(Guid idGame, bool filterQuestions)
        {
            return GetAllQuestionsAnsweredAs(idGame, filterQuestions, GameQuestion.AnswerEnum.No);
        }

        public IEnumerable<GameQuestion> GetAllQuestionsAnsweredIdk(Guid idGame, bool filterQuestions)
        {
            return GetAllQuestionsAnsweredAs(idGame, filterQuestions, GameQuestion.AnswerEnum.Idk);
        }

        private IEnumerable<GameQuestion> GetAllQuestionsAnsweredAs(Guid idGame, bool filterQuestions, GameQuestion.AnswerEnum answer)
        {
            if (filterQuestions)
                return context.GameQuestions.Where(x => x.GameId == idGame && x.FilterQuestion != null && x.Answer == answer);

            return context.GameQuestions.Where(x => x.GameId == idGame && x.TagQuestion != null && x.Answer == answer);
        }

        public bool IsMoreQuestionsThanOne(Guid idGame)
        {
            return context.GameQuestions.Count(x => x.GameId == idGame) > 1;
        }

        public GameQuestion GetTheNewestQuestionByGameId(Guid idGame)
        {
            GameQuestion gameQuestion = context.GameQuestions
                .Include(x => x.TagQuestion)
                .Include(x => x.FilterQuestion)
                .Where(x => x.GameId == idGame)
                .OrderBy(x => x.GameQuestionId)
                .LastOrDefault(x => x.GameId == idGame);

            return gameQuestion;
        }
    }
}
