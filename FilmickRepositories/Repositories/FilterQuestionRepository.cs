﻿using FilmickDataAccess.Context;
using FilmickDataAccess.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static FilmickDataAccess.Model.Relationships.FilterQuestionAssociation;

namespace FilmickRepositories.Repositories
{
    public class FilterQuestionRepository : GenericRepository<FilterQuestion>
    {
        public FilterQuestionRepository(FilmickContext context) : base(context)
        {
        }

        public IEnumerable<FilterQuestion> GetAllNonParentAndNonChildQuestions()
        {
            return context.FilterQuestions.Where(x => x.DependentTo.Any(y=>y.Type == TypeEnum.Parent_child) == false 
                                                   && x.Dependents.Any(y=>y.Type == TypeEnum.Parent_child) == false);
        }

        public IEnumerable<FilterQuestion> GetAllMainParentQuestions()
        {
            return context.FilterQuestions.Where(x => x.DependentTo.Any(y=>y.Type == TypeEnum.Parent_child) == false 
                                                   && x.Dependents.Any(y=>y.Type == TypeEnum.Parent_child) == true);

        }
    }
}
