﻿using FilmickDataAccess.Context;
using FilmickDataAccess.Model;
using FilmickRepositories.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FilmickRepositories 
{ 
    public interface IUnitOfWork
    {
        public void Save();
        public void Dispose();
        public FilmickContext Context { get; }

        public FilterQuestionRepository FilterQuestionRepository { get; }
        public TagQuestionRepository TagQuestionRepository { get; }
        public GenericRepository<Game> GameRepository { get; }
        public GameQuestionRepository GameQuestionRepository { get; }
        public GenericRepository<Movie> MovieRepository { get; }
        public KeywordRepository KeywordRepository { get; }


    }
}
