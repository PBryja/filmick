﻿using FilmickDataAccess.Context;
using FilmickDataAccess.Model;
using FilmickRepositories.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FilmickRepositories
{
    public class UnitOfWork : IUnitOfWork
    {
        public FilmickContext Context { get; private set; }
        private FilterQuestionRepository filterQuestionRepository;
        private TagQuestionRepository tagQuestionRepository;
        private GenericRepository<Game> gameRepository;
        private GameQuestionRepository gameQuestionRepository;
        private GenericRepository<Movie> movieRepository;
        private KeywordRepository keywordRepository;

        public UnitOfWork(FilmickContext context)
        {
            Context = context;
        }

        public TagQuestionRepository TagQuestionRepository => tagQuestionRepository ??= new TagQuestionRepository(Context);
        public FilterQuestionRepository FilterQuestionRepository => filterQuestionRepository ??= new FilterQuestionRepository(Context);
        public GenericRepository<Game> GameRepository => gameRepository ??= new GenericRepository<Game>(Context);
        public GenericRepository<Movie> MovieRepository => movieRepository ??= new GenericRepository<Movie>(Context);
        public GameQuestionRepository GameQuestionRepository => gameQuestionRepository ??= new GameQuestionRepository(Context);
        public KeywordRepository KeywordRepository => keywordRepository ??= new KeywordRepository(Context);


        public void Save()
        {
            Context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    Context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}

