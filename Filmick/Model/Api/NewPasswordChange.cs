﻿using System.ComponentModel.DataAnnotations;

namespace Filmick.Model.Api
{
    public class NewPasswordChange
    {
        [Required(ErrorMessage = "Nazwa użytkownika jest wymagana")]
        public string Username { get; set; }

        [Required(ErrorMessage = "Aktualne hasło jest wymagane.")]
        public string CurrentPassword { get; set; }

        [Required(ErrorMessage = "Nowe hasło jest wymagane.")]
        public string NewPassword { get; set; }
    }
}