﻿using System.ComponentModel.DataAnnotations;

namespace Filmick.Model.Api
{
    public class NewPasswordResetToken
    {
        [EmailAddress(ErrorMessage = "Niepoprawny format adresu email")]
        [Required(ErrorMessage = "Email jest wymagany")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Token jest wymagany")]
        public string Token { get; set; }

        [Required(ErrorMessage = "Nowe hasło jest wymagane.")]
        public string NewPassword { get; set; }
    }
}