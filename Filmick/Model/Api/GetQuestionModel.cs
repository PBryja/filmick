﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Filmick.Model.Api
{
    public class GetQuestionsModel
    {
        [Required(ErrorMessage = "Id gry jest wymagany")]
        public Guid GameId { get; set; }
        [Required(ErrorMessage = "Id pytania jest wymagany")]
        public int LastQuestionId { get; set; }
    }
}
