﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Filmick.Model.Api
{
    public class ConfirmEmailToken
    {
        [EmailAddress(ErrorMessage = "Niepoprawny format email")]
        [Required(ErrorMessage = "Email jest wymagany")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Token jest wymagany")]
        public string Token { get; set; }
    }
}
