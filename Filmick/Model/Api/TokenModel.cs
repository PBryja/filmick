﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Filmick.Model.Api
{
    public class TokenModel
    {
        [Required(ErrorMessage = "Token jest wymagany")]
        public string Token { get; set; }

        [Required(ErrorMessage = "Token odświeżający jest wymagany")]
        public string RefreshToken { get; set; }

    }
}
