﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Filmick.Model.Api
{
    public class AnswerModel
    {
        [Required(ErrorMessage = "Odpowiedź jest wymagana")]
        public int Answer { get; set; }

        [Required(ErrorMessage = "Id gry jest wymagany")]
        public Guid GameId { get; set; }
        [Required(ErrorMessage = "Id pytania jest wymagany")]
        public int QuestionId { get; set; }
    }
}
