﻿using System.ComponentModel.DataAnnotations;

namespace Filmick.Model.Api
{
    public class RegisterModel
    {
        [Required(ErrorMessage = "Nazwa użytkownika jest wymagana")]
        public string Username { get; set; }

        [Required(ErrorMessage = "Hasło jest wymagane")]
        public string Password { get; set; }

        [Compare("Password", ErrorMessage = "Hasło oraz powtórz hasło nie są identyczne.")]
        [Required(ErrorMessage = "Powtórz hasło jest wymagane")]
        public string RepeatPassword { get; set; }

        [Required(ErrorMessage = "Email jest wymagany")]
        public string Email { get; set; }
    }
}
