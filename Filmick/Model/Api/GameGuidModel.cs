﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Filmick.Model.Api
{
    public class GameGuidModel
    {
        [Required(ErrorMessage = "Id gry jest wymagany")]
        public Guid GameId { get; set; }
    }
}
