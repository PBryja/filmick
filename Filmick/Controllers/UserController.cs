﻿using System.Linq;
using System.Threading.Tasks;
using Filmick.Model.Api;
using FilmickDataAccess.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace Filmick.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly UserManager<ApplicationUser> userManager;
        private readonly RoleManager<IdentityRole> roleManager;

        public UserController(UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager)
        {
            this.userManager = userManager;
            this.roleManager = roleManager;
        }

        [Route("PostFirstAdmin")]
        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> PostFirstAdmin([FromBody] RegisterModel model)
        {
            if (userManager.GetUsersInRoleAsync("Admin").Result.Any())
                return BadRequest("Istnieje już użytkownik z rolą Admin.");

            if (!roleManager.Roles.Any(x => x.Name == "Admin"))
                return BadRequest("Rola Admin nie istnieje.");

            if (!ModelState.IsValid)
                return BadRequest("Parametry są nieprawidłowe.");

            var user = await userManager.FindByNameAsync(model.Username);
            if (user != null)
                return BadRequest("Użytkownik już istnieje.");

            var newUser = new ApplicationUser() { UserName = model.Username, Email = model.Email, EmailConfirmed = true };

            var userResult = await userManager.CreateAsync(newUser, model.Password);
            if (!userResult.Succeeded)
                return BadRequest(userResult.Errors.Select(x => x.Description));

            await userManager.AddToRoleAsync(newUser, "Admin");

            return Ok();
        }
    }
}
