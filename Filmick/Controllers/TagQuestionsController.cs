﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Filmick.Infrastructure.Hangfire;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FilmickDataAccess.Context;
using FilmickDataAccess.Model;
using FilmickDataAccess.ExtensionMethods;
using FilmickDataAccess.Model.Relationships;
using Hangfire;
using FilmickServices.Importer;
using Hangfire.States;

namespace Filmick.Controllers
{
    [Route("api/[controller]")]
    //[Authorize(Roles = "Admin")]
    [ApiController]
    public class TagQuestionsController : ControllerBase
    {
        private readonly FilmickContext _context;
        private readonly KeywordImporter keywordImporter;

        public TagQuestionsController(FilmickContext context, KeywordImporter keywordImporter)
        {
            _context = context;
            this.keywordImporter = keywordImporter;
        }

        // GET: api/TagQuestions
        [HttpGet]
        public async Task<ActionResult<IEnumerable<TagQuestion>>> GetTagQuestions()
        {
            return await _context.TagQuestions.ToListAsync();
        }

        // GET: api/TagQuestions/5
        [HttpGet("{id}")]
        public async Task<ActionResult<TagQuestion>> GetTagQuestion(int id)
        {
            var tagQuestion = await _context.TagQuestions.IncludeAll().SingleOrDefaultAsync(x => x.TagQuestionId == id);

            if (tagQuestion == default)
            {
                return NotFound();
            }

            return tagQuestion;
        }

        // PUT: api/TagQuestions/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTagQuestion(int id, TagQuestion tagQuestion)
        {
            if (id != tagQuestion.TagQuestionId)
            {
                return BadRequest();
            }

            var oldTagQuestion = await _context.TagQuestions.IncludeAll().SingleOrDefaultAsync(x => x.TagQuestionId == id);

            if (oldTagQuestion == default)
            {
                return BadRequest("TagQuestion nie istnieje");
            }

            oldTagQuestion.Dependents = tagQuestion.Dependents;
            oldTagQuestion.MovieTags = tagQuestion.MovieTags;
            oldTagQuestion.Texts = tagQuestion.Texts;
            oldTagQuestion.DependentTo = tagQuestion.DependentTo;
            oldTagQuestion.TagKeyword = tagQuestion.TagKeyword;

            _context.Entry(oldTagQuestion).CurrentValues.SetValues(tagQuestion);

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TagQuestionExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            var result = await GetTagQuestion(oldTagQuestion.TagQuestionId);

            return Ok(result);
        }

        // PUT: api/TagQuestions/SetAndSynch
        [Route("SetKeywordAndSynch/{tagId}/{keywordId}")]
        [HttpPut("{tagId}/{keywordId}")]
        public async Task<ActionResult<TagQuestion>> SetKeywordAndSynch(int tagId, int keywordId)
        {
            var tagQuestion = await _context.TagQuestions.Include(x => x.TagKeyword).SingleOrDefaultAsync(x => x.TagQuestionId == tagId);
            var keywordDb = await _context.Keywords.Include(x => x.TagKeyword).SingleOrDefaultAsync(x => x.KeywordId == keywordId);

            if (tagQuestion == default || keywordDb == default)
            {
                return BadRequest("Element(y) nie został odnaleziony");
            }

            if (keywordDb.TagKeyword != null && keywordDb.TagKeyword.TagQuestionId != tagQuestion.TagQuestionId)
            {
                return BadRequest("Ten keyword jest już przypisany do innego TagQuestion.");
            }

            tagQuestion.TagKeyword = new TagKeyword() { KeywordId = keywordDb.KeywordId };

            await _context.SaveChangesAsync();

            var jobId = new BackgroundJobClient().Create(() => keywordImporter.SynchKeywordMovies(keywordDb, true), new EnqueuedState(HangfireQueuePriority.c_low));

            return Ok("Hangfire task id : " + jobId);
        }

        // POST: api/TagQuestions
        [HttpPost]
        public async Task<ActionResult<TagQuestion>> PostTagQuestion(TagQuestion tagQuestion)
        {
            _context.TagQuestions.Add(tagQuestion);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetTagQuestion", new { id = tagQuestion.TagQuestionId }, tagQuestion);
        }

        // DELETE: api/TagQuestions/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<TagQuestion>> DeleteTagQuestion(int id)
        {
            var tagQuestion = await _context.TagQuestions.FindAsync(id);
            if (tagQuestion == null)
            {
                return NotFound();
            }

            _context.TagQuestions.Remove(tagQuestion);
            await _context.SaveChangesAsync();

            return tagQuestion;
        }

        private bool TagQuestionExists(int id)
        {
            return _context.TagQuestions.Any(e => e.TagQuestionId == id);
        }
    }
}
