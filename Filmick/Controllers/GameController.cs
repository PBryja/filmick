﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Security.Claims;
using AutoMapper;
using Filmick.Model.Api;
using FilmickDataAccess.Model;
using Microsoft.AspNetCore.Mvc;
using Filmick.Attributes;
using FilmickRepositories;
using FilmickServices.Game;
using Hangfire;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using Game = FilmickDataAccess.Model.Game;

namespace Filmick.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GameController : ControllerBase
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly GameManager gameManager;
        private readonly IMapper mapper;

        public GameController(IUnitOfWork unitOfWork, GameManager gameManager, IMapper mapper)
        {
            this.unitOfWork = unitOfWork;
            this.gameManager = gameManager;
            this.mapper = mapper;
        }

        [HeaderAuthorize]
        [HttpPost]
        [Route("newgame")]
        public ActionResult<Question> NewGame()
        {
            string userId = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;

            Game newGame = new Game() { ApplicationUserId = userId };

            unitOfWork.GameRepository.Insert(newGame);
            unitOfWork.Save();

            return Ok(gameManager.GetQuestion(newGame));
        }

        [HeaderAuthorize]
        [HttpPost]
        [Route("setanswer")]
        public ActionResult<Question> SetAnswer([FromBody]AnswerModel answerModel)
        {
            if (!Enum.IsDefined(typeof(GameQuestion.AnswerEnum), answerModel.Answer))
                return BadRequest(new Question() { StatusCode = Question.StatusEnum.Failed_NotCorrectAnswer });

            var answer = (GameQuestion.AnswerEnum)answerModel.Answer;
            string userId = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;

            var game = unitOfWork.GameRepository.GetByID(answerModel.GameId);

            var status = gameManager.IsGameAvailableAndValidForPlay(game, userId);

            if (status != Question.StatusEnum.Ok)
                return BadRequest(new Question() { StatusCode = status });

            var gameQuestion = unitOfWork.GameQuestionRepository.GetTheNewestQuestionByGameId(answerModel.GameId);
            if (IsGameQuestionValid(gameQuestion, answerModel.QuestionId) == false)
                return BadRequest(new Question() { StatusCode = Question.StatusEnum.Failed_NotCorrectQuestionId });

            gameQuestion.Answer = answer;
            unitOfWork.Save();

            return Ok(gameManager.GetQuestion(game));
        }

        [HeaderAuthorize]
        [HttpPost]
        [Route("lastquestion")]
        public ActionResult<Question> LastQuestion([FromBody]GetQuestionsModel questionModel)
        {
            string userId = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;

            var gameDb = unitOfWork.GameRepository.GetByID(questionModel.GameId);

            var status = gameManager.IsGameAvailableAndValidForPlay(gameDb, userId);

            if (status != Question.StatusEnum.Ok)
                return BadRequest(new Question() { StatusCode = status });

            var question = unitOfWork.GameQuestionRepository.GetTheNewestQuestionByGameId(gameDb.GameId);
            if (question == null || question.GameQuestionId <= 0)
            {
                return BadRequest(new Question() { StatusCode = Question.StatusEnum.Failed_NotCorrectGameId });
            }

            if (IsGameQuestionValid(question, questionModel.LastQuestionId) == false)
                return BadRequest(new Question() { StatusCode = Question.StatusEnum.Failed_NotCorrectQuestionId });

            return Ok(gameManager.GetQuestion(gameDb));
        }

        [HeaderAuthorize]
        [HttpPost]
        [Route("previousquestion")]
        public ActionResult<Question> PreviousQuestion([FromBody]GetQuestionsModel questionModel)
        {
            string userId = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;

            var gameDb = unitOfWork.GameRepository.GetByID(questionModel.GameId);

            var status = gameManager.IsGameAvailableAndValidForPlay(gameDb, userId);

            if (status != Question.StatusEnum.Ok)
                return BadRequest(new Question() { StatusCode = status });

            if (!unitOfWork.GameQuestionRepository.IsMoreQuestionsThanOne(questionModel.GameId))
                return BadRequest(new Question() { StatusCode = Question.StatusEnum.Failed_NotEnoughQuestionsToGetBack });

            var lastQuestion = unitOfWork.GameQuestionRepository.GetTheNewestQuestionByGameId(gameDb.GameId);

            if (lastQuestion == null || lastQuestion.GameQuestionId <= 0)
                return BadRequest(new Question() { StatusCode = Question.StatusEnum.Failed_NotCorrectGameId });

            if (IsGameQuestionValid(lastQuestion, questionModel.LastQuestionId) == false)
                return BadRequest(new Question() { StatusCode = Question.StatusEnum.Failed_NotCorrectQuestionId });

            unitOfWork.GameQuestionRepository.Delete(unitOfWork.GameQuestionRepository.GetTheNewestQuestionByGameId(gameDb.GameId));
            unitOfWork.Context.SaveChanges();

            lastQuestion = unitOfWork.GameQuestionRepository.GetTheNewestQuestionByGameId(gameDb.GameId);
            lastQuestion.Answer = GameQuestion.AnswerEnum.NoAnswer;

            unitOfWork.Context.SaveChanges();

            return Ok(new Question()
            {
                StatusCode = Question.StatusEnum.Ok,
                IsAvailablePreviousQuestion = unitOfWork.Context.GameQuestions.Count(x => x.GameId == gameDb.GameId) > 1
            }.SetProperties(gameDb.GameId.ToString(), lastQuestion));
        }

        [Authorize]
        [HttpGet]
        [Route("getgames")]
        public ActionResult<List<Game>> GetGames()
        {
            string userId = User.FindFirst(ClaimTypes.NameIdentifier).Value;

            var games = unitOfWork.Context.Games.Include(x=>x.GameQuestions).Where(x => x.ApplicationUserId == userId);

            return Ok(games.ToList());
        }

        [HttpGet("{id}")]
        [Route("getfilteredmovies/{id}")]
        public ActionResult<List<MovieVM>> GetFilteredMovies(Guid id)
        {
            var gameDb = unitOfWork.GameRepository.GetByID(id);

            if (gameDb == null)
                return BadRequest("Gra nie istnieje");

            List<MovieVM> result;

            try
            {
                result = mapper.Map<List<Movie>, List<MovieVM>>(gameManager.GetFilteredMovies(gameDb));
            }
            catch (CustomAttributeFormatException customException)
            {
                return BadRequest(customException.Message);
            }
            catch (Exception e)
            {
                BackgroundJob.Enqueue(
                    () => Console.WriteLine("gameManager.GetFilteredMovies" + e.Message));

                return BadRequest("Wystąpił nieoczekiwany błąd");
            }

            return Ok(result);
        }

        [HttpGet("{id}")]
        [Route("getproposedmovies/{id}")]
        public ActionResult<List<MovieVM>> GetProposedMovies(Guid id)
        {
            var gameDb = unitOfWork.GameRepository.GetByID(id);

            if (gameDb == null)
                return BadRequest("Gra nie istnieje");

            List<MovieVM> result;

            try
            {
                result = mapper.Map<List<Movie>, List<MovieVM>>(gameManager.GetProposedMovies(gameDb));
            }
            catch (CustomAttributeFormatException customException)
            {
                return BadRequest(customException.Message);
            }
            catch (Exception e)
            {
                BackgroundJob.Enqueue(
                    () => Console.WriteLine("gameManager.GetProposedMovies" + e.Message));

                return BadRequest("Wystąpił nieoczekiwany błąd");
            }

            return Ok(result);
        }

        private bool IsGameQuestionValid(GameQuestion gameQuestion, int idFromAnswer)
        {
            if (gameQuestion == null || gameQuestion.GameQuestionId <= 0 || gameQuestion.GameQuestionId != idFromAnswer)
            {
                return false;
            }

            return true;
        }
    }
}
