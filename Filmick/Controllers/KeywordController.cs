﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Filmick.Infrastructure.Hangfire;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FilmickDataAccess.Model;
using Microsoft.AspNetCore.Authorization;
using FilmickRepositories;
using Hangfire;
using Hangfire.States;
using FilmickServices.Importer;

namespace Filmick.Controllers
{
    [Route("api/[controller]")]
    [Authorize(Roles = "Admin")]
    [ApiController]
    public class KeywordController : ControllerBase
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly KeywordImporter keywordImporter;

        public KeywordController(IUnitOfWork unitOfWork, KeywordImporter keywordImporter)
        {
            this.unitOfWork = unitOfWork;
            this.keywordImporter = keywordImporter;
        }

        // POST: api/Keyword/SynchAllTagsWithKeywords
        [Route("SynchAllTagsWithKeywords")]
        [HttpPost]
        public ActionResult<TagQuestion> SynchAllTagsWithKeywords()
        {
            var tags = unitOfWork.KeywordRepository.GetAssignedKeywords().ToList();

            var jobId = new BackgroundJobClient().Create(() => keywordImporter.SynchKeywordMovies(tags, true), new EnqueuedState(HangfireQueuePriority.c_low));

            return Ok("Hangfire task id : " + jobId);
        }

        // POST: api/Keyword/ImportAndSynchAllKeywords
        [Route("ImportAndSynchAllKeywords")]
        [HttpPost]
        public ActionResult<TagQuestion> ImportAndSynchAllKeywords()
        {
            var jobId = new BackgroundJobClient().Create(() => keywordImporter.Import(), new EnqueuedState(HangfireQueuePriority.c_low));

            return Ok("Hangfire task id : " + jobId);
        }

        // GET: api/Keyword
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Keyword>>> GetKeywords()
        {
            return await unitOfWork.Context.Keywords.Include(x=>x.TagKeyword).ToListAsync();
        }

        [Route("GetAvailableKeywords")]
        [HttpGet]
        public ActionResult<IEnumerable<Keyword>> GetAvailableKeywords()
        {
            return unitOfWork.KeywordRepository.GetNonAssignedKeywords().ToList();
        }

    }
}
