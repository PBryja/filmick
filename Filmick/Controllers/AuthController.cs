﻿using System;
using System.ComponentModel.DataAnnotations;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading.Tasks;
using Common;
using Filmick.Infrastructure.Auth;
using Filmick.Infrastructure.Hangfire;
using Filmick.Model.Api;
using FilmickDataAccess.Model;
using FilmickRepositories;
using Hangfire;
using Hangfire.States;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.IdentityModel.Tokens;
using Filmick.Infrastructure.EmailProvider;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace Filmick.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : Controller
    {
        private readonly UserManager<ApplicationUser> userManager;
        private readonly IAuth auth;
        private readonly IUnitOfWork unitOfWork;
        private readonly AppConfig appConfig;
        private readonly SmtpClientEmailProvider emailProvider;

        public AuthController(UserManager<ApplicationUser> userManager, IAuth auth, IUnitOfWork unitOfWork, AppConfig appConfig, SmtpClientEmailProvider emailProvider)
        {
            this.userManager = userManager;
            this.auth = auth;
            this.unitOfWork = unitOfWork;
            this.appConfig = appConfig;
            this.emailProvider = emailProvider;
        }

        [HttpPost]
        [Route("register")]
        public async Task<IActionResult> Register([FromBody] RegisterModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest("Parametry nie są poprawne");

            var user = await userManager.FindByNameAsync(model.Username);
            if (user != null)
                return BadRequest("Użytkownik już istnieje");

            var userToSave = new ApplicationUser() { UserName = model.Username, Email = model.Email };
            var newUser = await userManager.CreateAsync(userToSave, model.Password);
            if (!newUser.Succeeded)
                return BadRequest(String.Join(";", newUser.Errors.Select(x => x.Description).ToList()));

            SendConfirmationEmail(userToSave);

            return Ok();
        }

        [HttpPost]
        [Route("ResendConfirmation/{email}")]
        public async Task<IActionResult> ResendConfirmation(string email)
        {
            if (!ModelState.IsValid)
                return BadRequest("Błąd, nieprawidłowe parametry");

            var user = await userManager.FindByEmailAsync(email);

            if (user == null)
                return BadRequest("Użytkownik nie istnieje");

            if (user.EmailConfirmed)
                return BadRequest("Email został już zatwierdzony.");

            SendConfirmationEmail(user);

            return Ok();
        }

        [HttpPost]
        [Route("ResetPassword/{email}")]
        public async Task<IActionResult> ResetPassword(string email)
        {
            if (!ModelState.IsValid)
                return BadRequest("Błąd, nieprawidłowe parametry");

            var user = await userManager.FindByEmailAsync(email);

            if (user == null)
                return BadRequest("Użytkownik nie istnieje");

            if (!user.EmailConfirmed)
                return BadRequest("Email nie jest jeszcze zweryfikowany. Musisz najpierw potwierdzić swój email.");

            SendResetPasswordEmail(user);

            return Ok();
        }

        [HttpPut]
        [Route("newpassword")]
        public async Task<IActionResult> NewPassword([FromBody] NewPasswordChange newPasswordChange)
        {
            if (!ModelState.IsValid)
                return BadRequest("Błąd, nieprawidłowe parametry");

            var user = await userManager.FindByNameAsync(newPasswordChange.Username);
            if (user == null)
                return BadRequest("Nie znaleziono użytkownika");

            var result = await userManager.ChangePasswordAsync(user, newPasswordChange.CurrentPassword, newPasswordChange.NewPassword); 

            if (!result.Succeeded)
                return BadRequest("Błąd podczas zmiany hasła. " + String.Join(";",result.Errors.Select(x => x.Description).ToList()));

            return Ok("Hasło zostało zmienione");
        }

        [HttpPut]
        [Route("newpasswordreset")]
        public async Task<IActionResult> NewPasswordReset([FromBody] NewPasswordResetToken newPasswordResetToken)
        {
            if (!ModelState.IsValid)
                return BadRequest("Błąd, nieprawidłowe parametry");

            var user = await userManager.FindByEmailAsync(newPasswordResetToken.Email);
            if (user == null)
                return BadRequest("Nie znaleziono użytkownika o takim adresie email");

            var result = await userManager.ResetPasswordAsync(user, newPasswordResetToken.Token,
                newPasswordResetToken.NewPassword);

            if (!result.Succeeded)
                return BadRequest("Błąd podczas zmiany hasła.");

            return Ok("Hasło zostało zmienione");
        }

        [HttpPut]
        [Route("confirmemail")]
        public async Task<IActionResult> ConfirmEmail([FromBody] ConfirmEmailToken confirmEmailToken)
        {
            if (!ModelState.IsValid)
                return BadRequest("Błąd, nieprawidłowe parametry");

            var user = await userManager.FindByEmailAsync(confirmEmailToken.Email);
            if (user == null)
                return BadRequest("Błąd, Email nie został potwierdzony.");

            if (user.EmailConfirmed)
                return BadRequest("Email został już zatwierdzony.");

            var result = await userManager.ConfirmEmailAsync(user, confirmEmailToken.Token);

            if (!result.Succeeded)
                return BadRequest("Błąd podczas weryfikacji Email.");

            return Ok("Email potwierdzony.");
        }

        [HttpPost]
        [Route("login")]
        public async Task<IActionResult> Login([FromBody] LoginModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest("Błąd, nieprawidłowe parametry");

            var user = await userManager.FindByNameAsync(model.Username);

            if (user == null || await userManager.CheckPasswordAsync(user, model.Password) == false)
                return Unauthorized();

            var token = auth.GenerateToken(user);
            ;

            if (String.IsNullOrWhiteSpace(user.RefreshToken))
            {
                user.RefreshToken = auth.GenerateRefreshToken();
                user.ExpiryDateRefreshToken = DateTime.Now.AddMinutes(appConfig.RefreshTokenExpireMinutes);
                unitOfWork.Context.Users.Update(user);
                await unitOfWork.Context.SaveChangesAsync();
            }

            return Ok(new
            {
                token = new JwtSecurityTokenHandler().WriteToken(token),
                expiration = token.ValidTo,
                refreshToken = user.RefreshToken
            });
        }

        [HttpPost]
        [Route("refreshtoken")]
        public async Task<IActionResult> RefreshToken([FromBody]TokenModel tokenModel)
        {
            var principal = auth.GetPrincipalFromToken(tokenModel.Token);
            var username = principal.Identity.Name ?? "";
            var user = await userManager.FindByNameAsync(username);

            if (user == null)
                return BadRequest("Użytkownik nie został znaleziony.");

            var savedRefreshToken = user.RefreshToken;
            if (savedRefreshToken != tokenModel.RefreshToken)
                throw new SecurityTokenException("Nieprawidłowy refresh token");

            if (user.ExpiryDateRefreshToken.Date < DateTime.UtcNow.Date)
                throw new SecurityTokenException("Refresh token stracił ważność.");


            var newJwtToken = auth.GenerateToken(user);

            return Ok(new
            {
                token = new JwtSecurityTokenHandler().WriteToken(newJwtToken),
                expiration = newJwtToken.ValidTo,
                refreshToken = user.RefreshToken
            });
        }

        private string GetUrlToRedirect()
        {
            string urlRedirect = HttpContext.Request.GetDisplayUrl();

            return urlRedirect.Remove(urlRedirect.IndexOf(HttpContext.Request.GetEncodedPathAndQuery(), StringComparison.Ordinal));
        }

        private void SendResetPasswordEmail(ApplicationUser user)
        {
            string urlRedirect = GetUrlToRedirect();

            string emailMessage = GenerateResetPasswordEmailMessage(user, urlRedirect);

            new BackgroundJobClient().Create(() => emailProvider.SendMessage(emailMessage, user.Email, "Reset hasła")
                , new EnqueuedState(HangfireQueuePriority.@default));
        }

        private string GenerateResetPasswordEmailMessage(ApplicationUser user, string url)
        {
            var token = userManager.GeneratePasswordResetTokenAsync(user);

            return url.Trim('/').Trim() + "/newpasswordreset?token=" + token.Result + "&email=" + user.Email;
        }

        private void SendConfirmationEmail(ApplicationUser user)
        {
            string urlRedirect = GetUrlToRedirect();

            string emailMessage = GenerateConfirmationEmailMessage(user, urlRedirect);

            new BackgroundJobClient().Create(() => emailProvider.SendMessage(emailMessage, user.Email, "Weryfikacja email")
                , new EnqueuedState(HangfireQueuePriority.@default));
        }

        private string GenerateConfirmationEmailMessage(ApplicationUser user, string url)
        {
            var token = userManager.GenerateEmailConfirmationTokenAsync(user);

            return url.Trim('/').Trim() + "/confirmemail?token=" + token.Result + "&email=" + user.Email;
        }
    }
}