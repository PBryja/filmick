﻿namespace Filmick.Infrastructure.EmailProvider
{
    public interface IEmailProvider
    {
        void SendMessage(string message, string toEmail, string title);
        void SendMessage(string message, string toEmail, string title, int port);
    }
}