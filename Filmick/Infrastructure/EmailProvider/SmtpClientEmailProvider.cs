﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using Common;

namespace Filmick.Infrastructure.EmailProvider
{
    public class SmtpClientEmailProvider :  IEmailProvider
    {
        private readonly AppConfig appConfig;

        public SmtpClientEmailProvider(AppConfig appConfig)
        {
            this.appConfig = appConfig;
        }

        public void SendMessage(string message, string toEmail, string title)
        {
            SendMessage(message, toEmail, title, 587);
        }

        public void SendMessage(string message, string toEmail, string title, int port)
        {
            SmtpClient client = new SmtpClient(appConfig.EmailServer, port)
            {
                UseDefaultCredentials = false,
                EnableSsl = true,
                Credentials = new NetworkCredential(appConfig.EmailUsername, appConfig.EmailPassword)
            };

            var mailMessage = new MailMessage(appConfig.EmailSender, toEmail, title, message);

            client.Send(mailMessage);
        }
    }
}
