﻿using System.IdentityModel.Tokens.Jwt;
using System.Net.Mail;
using System.Security.Claims;
using Common;
using Filmick.Controllers;
using FilmickDataAccess.Model;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.IdentityModel.Tokens;

namespace Filmick.Infrastructure.Auth
{
    public interface IAuth
    {
        TokenValidationParameters TokenValidationParameters();
        JwtSecurityToken GenerateToken(ApplicationUser user);
        ClaimsPrincipal GetPrincipalFromToken(string token);
        string GenerateRefreshToken();
    }
}