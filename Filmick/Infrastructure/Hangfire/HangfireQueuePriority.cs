﻿namespace Filmick.Infrastructure.Hangfire
{
    public static class HangfireQueuePriority
    {
        public static string[] Queues()
        {
            return new[] { c_low, @default, h_normal, l_important };
        }

        public static string c_low = "c_low";
        public static string @default = "default";
        public static string h_normal = "h_normal";
        public static string l_important = "l_important";
    }
}
