﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Filmick.Attributes
{
    public class HeaderAuthorizeAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if (context.HttpContext.Request.Headers.ContainsKey("Authorization"))
            {
                if (context.HttpContext.User.Identity.IsAuthenticated == false)
                {
                    context.Result = new UnauthorizedResult();
                }

            }
        }
    }
}
