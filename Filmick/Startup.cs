using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Common;
using Filmick.Infrastructure;
using FilmickDataAccess.Context;
using Filmick.Infrastructure.Auth;
using Filmick.Infrastructure.EmailProvider;
using Filmick.Infrastructure.Hangfire;
using Filmick.Infrastructure.QuestionAdapter;
using FilmickDataAccess.Model;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using FilmickDataAccess.Data;
using FilmickRepositories;
using FilmickServices.AutoMapper;
using FilmickServices.DownloadDailyFile;
using FilmickServices.Game;
using FilmickServices.Game.MovieManagers;
using FilmickServices.Game.QuestionAdapter;
using FilmickServices.Importer;
using Hangfire;
using Hangfire.States;
using Newtonsoft.Json;

namespace Filmick
{
    public class Startup
    {
        private IWebHostEnvironment Env { get; set; }
        public Startup(IConfiguration configuration, IWebHostEnvironment env)
        {
            Configuration = configuration;
            Env = env;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            services.AddDbContext<FilmickContext>(options => options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddOptions();
            services.Configure<AppConfig>(Configuration.GetSection("AppSettings"));

            //Dependency Injection
            services.AddTransient<AppConfig, AppConfig>();
            services.AddTransient<IUnitOfWork, UnitOfWork>();
            services.AddTransient<IQuestionAdapter, SaveQuestionAdapter>();
            services.AddTransient<MovieFilterManager, MovieFilterManager>();
            services.AddTransient<GameManager, GameManager>();
            services.AddTransient<SmtpClientEmailProvider, SmtpClientEmailProvider>();

            services.AddTransient<IFileDownloader, FileDownloader>();
            services.AddTransient<KeywordImporter, KeywordImporter>();
            services.AddTransient<IAuth, Auth>();

            services.AddIdentity<ApplicationUser, IdentityRole>(
                    o =>
                    {
                        o.Password.RequiredLength = 7;
                        o.Password.RequiredUniqueChars = 2;
                        o.Password.RequireNonAlphanumeric = false;
                        o.Password.RequireDigit = false;
                        o.Password.RequireLowercase = false;
                        o.Password.RequireNonAlphanumeric = false;
                        o.Password.RequireUppercase = false;

                        o.User.RequireUniqueEmail = true;
                        o.SignIn.RequireConfirmedEmail = false;
                    })
                    .AddEntityFrameworkStores<FilmickContext>()
                    .AddDefaultTokenProviders();

            var serviceProvider = services.BuildServiceProvider();

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(options =>
            {
                options.SaveToken = true;
                options.RequireHttpsMetadata = false;
                options.TokenValidationParameters = serviceProvider.GetService<IAuth>().TokenValidationParameters();
                options.Events = new JwtBearerEvents
                {
                    OnAuthenticationFailed = context =>
                    {
                        if (context.Exception.GetType() == typeof(SecurityTokenExpiredException))
                        {
                            context.Response.Headers.Add("Token-Expired", "true");
                        }
                        return Task.CompletedTask;
                    }
                };
            });

            services.AddControllersWithViews()
                .AddNewtonsoftJson(options =>
                    options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
                );

            services.AddHangfire(x =>
            {
                x.UseSqlServerStorage(Configuration.GetConnectionString("DefaultConnection"));
                x.UseSerializerSettings(new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
            });

            services.AddAutoMapper(typeof(DefaultProfile));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            var serviceProvider = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope().ServiceProvider;

            SeedDatabase.Initialize(serviceProvider);

            app.UseHttpsRedirection();
            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseHangfireDashboard("/dashboard");

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute("default", "api/{controller}/{action}/{id?}");
            });

            app.UseHangfireServer(new BackgroundJobServerOptions
            {
                Queues = HangfireQueuePriority.Queues()
            });

            if (env.IsProduction())
            {
                RecurringJob.AddOrUpdate(
                    () => ActivatorUtilities.CreateInstance<KeywordImporter>(serviceProvider)
                        .SynchKeywordMovies(
                            ActivatorUtilities.CreateInstance<UnitOfWork>(serviceProvider)
                                .KeywordRepository.GetAssignedKeywords().ToList(), true
                        ),
                    Cron.Daily(1));
            }
        }
    }
}
