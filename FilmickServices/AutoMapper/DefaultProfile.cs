﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using FilmickDataAccess.Model;
using FilmickDataAccess.Model.Assocs;
using FilmickServices.Game;

namespace FilmickServices.AutoMapper
{
    public class DefaultProfile : Profile
    {
        public DefaultProfile()
        {
            CreateMap<Genre, GenreVM>();
            CreateMap<Movie, MovieVM>()
                .ForMember(x => x.Genres,
                    y => y.MapFrom(z => z.MovieGenres.Select(genre => genre.Genre)
                    ));
        }
    }
}
