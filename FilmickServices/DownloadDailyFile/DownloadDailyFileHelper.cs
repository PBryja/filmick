﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FilmickServices.DownloadDailyFile
{
    class DownloadDailyFileHelper
    {
        public static string ExportUrl { get; } = @"http://files.tmdb.org/p/exports/";
        public static string DateUrl { get; } = DateTime.Now.AddDays(-1).ToString("MM_dd_yyyy");
        public static string SaveFolder { get; } = "Download/";
    }
}
