﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace FilmickServices.DownloadDailyFile
{
    public interface IFileDownloader
    {
        StreamReader GetFile(File file);
    }
}
