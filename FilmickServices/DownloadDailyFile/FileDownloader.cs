﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Text;

namespace FilmickServices.DownloadDailyFile
{
    public class FileDownloader : IFileDownloader
    {
        public StreamReader GetFile(File file)
        {
            using var client = new WebClient();
            var savedFile = DownloadDailyFileHelper.SaveFolder + file.Name + file.CompressedFormat;

            if (!System.IO.File.Exists(savedFile))
                client.DownloadFile(DownloadDailyFileHelper.ExportUrl + file.Name + file.CompressedFormat, savedFile);

            FileStream filestream = System.IO.File.OpenRead(savedFile);
            StreamReader decompressed = Decompress(filestream);

            return decompressed;
        }

        private StreamReader Decompress(FileStream fileStream)
        {
            using GZipStream gstream = new GZipStream(fileStream, CompressionMode.Decompress);
            MemoryStream memory = new MemoryStream();

            gstream.CopyTo(memory);
            memory.Seek(0, SeekOrigin.Begin);

            var sreader = new StreamReader(memory);
            return sreader;
        }
    }
}
