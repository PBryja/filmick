﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FilmickServices.DownloadDailyFile
{
    public abstract class File
    {
        public abstract string Name { get; }
        public virtual string CompressedFormat { get; } = ".json.gz";
    }
}
