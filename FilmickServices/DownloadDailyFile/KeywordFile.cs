﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace FilmickServices.DownloadDailyFile
{
    public class KeywordFile : File
    {
        public override string Name => "keyword_ids_" + DownloadDailyFileHelper.DateUrl;
    }
}
