﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace FilmickServices.DownloadDailyFile
{
    public class MovieFile : File
    {
        public override string Name => "movie_ids_" + DownloadDailyFileHelper.DateUrl;
    }
}
