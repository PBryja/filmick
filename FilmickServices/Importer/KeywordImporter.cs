﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using Common;
using FilmickDataAccess.Model;
using FilmickDataAccess.Model.Assocs;
using FilmickRepositories;
using FilmickServices.DownloadDailyFile;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace FilmickServices.Importer
{
    public class KeywordImporter
    {
        public IUnitOfWork UnitOfWork { get; }
        public IFileDownloader FileDownloader { get; }
        public StringBuilder ReportBuilder { get; }
        private AppConfig AppConfig { get; }
        public bool ImportExpressMode_UpdateOnlyNameIfKeywordAlreadySavedInDb { get; set; } = false;

        private List<Keyword> _keywordsDb;
        private List<Keyword> KeywordsDb => _keywordsDb ??= UnitOfWork.Context.Keywords.Include(x => x.TagKeyword).ToList();

        private readonly string urlMovies =
            "https://api.themoviedb.org/3/keyword/{0}/movies?api_key={2}&language=pl-pl&include_adult=false&page={1}";

        public KeywordImporter(IUnitOfWork unitOfWork, IFileDownloader fileDownloader, AppConfig appConfig)
        {
            this.UnitOfWork = unitOfWork;
            this.FileDownloader = fileDownloader;
            this.AppConfig = appConfig;

            ReportBuilder = new StringBuilder();
        }

        public void Import()
        {
            var fileProp = new KeywordFile();
            var sReader = FileDownloader.GetFile(fileProp);

            while (!sReader.EndOfStream)
            {
                var jsonString = sReader.ReadLine();
                var keywordJson = JsonConvert.DeserializeObject<KeywordJson>(jsonString);

                if (ImportExpressMode_UpdateOnlyNameIfKeywordAlreadySavedInDb)
                {
                    if (OverrideNameOfKeyword(keywordJson.id, keywordJson.name))
                        continue;
                }

                SynchKeywordData(keywordJson.id, keywordJson.name);
            }
        }

        private KeywordMovies GetKeywordMoviesFromUrl(int idKeyword, string nameKeyword, int page)
        {
            using var wc = new WebClient();
            string jsonFromUrl;

            try
            {
                jsonFromUrl = wc.DownloadString(String.Format(urlMovies, idKeyword, page, AppConfig.ApiKey));
            }
            catch (Exception e)
            {
                ReportBuilder.AppendLine("Wystapil błąd przy pobieraniu szczegółów(filmów), keyword " + idKeyword + " nazwa " + nameKeyword + " page " + page + " error komunikat: "+e.Message);
                return null;
            }

            var keywordMovies = JsonConvert.DeserializeObject<KeywordMovies>(jsonFromUrl);

            if (page == 1 && keywordMovies.total_pages >= 1000)
                ReportBuilder.AppendLine("Keyword o id " + keywordMovies.id + " posiada więcej niż 1000 stron");

            return keywordMovies;
        }

        private List<int> GetMoviesFromUrl(int idKeyword, string nameKeyword, KeywordMovies keywordMovies)
        {
            List<int> fullMovieIds = new List<int>();

            for (int i = 1; i <= keywordMovies.total_pages; i++)
            {
                if (keywordMovies.page != i)
                {
                    keywordMovies = GetKeywordMoviesFromUrl(idKeyword, nameKeyword, i);
                    if (keywordMovies == null)
                        return null;
                }

                fullMovieIds.AddRange(keywordMovies.results.Where(x=>x != null).Select(x=>x.id));
            }

            return fullMovieIds;
        }

        public void SynchKeywordMovies(List<Keyword> keywords, bool synchAllMovies)
        {
            foreach (var keyword in keywords)
            {
                SynchKeywordMovies(keyword, synchAllMovies);
            }
        }

        public void SynchKeywordMovies(Keyword keyword, bool synchAllMovies)
        {
            var keywordMovies = GetKeywordMoviesFromUrl(keyword.ApiId, keyword.Name, 1);

            if (keywordMovies == null)
                return;

            if (keyword.TotalMovies != keywordMovies.total_results)
            {
                keyword.TotalMovies = keywordMovies.total_results;
                UnitOfWork.KeywordRepository.Update(keyword);
                UnitOfWork.Context.SaveChanges();
            }

            if (keyword.TagKeyword == null || synchAllMovies == false)
                return;

            var fullMovieIds = GetMoviesFromUrl(keyword.ApiId, keyword.Name, keywordMovies);

            if (fullMovieIds == null)
                return;

            UnitOfWork.TagQuestionRepository.UpdateMoviesWithTagQuestionConnectedByKeyword(keyword.KeywordId, fullMovieIds);
        }

        private void SynchKeywordData(int apiIdKeyword, string nameKeyword)
        {
            var keywordEntity = KeywordsDb.SingleOrDefault(x => x.ApiId == apiIdKeyword) ?? new Keyword();

            keywordEntity.ApiId = apiIdKeyword;
            keywordEntity.Archive = false;
            keywordEntity.Name = nameKeyword;

            if (keywordEntity.KeywordId > 0)
                UnitOfWork.Context.Keywords.Update(keywordEntity);
            else
                UnitOfWork.Context.Keywords.Add(keywordEntity);

            UnitOfWork.Context.SaveChanges();

            SynchKeywordMovies(keywordEntity, false);
        }

        private bool OverrideNameOfKeyword(int id, string name)
        {
            if (KeywordsDb.Any(x => x.ApiId == id))
            {
                var keywordEntityy = KeywordsDb.Single(x => x.ApiId == id);

                keywordEntityy.Name = name;
                UnitOfWork.Context.SaveChanges();

                return true;
            }

            return false;
        }

        private class KeywordJson
        {
            public int id;
            public string name;
        }

        private class KeywordMovies
        {
            public int id;
            public int total_results;
            public List<MovieIds> results;
            public int total_pages;
            public int page;
        }
        private class MovieIds
        {
            public int id;
        }
    }
}
