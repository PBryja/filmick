﻿using System;
using System.Collections.Generic;
using System.Linq;
using FilmickDataAccess.Model;
using FilmickRepositories;
using static FilmickDataAccess.Model.Relationships.FilterQuestionAssociation;


namespace FilmickServices.Game.GameQuestionManagers
{
    public class FilterQuestionManager : GameQuestionManager
    {
        public FilterQuestionManager(IUnitOfWork unitOfWork, Guid idGame) : base(unitOfWork, idGame) { }

        protected override GameQuestion GenerateNextQuestion()
        {
            if (lastQuestion == null)
            {
                return GenerateNonChildQuestion(new List<FilterQuestion>());
            }

            if (lastQuestion.FilterQuestion == null)
            {
                return null;
            }

            return GenerateAnyTypeQuestion();
        }
        private GameQuestion GenerateAnyTypeQuestion()
        {
            var allAskedQuestionsGameQuestion = unitOfWork.GameQuestionRepository.GetAllQuestions(idGame, true);
            var allAskedQuestionsGameQuestionsWithYesAnswer = allAskedQuestionsGameQuestion.Where(x => x.Answer == GameQuestion.AnswerEnum.Yes); 
            var allAskedQuestionsGameQuestionsWithNoAnswer = allAskedQuestionsGameQuestion.Where(x => x.Answer == GameQuestion.AnswerEnum.No);
            var allAskedQuestionsGameQuestionsWithIdkAnswer = allAskedQuestionsGameQuestion.Where(x => x.Answer == GameQuestion.AnswerEnum.Idk);

            var allAskedQuestions = allAskedQuestionsGameQuestion.Select(x => x.FilterQuestion);
            var allAskedQuestionsWithYesAnswer = allAskedQuestionsGameQuestionsWithYesAnswer.Select(x => x.FilterQuestion);
            var allAskedQuestionsWithNoAnswer = allAskedQuestionsGameQuestionsWithNoAnswer.Select(x => x.FilterQuestion);
            var allAskedQuestionsWithIdkAnswer = allAskedQuestionsGameQuestionsWithIdkAnswer.Select(x => x.FilterQuestion);

            var questionToReturn = new FilterQuestion();

            var excludedQuestions = GetAllQuestionsExcludedByYesAnswer(allAskedQuestionsWithYesAnswer);
            excludedQuestions = excludedQuestions.Concat(GetAllQuestionsExcludedByNoAnswer(allAskedQuestionsWithNoAnswer));
            excludedQuestions = excludedQuestions.Concat(GetAllQuestionsExcludedByIdkAnswer(allAskedQuestionsWithIdkAnswer));
            excludedQuestions = excludedQuestions.Concat(allAskedQuestions).Distinct();

            questionToReturn = GetQuestionIfAvailable(GetAllChildQuestions(allAskedQuestionsWithYesAnswer).ToList(), excludedQuestions);

            if (questionToReturn != null)
                return new GameQuestion() { FilterQuestion = questionToReturn };

            return GenerateNonChildQuestion(excludedQuestions);
        }

        private GameQuestion GenerateNonChildQuestion(IEnumerable<FilterQuestion> excludedQuestions)
        {
            var random = new Random();

            bool getQuestionFromAllMainParentQuestions = Convert.ToBoolean(random.Next(2));
            FilterQuestion newFilterQuestion;

            if (getQuestionFromAllMainParentQuestions)
            {
                newFilterQuestion = GetQuestionIfAvailable(unitOfWork.FilterQuestionRepository.GetAllMainParentQuestions().ToList(), excludedQuestions)
                       ?? GetQuestionIfAvailable(unitOfWork.FilterQuestionRepository.GetAllNonParentAndNonChildQuestions().ToList(), excludedQuestions);
            }

            else
            {
                newFilterQuestion = GetQuestionIfAvailable(unitOfWork.FilterQuestionRepository.GetAllNonParentAndNonChildQuestions().ToList(), excludedQuestions)
                     ?? GetQuestionIfAvailable(unitOfWork.FilterQuestionRepository.GetAllMainParentQuestions().ToList(), excludedQuestions);
            }

            if (newFilterQuestion == null)
                return null;

            return new GameQuestion() { FilterQuestion = newFilterQuestion };
        }

        private IEnumerable<FilterQuestion> GetAllChildQuestions(IEnumerable<FilterQuestion> parentQuestions)
        {
            return parentQuestions.SelectMany(x => x.Dependents).Where(y => y.Type == TypeEnum.Parent_child).Select(y => y.Dependent).ToList();
        }

        private IEnumerable<FilterQuestion> GetAllQuestionsExcludedByYesAnswer(IEnumerable<FilterQuestion> sourceQuestions)
        {
            List<TypeEnum> excludedTypes = new List<TypeEnum>()
            {
                TypeEnum.Exclude_WhenOnlyYes,
                TypeEnum.Exclude_WhenYesAndNo,
                TypeEnum.Exclude_WhenYesAndIdk,
            };

            return GetDependentsExcludedByTypes(excludedTypes, sourceQuestions);
        }

        private IEnumerable<FilterQuestion> GetAllQuestionsExcludedByNoAnswer(IEnumerable<FilterQuestion> sourceQuestions)
        {
            List<TypeEnum> excludedTypes = new List<TypeEnum>()
            {
                TypeEnum.Exclude_WhenOnlyNo,
                TypeEnum.Exclude_WhenYesAndNo,
                TypeEnum.Exclude_WhenNoAndIdk,
            };

            return GetDependentsExcludedByTypes(excludedTypes, sourceQuestions);
        }

        private IEnumerable<FilterQuestion> GetAllQuestionsExcludedByIdkAnswer(IEnumerable<FilterQuestion> sourceQuestions)
        {
            List<TypeEnum> excludedTypes = new List<TypeEnum>()
            {
                TypeEnum.Exclude_WhenOnlyIdk,
                TypeEnum.Exclude_WhenYesAndIdk,
                TypeEnum.Exclude_WhenNoAndIdk,
            };

            return GetDependentsExcludedByTypes(excludedTypes, sourceQuestions);
        }

        private IEnumerable<FilterQuestion> GetDependentsExcludedByTypes(List<TypeEnum> excludedTypes, IEnumerable<FilterQuestion> sourceQuestions)
        {
            return sourceQuestions.SelectMany(x => x.Dependents).Where(y => excludedTypes.Contains(y.Type)).Select(y => y.Dependent).ToList();
        }
    }
}
