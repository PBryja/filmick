﻿using System;
using System.Collections.Generic;
using System.Linq;
using FilmickDataAccess.Model;
using FilmickRepositories;
using static FilmickDataAccess.Model.Relationships.TagQuestionAssociation;

namespace FilmickServices.Game.GameQuestionManagers
{
    public class TagQuestionManager : GameQuestionManager
    {
        private readonly IEnumerable<Movie> filteredMovies;
        private IEnumerable<KeyValuePair<TagQuestion, int>> allTagsOfAvailableMovies;
        private IEnumerable<GameQuestion> allAskedQuestionsGameQuestion;
        private IEnumerable<KeyValuePair<TagQuestion, GameQuestion.AnswerEnum>> allAskedQuestionsWithAnswers;
        private IEnumerable<KeyValuePair<TagQuestion, int>> allAvailableQuestions;

        const int HowManyTopQuestions = 10;

        public TagQuestionManager(IUnitOfWork unitOfWork, Guid idGame, IEnumerable<Movie> filteredMovies) : base(unitOfWork, idGame)
        {
            this.filteredMovies = filteredMovies;
        }

        protected override GameQuestion GenerateNextQuestion()
        {
            var lastQuestion = unitOfWork.GameQuestionRepository.GetTheNewestQuestionByGameId(idGame);
            allTagsOfAvailableMovies = GetAllTagsOfMoviesWithCount();

            allAskedQuestionsGameQuestion = unitOfWork.GameQuestionRepository.GetAllQuestions(idGame, false);
            allAskedQuestionsWithAnswers = allAskedQuestionsGameQuestion.ToDictionary(x => x.TagQuestion, y => y.Answer);

            allAvailableQuestions = unitOfWork.TagQuestionRepository.GetAvailableQuestionsAndCount(allAskedQuestionsWithAnswers.ToList(), filteredMovies, true);

            if (lastQuestion?.TagQuestion == null)
            {
                return GenerateNonChildQuestion(allAvailableQuestions.Select(x=>x.Key));
            }

            return GenerateAnyTypeQuestion();
        }
        private GameQuestion GenerateAnyTypeQuestion()
        {
            var allChildQuestions =
                GetAllChildQuestions(
                    allAskedQuestionsGameQuestion.Where(x => x.Answer == GameQuestion.AnswerEnum.Yes)
                        .Select(x => x.TagQuestion), allTagsOfAvailableMovies.Select(x => x.Key));

            var availableQuestions = allAvailableQuestions.Select(x => x.Key).ToList();

            var questionToReturn = GetQuestion(allChildQuestions.ToList(), availableQuestions);

            return questionToReturn != null ? new GameQuestion() { TagQuestion = questionToReturn } : GenerateNonChildQuestion(availableQuestions);
        }

        private IEnumerable<TagQuestion> GetAllChildQuestions(IEnumerable<TagQuestion> parentQuestions, IEnumerable<TagQuestion> source)
        {
            return parentQuestions.SelectMany(x => x.Dependents).Where(y => y.Type == TypeEnum.Parent_child && source.Contains(y.Dependent)).Select(y => y.Dependent).ToList();
        }

        private IEnumerable<KeyValuePair<TagQuestion, int>> GetAllTagsOfMoviesWithCount()
        {
            return filteredMovies
                .SelectMany(x => x.MovieTags)
                .GroupBy(x => x.TagQuestion)
                .ToDictionary(x => x.Key, y => y.Count())
                .OrderByDescending(y => y.Value);
        }

        private GameQuestion GenerateNonChildQuestion(IEnumerable<TagQuestion> availableQuestions)
        {
            var random = new Random();

            bool getQuestionFromAllMainParentQuestions = Convert.ToBoolean(random.Next(2));
            TagQuestion newTagQuestion;

            if (getQuestionFromAllMainParentQuestions)
            {
                newTagQuestion = GetQuestion(unitOfWork.TagQuestionRepository.GetAllMainParentQuestionsGroupAndCount(filteredMovies).Select(x => x.Key).ToList(), availableQuestions, HowManyTopQuestions)
                       ?? GetQuestion(unitOfWork.TagQuestionRepository.GetAllNonParentAndNonChildQuestionsGroupAndCount(filteredMovies).Select(x => x.Key).ToList(), availableQuestions, HowManyTopQuestions);
            }

            else
            {
                newTagQuestion = GetQuestion(unitOfWork.TagQuestionRepository.GetAllNonParentAndNonChildQuestionsGroupAndCount(filteredMovies).Select(x => x.Key).ToList(), availableQuestions, HowManyTopQuestions)
                     ?? GetQuestion(unitOfWork.TagQuestionRepository.GetAllMainParentQuestionsGroupAndCount(filteredMovies).Select(x => x.Key).ToList(), availableQuestions, HowManyTopQuestions);
            }

            if (newTagQuestion == null)
                return null;

            return new GameQuestion() { TagQuestion = newTagQuestion };
        }
    }
}
