﻿using System;
using System.Collections.Generic;
using System.Linq;
using FilmickDataAccess.Model;
using FilmickRepositories;

namespace FilmickServices.Game.GameQuestionManagers
{
    public abstract class GameQuestionManager
    {
        protected IUnitOfWork unitOfWork;
        protected Guid idGame;
        protected GameQuestion lastQuestion;
        public Random Random { get; set; }

        protected GameQuestionManager(IUnitOfWork unitOfWork, Guid idGame)
        {
            this.unitOfWork = unitOfWork;
            this.idGame = idGame;
            this.Random = new Random();
        }

        public virtual GameQuestion GetQuestion()
        {
            var lastQuestionWithoutAnswer = GetAskedQuestionWithoutAnswerIfExists();

            return lastQuestionWithoutAnswer ?? GenerateNextQuestion();

        }

        protected abstract GameQuestion GenerateNextQuestion();

        protected T GetQuestion<T>(List<T> questions, IEnumerable<T> availableQuestions, int limitNumTop) where T : class
        {
            T questionToReturn = default;
            List<T> questionsToReturn = new List<T>();

            if (availableQuestions.Any())
            {
                questionsToReturn = questions.Where(availableQuestions.Contains).ToList();
            }

            if (questionsToReturn.Any())
            {
                questionsToReturn = questionsToReturn.Take(limitNumTop).ToList();
                questionToReturn = questionsToReturn[Random.Next(questionsToReturn.Count)];
            }

            return questionToReturn;
        }

        protected T GetQuestion<T>(List<T> questions, IEnumerable<T> availableQuestions) where T : class
        {
            T questionToReturn = default;
            List<T> questionsToReturn = new List<T>();

            if (availableQuestions.Any())
            {
                questionsToReturn = questions.Where(availableQuestions.Contains).ToList();
            }

            if (questionsToReturn.Any())
                questionToReturn = questionsToReturn[Random.Next(questionsToReturn.Count)];

            return questionToReturn;
        }

        protected T GetQuestionIfAvailable<T>(List<T> questions, IEnumerable<T> excludedQuestions) where T : class
        {
            T questionToReturn = default;

            if (excludedQuestions.Any())
                questions.RemoveAll(excludedQuestions.Contains);

            if (questions.Any())
                questionToReturn = questions[Random.Next(questions.Count)];

            return questionToReturn;
        }

        protected GameQuestion GetAskedQuestionWithoutAnswerIfExists()
        {
            lastQuestion = unitOfWork.GameQuestionRepository.GetTheNewestQuestionByGameId(idGame);

            if (lastQuestion != null && lastQuestion.Answer == GameQuestion.AnswerEnum.NoAnswer)
                return lastQuestion;

            return null;
        }
    }
}
