﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using AutoMapper;
using FilmickRepositories;
using FilmickServices.Game.QuestionAdapter;
using FilmickDataAccess.Model;
using FilmickServices.Game.MovieManagers;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using static FilmickDataAccess.Model.Game;

namespace FilmickServices.Game
{
    public class GameManager
    {
        private readonly IQuestionAdapter questionAdapter;
        private readonly IUnitOfWork unitOfWork;
        private readonly MovieFilterManager movieFilterManager;
        private readonly IMapper mapper;

        public GameManager(IQuestionAdapter questionAdapter, IUnitOfWork unitOfWork, MovieFilterManager movieFilterManager, IMapper mapper)
        {
            this.questionAdapter = questionAdapter;
            this.unitOfWork = unitOfWork;
            this.movieFilterManager = movieFilterManager;
            this.mapper = mapper;
        }

        private Expression<Func<Movie, bool>> GetExpressionForFilters(FilmickDataAccess.Model.Game game)
        {
            var askedQuestions = unitOfWork.GameQuestionRepository.GetAllQuestions(game.GameId, true).ToList();
            var expressionFilter = movieFilterManager.GetExpressionFilterEnglishMoviesWhenNoLanguageIsPicked(movieFilterManager.GetExpressionFilterMovies(askedQuestions), askedQuestions);

            return expressionFilter;
        }

        public List<Movie> GetFilteredMovies(FilmickDataAccess.Model.Game game, bool ignoreStatus = false)
        {
            if (!ignoreStatus && (game.Status == StatusEnum.NoMoviesAfterFilters ||
                                  (unitOfWork.GameQuestionRepository.GetTheNewestQuestionByGameId(game.GameId)
                                      .TagQuestion == null && game.Status != StatusEnum.NoTagQuestionsAfterFilters)))
                throw new CustomAttributeFormatException("Movies are not available due to status of game");

            var expressionFilter = GetExpressionForFilters(game);
            var filteredMovies = unitOfWork.Context.Movies
                .Include(x => x.MovieTags).ThenInclude(x => x.TagQuestion)
                .Include(x => x.MovieGenres)
                .ThenInclude(x=>x.Genre)
                .Where(expressionFilter)
                .OrderByDescending(x=>x.VoteAverage)
                .ToList();

            return filteredMovies;
        }

        public List<Movie> GetProposedMovies(FilmickDataAccess.Model.Game game, bool ignoreStatus = false, List<Movie> filteredMovies = null)
        {
            if (!ignoreStatus)
            {
                if (game.Status == StatusEnum.NotFinish || game.Status == StatusEnum.NoMoviesAfterFilters)
                    throw new CustomAttributeFormatException("Movies are not available due to status of game");
            }

            filteredMovies ??= GetFilteredMovies(game, ignoreStatus);

            MatchMovieManager matchMovieManager = new MatchMovieManager(unitOfWork, game.GameId, filteredMovies);
            return matchMovieManager.GetGoodMatchMovies().ToList();
        }

        public Question GetQuestion(FilmickDataAccess.Model.Game game)
        {
            if (game.Status != StatusEnum.NotFinish)
                return new Question() { StatusCode = Question.StatusEnum.Failed_GameIsAlreadyFinished };

            Question question;

            if (unitOfWork.GameQuestionRepository.GetTheNewestQuestionByGameId(game.GameId)?.TagQuestion == null)
            {
                question = questionAdapter.GetQuestionFromFilterQuestionManager(game.GameId);

                if (question != null)
                {
                    question.IsAvailablePreviousQuestion = unitOfWork.GameQuestionRepository.IsMoreQuestionsThanOne(game.GameId);
                    return question;
                }
            }

            var filteredMovies = GetFilteredMovies(game, true);

            if (!filteredMovies.Any())
                return FinishGameCauseNoFilteredMovies(game);

            MatchMovieManager matchMovieManager = new MatchMovieManager(unitOfWork, game.GameId, filteredMovies);
            var movies = matchMovieManager.GetAvailableMovies();

            question = questionAdapter.GetQuestionFromTagQuestionManager(game.GameId, movies) ?? new Question();

            question.ProposedMovies = mapper.Map<List<Movie>, List<MovieVM>>(matchMovieManager.GetGoodMatchMovies().ToList());
            question.AvailableMovies = movies.Count();
            question.MoviesAfterFilters = filteredMovies.Count();

            if (question.QuestionId > 0)
                return question;

            return FinishGameCauseNoMoreTagQuestions(question, game);
        }

        public Question.StatusEnum IsGameAvailableAndValidForPlay(FilmickDataAccess.Model.Game game, string userId)
        {
            if (game == null)
            {
                return Question.StatusEnum.Failed_NotCorrectGameId;
            }

            if (game.ApplicationUserId != userId)
            {
                return Question.StatusEnum.Failed_AccessDenied;
            }

            if (game.Status != StatusEnum.NotFinish)
                return Question.StatusEnum.Failed_GameIsAlreadyFinished;

            return Question.StatusEnum.Ok;
        }

        private Question FinishGameCauseNoFilteredMovies(FilmickDataAccess.Model.Game game)
        {
            game.Status = StatusEnum.NoMoviesAfterFilters;
            unitOfWork.GameRepository.Update(game);
            unitOfWork.Save();

            return new Question()
            {
                GameId = game.GameId.ToString(),
                IsAvailablePreviousQuestion = false,
                StatusCode = Question.StatusEnum.Ok_NoMoreQuestions
            };
        }

        private Question FinishGameCauseNoMoreTagQuestions(Question question, FilmickDataAccess.Model.Game game)
        {
            game.Status = unitOfWork.GameQuestionRepository.GetTheNewestQuestionByGameId(game.GameId).TagQuestion == null ? StatusEnum.NoTagQuestionsAfterFilters : StatusEnum.GameFinish;
            unitOfWork.GameRepository.Update(game);
            unitOfWork.Save();

            question.GameId = game.GameId.ToString();
            question.IsAvailablePreviousQuestion = false;
            question.StatusCode = Question.StatusEnum.Ok_NoMoreQuestions;

            return question;
        }
    }
}
