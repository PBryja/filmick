﻿using System;
using System.Collections.Generic;
using System.Text;
using FilmickDataAccess.Model;
using FilmickDataAccess.Model.Assocs;

namespace FilmickServices.Game
{
    public class MovieVM
    {
        public int MovieId { get; set; }
        public int ApiId { get; set; }
        public string PosterPath { get; set; }
        public string BackdropPath { get; set; }
        public string OriginalTitle { get; set; }
        public virtual List<GenreVM> Genres { get; set; } = new List<GenreVM>();
        public string Title { get; set; }
        public float VoteAverage { get; set; }
        public string Description { get; set; }
        public DateTime ReleaseDate { get; set; }
    }
}
