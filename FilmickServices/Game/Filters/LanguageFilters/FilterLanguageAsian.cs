﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using FilmickDataAccess.Model;
using FilmickDataAccess.Model.Enums;

namespace FilmickServices.Game.Filters.LanguageFilters
{
    public class FilterLanguageAsian : Filter
    {
        private List<string> LanguageNames = new List<string>() { "ja", "zh", "ko" };

        public override FilterEnum GetFilterEnum => FilterEnum.Language_Asian;

        public override Expression<Func<Movie, bool>> Yes()
        {
            return x => LanguageNames.Contains(x.OriginalLanguage);
        }

        public override Expression<Func<Movie, bool>> No()
        {
            return x => !LanguageNames.Contains(x.OriginalLanguage);
        }
    }
}
