﻿using System;
using System.Linq.Expressions;
using FilmickDataAccess.Model;
using FilmickDataAccess.Model.Enums;

namespace FilmickServices.Game.Filters.LanguageFilters
{
    public class FilterLanguageEnglish : Filter
    {
        private const string LanguageName = "en";

        public override FilterEnum GetFilterEnum => FilterEnum.Language_English;

        public override Expression<Func<Movie, bool>> Yes()
        {
            return x => x.OriginalLanguage == LanguageName;
        }

        public override Expression<Func<Movie, bool>> No()
        {
            return x => x.OriginalLanguage != LanguageName;
        }
    }
}
