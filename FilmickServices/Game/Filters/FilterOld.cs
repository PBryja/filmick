﻿using System;
using System.Linq.Expressions;
using FilmickDataAccess.Model;
using FilmickDataAccess.Model.Enums;

namespace FilmickServices.Game.Filters
{
    public class FilterOld : Filter
    {
        public override FilterEnum GetFilterEnum => FilterEnum.Old;

        public override Expression<Func<Movie, bool>> Yes()
        {
            return x => x.ReleaseDate.Year < 2000;
        }

        public override Expression<Func<Movie, bool>> No()
        {
            return x => x.ReleaseDate.Year >= 2000;
        }

        public override Expression<Func<Movie, bool>> Idk()
        {
            return x => x.ReleaseDate.Year < 2015 && x.ReleaseDate.Year > 1990;
        }
    }
}
