﻿using FilmickDataAccess.Model.Enums;

namespace FilmickServices.Game.Filters
{
    public class DefaultFilter :Filter
    {
        public override FilterEnum GetFilterEnum => FilterEnum.NoFilter;
    }
}
