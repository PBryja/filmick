﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using FilmickDataAccess.Model;
using FilmickDataAccess.Model.Enums;

namespace FilmickServices.Game.Filters.GenreFilters
{
    public class FilterGenreHorrorOrThriller : Filter
    {
        private List<string> GenreNames = new List<string>() { "Horror", "Thriller" }; 
        public override FilterEnum GetFilterEnum => FilterEnum.Genre_HorrorOrThriller;

        public override Expression<Func<Movie, bool>> Yes()
        {
            return x => x.MovieGenres.Any(y => GenreNames.Contains(y.Genre.ApiName));
        }

        public override Expression<Func<Movie, bool>> No()
        {
            return x => !x.MovieGenres.Any(y => GenreNames.Contains(y.Genre.ApiName));
        }
    }
}
