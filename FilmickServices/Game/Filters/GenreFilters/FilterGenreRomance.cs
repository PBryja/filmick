﻿using System;
using System.Linq;
using System.Linq.Expressions;
using FilmickDataAccess.Model;
using FilmickDataAccess.Model.Enums;

namespace FilmickServices.Game.Filters.GenreFilters
{
    public class FilterGenreRomance : Filter
    {
        private const string GenreName = "Romance";
        public override FilterEnum GetFilterEnum => FilterEnum.Genre_Romance;

        public override Expression<Func<Movie, bool>> Yes()
        {
            return x => x.MovieGenres.Any(y => y.Genre.ApiName == GenreName);
        }

        public override Expression<Func<Movie, bool>> No()
        {
            return x => !x.MovieGenres.Any(y => y.Genre.ApiName == GenreName);
        }
    }
}
