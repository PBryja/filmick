﻿using System;
using System.Linq;
using System.Linq.Expressions;
using FilmickDataAccess.Model;
using FilmickDataAccess.Model.Enums;

namespace FilmickServices.Game.Filters.GenreFilters
{
    public class FilterGenreWar : Filter
    {
        private const string GenreName = "War";
        public override FilterEnum GetFilterEnum => FilterEnum.Genre_War;

        public override Expression<Func<Movie, bool>> Yes()
        {
            return x => x.MovieGenres.Any(y => y.Genre.ApiName == GenreName);
        }

        public override Expression<Func<Movie, bool>> No()
        {
            return x => !x.MovieGenres.Any(y => y.Genre.ApiName == GenreName);
        }

        public override Expression<Func<Movie, bool>> Idk()
        {
            return x => !x.MovieGenres.Any(y => y.Genre.ApiName == GenreName);
        }
    }
}
