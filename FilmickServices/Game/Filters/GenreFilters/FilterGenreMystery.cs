﻿using System;
using System.Linq;
using System.Linq.Expressions;
using FilmickDataAccess.Model;
using FilmickDataAccess.Model.Enums;

namespace FilmickServices.Game.Filters.GenreFilters
{
    public class FilterGenreMystery : Filter
    {
        private const string GenreName = "Mystery";
        public override FilterEnum GetFilterEnum => FilterEnum.Genre_Mystery;

        public override Expression<Func<Movie, bool>> No()
        {
            return x => !x.MovieGenres.Any(y => y.Genre.ApiName == GenreName);
        }
    }
}
