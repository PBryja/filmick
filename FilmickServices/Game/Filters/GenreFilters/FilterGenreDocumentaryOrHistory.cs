﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using FilmickDataAccess.Model;
using FilmickDataAccess.Model.Enums;

namespace FilmickServices.Game.Filters.GenreFilters
{
    public class FilterGenreDocumentaryOrHistory : Filter
    {
        private List<string> GenreNames = new List<string>() { "Documentary", "History" };
        public override FilterEnum GetFilterEnum => FilterEnum.Genre_DocumentaryOrHistory;

        public override Expression<Func<Movie, bool>> Yes()
        {
            return x => x.MovieGenres.Any(y => GenreNames.Contains(y.Genre.ApiName));
        }

        public override Expression<Func<Movie, bool>> No()
        {
            return x => !x.MovieGenres.Any(y => GenreNames.Contains(y.Genre.ApiName));
        }

        public override Expression<Func<Movie, bool>> Idk()
        {
            return x => !x.MovieGenres.Any(y => GenreNames.Contains(y.Genre.ApiName));
        }
    }
}
