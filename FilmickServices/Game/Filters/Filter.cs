﻿using System;
using System.Linq.Expressions;
using FilmickDataAccess.Model;
using FilmickDataAccess.Model.Enums;

namespace FilmickServices.Game.Filters
{
    public abstract class Filter
    {
        public abstract FilterEnum GetFilterEnum { get; }
        public virtual Expression<Func<Movie, bool>> Yes()
        {
            return x => true;
        }

        public virtual Expression<Func<Movie, bool>> No()
        {
            return x => true;
        }

        public virtual Expression<Func<Movie, bool>> Idk()
        {
            return x => true;
        }
    }
}
