﻿using System;
using System.Linq.Expressions;
using FilmickDataAccess.Model;
using FilmickDataAccess.Model.Enums;

namespace FilmickServices.Game.Filters
{
    public class FilterNewest : Filter
    {
        public override FilterEnum GetFilterEnum => FilterEnum.Newest;

        public override Expression<Func<Movie, bool>> Yes()
        {
            return x => x.ReleaseDate.Year >= DateTime.Now.Year - 1;
        }

        public override Expression<Func<Movie, bool>> No()
        {
            return x => x.ReleaseDate.Year < DateTime.Now.Year - 1;
        }

        public override Expression<Func<Movie, bool>> Idk()
        {
            return x => x.ReleaseDate.Year < DateTime.Now.Year;
        }
    }
}
