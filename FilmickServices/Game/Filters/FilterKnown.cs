﻿using System;
using System.Linq.Expressions;
using FilmickDataAccess.Model;
using FilmickDataAccess.Model.Enums;

namespace FilmickServices.Game.Filters
{
    public class FilterKnown : Filter
    {
        public override FilterEnum GetFilterEnum => FilterEnum.Known;

        public override Expression<Func<Movie, bool>> Yes()
        {
            return x => x.VoteCount >= 100;
        }

        public override Expression<Func<Movie, bool>> No()
        {
            return x => x.VoteCount <= 1000;
        }

        public override Expression<Func<Movie, bool>> Idk()
        {
            return x => x.VoteCount >= 10;
        }
    }
}
