﻿using FilmickDataAccess.Model.Enums;

namespace FilmickServices.Game.Filters
{
    public class FilterShort : Filter
    {
        public override FilterEnum GetFilterEnum => FilterEnum.Short;
    }
}
