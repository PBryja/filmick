﻿using System;
using System.Linq.Expressions;
using FilmickDataAccess.Model;
using FilmickDataAccess.Model.Enums;

namespace FilmickServices.Game.Filters
{
    public class FilterBeginXXI : Filter
    {
        public override FilterEnum GetFilterEnum => FilterEnum.BeginXXI;

        public override Expression<Func<Movie, bool>> Yes()
        {
            return x => x.ReleaseDate.Year < 2010;
        }

        public override Expression<Func<Movie, bool>> No()
        {
            return x => x.ReleaseDate.Year >= 2010;
        }

        public override Expression<Func<Movie, bool>> Idk()
        {
            return x => x.ReleaseDate.Year > 2000;
        }
    }
}
