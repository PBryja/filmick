﻿using System.Collections.Generic;
using FilmickDataAccess.Model;

namespace FilmickServices.Game
{
    public class Question
    {
        public int QuestionId { get; set; } = -1;
        public string QuestionText { get; set; } = "";
        public string Description { get; set; } = "";
        public string GameId { get; set; }
        public int AvailableMovies { get; set; }
        public int MoviesAfterFilters { get; set; }
        public List<MovieVM> ProposedMovies { get; set; } = new List<MovieVM>();
        public bool IsAvailablePreviousQuestion { get; set; } = true;

        public StatusEnum StatusCode { get; set; } = StatusEnum.NoStatus;

        public enum StatusEnum
        {
            NoStatus = 0,

            Ok = 1000,
            Ok_TooEarlyForProposedOrFilteredMovies = 1010,
            Ok_NoMoreQuestions = 1020,

            Failed_NotCorrectGameId = 4000,
            Failed_GameIsAlreadyFinished = 4100,
            Failed_NotCorrectAnswer = 4200,
            Failed_AccessDenied = 4300,
            Failed_NotCorrectQuestionId = 4400,
            Failed_NotEnoughQuestionsToGetBack = 4410
        }

        public Question SetProperties(FilmickDataAccess.Model.Game game, GameQuestion gameQuestion)
        {
            return SetProperties(game.GameId.ToString(), gameQuestion);
        }

        public Question SetProperties(string gameId, GameQuestion gameQuestion)
        {
            GameId = gameId;
            QuestionText = gameQuestion.FilterQuestion?.DefaultText ?? gameQuestion.TagQuestion.DefaultText;
            QuestionId = gameQuestion.GameQuestionId;
            Description = gameQuestion.FilterQuestion?.Description ?? gameQuestion.TagQuestion.Description ?? "";

            return this;
        }
    }
}
