﻿using System;
using System.Collections.Generic;
using System.Text;
using FilmickDataAccess.Model;
using FilmickDataAccess.Model.Assocs;

namespace FilmickServices.Game
{
    public class GenreVM
    {
        public int GenreId { get; set; }

        public int ApiId { get; set; }

        public string ApiName { get; set; }

        public string DisplayName { get; set; } = "";

    }
}
