﻿using System;
using System.Collections.Generic;
using System.Linq;
using FilmickDataAccess.Model;
using FilmickRepositories;

namespace FilmickServices.Game.MovieManagers
{
    public class MatchMovieManager
    {
        private readonly IEnumerable<Movie> filteredMovies;
        protected IUnitOfWork unitOfWork;
        protected Guid idGame;
        public decimal MinimumPercentToMarkMovieAsGoodMatch { get; set; } = 60M;
        public int MinimumRequiredAmountOfQuestionsForMovie { get; set; } = 3;

        public MatchMovieManager(IUnitOfWork unitOfWork, Guid idGame, List<Movie> filteredMovies)
        {
            this.unitOfWork = unitOfWork;
            this.filteredMovies = filteredMovies;
            this.idGame = idGame;
        }

        public IEnumerable<Movie> GetGoodMatchMovies()
        {
            var allYesAnswers = unitOfWork.GameQuestionRepository.GetAllQuestionsAnsweredYes(idGame, false)
                .Select(x => x.TagQuestion)
                .ToList();

            var movieTagsOfYesAnswers = allYesAnswers.SelectMany(x => x.MovieTags).ToList();

            var toReturn = new Dictionary<Movie, decimal>();
            foreach (var movie in filteredMovies.Where(x => x.MovieTags.Count >= MinimumRequiredAmountOfQuestionsForMovie))
            {
                var countQuestionsOfMovie = movie.MovieTags.Count();
                if (countQuestionsOfMovie > 0)
                {
                    var actualPercent =
                        movieTagsOfYesAnswers.Count(x => x.MovieId == movie.MovieId) /
                        (decimal)countQuestionsOfMovie * 100;

                    if (actualPercent >= MinimumPercentToMarkMovieAsGoodMatch)
                        toReturn.Add(movie, actualPercent);
                }
            }

            return toReturn.OrderByDescending(x=>x.Value).ThenByDescending(x => x.Key.VoteAverage).Select(x=>x.Key).ToList();
        }

        public IEnumerable<Movie> GetAvailableMovies()
        {
            var allAskedQuestionsGameQuestion = unitOfWork.GameQuestionRepository.GetAllQuestions(idGame, false);

            var allAskedTagQuestionsWithYesAnswer = allAskedQuestionsGameQuestion
                .Where(x => x.Answer == GameQuestion.AnswerEnum.Yes)
                .Select(x => x.TagQuestion);

            var allAskedTagQuestionsWithNoAnswer = allAskedQuestionsGameQuestion
                .Where(x => x.Answer == GameQuestion.AnswerEnum.No)
                .Select(x => x.TagQuestion);

            var moviesToReturn = ExcludeMoviesByExcludingTypeOfAnsweredTags(allAskedTagQuestionsWithYesAnswer, allAskedTagQuestionsWithNoAnswer)
                                            .Where(x => x.MovieTags.Count >= MinimumRequiredAmountOfQuestionsForMovie);

            var availableTagQuestions = unitOfWork.TagQuestionRepository
                .GetAvailableQuestions(
                    allAskedQuestionsGameQuestion.ToDictionary(x => x.TagQuestion, y => y.Answer).ToList()
                    , moviesToReturn, true);

            moviesToReturn = moviesToReturn.Where(x => x.MovieTags.Count >= MinimumRequiredAmountOfQuestionsForMovie).ToList();

            moviesToReturn = ExcludeMoviesWithNoLeftQuestions(moviesToReturn, availableTagQuestions);
            moviesToReturn = ExcludeMoviesWithNoMoreChancesForGoodMatching(moviesToReturn, availableTagQuestions, allAskedTagQuestionsWithYesAnswer);

            return moviesToReturn;
        }

        private IEnumerable<Movie> ExcludeMoviesWithNoMoreChancesForGoodMatching(IEnumerable<Movie> movies, IEnumerable<TagQuestion> availableTagQuestions, IEnumerable<TagQuestion> allAskedTagQuestionsWithYesAnswer)
        {
            var movieTagsOfYesAnswers = allAskedTagQuestionsWithYesAnswer.SelectMany(x => x.MovieTags).ToList();
            var movieTagsOfLeftAnswers = availableTagQuestions.SelectMany(x => x.MovieTags).ToList();

            return movies.Where(x =>
            {
                int movieTagsCount = x.MovieTags.Count;
                decimal howManyYesNeedToGetMinimumPercent =
                    (movieTagsCount * (MinimumPercentToMarkMovieAsGoodMatch / 100));
                int howManyYesNeedToGetMinimumPercentCeiling = (int)Math.Ceiling(howManyYesNeedToGetMinimumPercent);

                int countLeftQuestions = x.MovieTags.Count(movieTagsOfLeftAnswers.Contains);
                int countYesAnswers = x.MovieTags.Count(movieTagsOfYesAnswers.Contains);

                if (countYesAnswers + countLeftQuestions >= howManyYesNeedToGetMinimumPercentCeiling)
                    return true;

                return false;
            });
        }

        private IEnumerable<Movie> ExcludeMoviesWithNoLeftQuestions(IEnumerable<Movie> movies, IEnumerable<TagQuestion> availableTagQuestions)
        {
            return movies.Where(x => x.MovieTags.Select(y => y.TagQuestion)
                .Any(availableTagQuestions.Contains));
        }

        private IEnumerable<Movie> ExcludeMoviesByExcludingTypeOfAnsweredTags(IEnumerable<TagQuestion> allAskedTagQuestionsWithYesAnswer, IEnumerable<TagQuestion> allAskedTagQuestionsWithNoAnswer)
        {
            var excludeMoviesWithTag = allAskedTagQuestionsWithNoAnswer.Where(x => x.ExcludeMoviesType == TagQuestion.ExcludeMovieEnum.ExcludeMoviesWithTag_WhenNoAnswer || x.ExcludeMoviesType == TagQuestion.ExcludeMovieEnum.ExcludeMoviesWithoutTag_WhenYesAnswer_OR_WithTag_WhenNoAnswer).ToList();
            var excludeMoviesWithoutTag = allAskedTagQuestionsWithYesAnswer.Where(x => x.ExcludeMoviesType == TagQuestion.ExcludeMovieEnum.ExcludeMoviesWithoutTag_WhenYesAnswer || x.ExcludeMoviesType == TagQuestion.ExcludeMovieEnum.ExcludeMoviesWithoutTag_WhenYesAnswer_OR_WithTag_WhenNoAnswer).ToList();

            var newFilteredMovies = filteredMovies;

            if (excludeMoviesWithTag.Any())
                newFilteredMovies = newFilteredMovies
                    .Where(x => x.MovieTags.Select(y => y.TagQuestion)
                    .Any(y => excludeMoviesWithTag.Contains(y)) == false);

            if (excludeMoviesWithoutTag.Any())
                newFilteredMovies = newFilteredMovies
                    .Where(x => x.MovieTags.Select(y => y.TagQuestion)
                    .Any(y => excludeMoviesWithoutTag.Contains(y)));

            return newFilteredMovies;
        }
    }
}
