﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using FilmickDataAccess.Model;
using FilmickDataAccess.Model.Enums;
using FilmickServices.Game.Filters;

namespace FilmickServices.Game.MovieManagers
{
    public class MovieFilterManager
    {
        private Dictionary<FilterEnum, Filter> filterInstances;
        private Dictionary<FilterEnum, Filter> FilterInstances
        {
            get
            {
                if (filterInstances == null)
                {
                    filterInstances = Assembly.GetExecutingAssembly().GetTypes()
                    .Where(t => typeof(Filter).IsAssignableFrom(t) && t.IsInterface == false && t.IsAbstract == false)
                    .Select(t => Activator.CreateInstance(t) as Filter).ToDictionary(x => x.GetFilterEnum, y => y);
                }

                return filterInstances;
            }
        }

        private Filter GetFilter(FilterEnum filterEnum)
        {
            if (FilterInstances.ContainsKey(filterEnum))
            {
                return FilterInstances[filterEnum];
            }

            return FilterInstances[FilterEnum.NoFilter];
        }

        private Expression<Func<Movie, bool>> GetExpressionByAnswerAndFilter(GameQuestion.AnswerEnum answerEnum, Filter filter)
        {
            return answerEnum switch
            {
                GameQuestion.AnswerEnum.Yes => filter.Yes(),
                GameQuestion.AnswerEnum.No => filter.No(),
                GameQuestion.AnswerEnum.Idk => filter.Idk(),
                _ => (x => true)
            };
        }

        public Expression<Func<Movie, bool>> GetExpressionFilterMovies(IEnumerable<GameQuestion> gameQuestions)
        {
            Expression<Func<Movie, bool>> expressionToReturn = null;

            foreach (var gameQuestion in gameQuestions)
            {
                if (gameQuestion.Answer == GameQuestion.AnswerEnum.NoAnswer || gameQuestion.FilterQuestion == null)
                    continue;

                var filter = GetFilter(gameQuestion.FilterQuestion.FilterEnum);

                var nextExpression = GetExpressionByAnswerAndFilter(gameQuestion.Answer, filter);

                if (expressionToReturn == null)
                {
                    expressionToReturn = nextExpression;
                }
                else
                {
                    expressionToReturn = ConcatExpressions(expressionToReturn, nextExpression);
                }
            }

            return expressionToReturn;
        }

        private Expression<Func<Movie, bool>> ConcatExpressions(Expression<Func<Movie, bool>> expression1, Expression<Func<Movie, bool>> expression2)
        {
            var replaceVisitor = new ReplaceParameterVisitor()
            {
                Second = expression2.Parameters[0],
                First = expression1.Parameters[0],
            };

            var newBody = replaceVisitor.Visit(expression2.Body);
            var newExpression = Expression.AndAlso(expression1.Body, newBody);
            return Expression.Lambda<Func<Movie, bool>>(newExpression, expression1.Parameters);
        }

        public Expression<Func<Movie, bool>> GetExpressionFilterEnglishMoviesWhenNoLanguageIsPicked(IEnumerable<GameQuestion> gameQuestions)
        {
            if (!gameQuestions.Any(x => x.FilterQuestion.FilterEnum == FilterEnum.Language_English))
            {
                return x => true;
            }

            var englishAnswer = gameQuestions.First(x => x.FilterQuestion.FilterEnum == FilterEnum.Language_English);

            if (englishAnswer.Answer != GameQuestion.AnswerEnum.Idk)
            {
                return x => true;
            }

            if (gameQuestions.Any(x => (int)x.FilterQuestion.FilterEnum > 200 && (int)x.FilterQuestion.FilterEnum < 300 && x.Answer == GameQuestion.AnswerEnum.Yes))
            {
                return x => true;
            }

            var newExpression = GetExpressionByAnswerAndFilter(GameQuestion.AnswerEnum.Yes, GetFilter(FilterEnum.Language_English));

            return newExpression;
        }

        public Expression<Func<Movie, bool>> GetExpressionFilterEnglishMoviesWhenNoLanguageIsPicked(Expression<Func<Movie, bool>> expressionFilter, IEnumerable<GameQuestion> gameQuestions)
        {
            var newExpression = GetExpressionFilterEnglishMoviesWhenNoLanguageIsPicked(gameQuestions);

            return ConcatExpressions(expressionFilter, newExpression);
        }

        private class ReplaceParameterVisitor : ExpressionVisitor
        {
            public ParameterExpression Second { get; set; }
            public ParameterExpression First { get; set; }

            protected override Expression VisitParameter(ParameterExpression node)
            {
                return node == Second ? First : base.VisitParameter(node);
            }
        }
    }

    
}
