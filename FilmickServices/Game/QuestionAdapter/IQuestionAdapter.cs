﻿using System;
using System.Collections.Generic;
using FilmickDataAccess.Model;

namespace FilmickServices.Game.QuestionAdapter
{
    public interface IQuestionAdapter
    {
        Question GetQuestionFromTagQuestionManager(Guid gameId, IEnumerable<Movie> movies);
        Question GetQuestionFromFilterQuestionManager(Guid gameId);
    }
}