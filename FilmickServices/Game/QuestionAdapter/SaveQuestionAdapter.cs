﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FilmickDataAccess.Model;
using FilmickRepositories;
using FilmickServices.Game;
using FilmickServices.Game.GameQuestionManagers;
using FilmickServices.Game.QuestionAdapter;

namespace Filmick.Infrastructure.QuestionAdapter
{
    public class SaveQuestionAdapter : IQuestionAdapter
    {
        private readonly IUnitOfWork unitOfWork;

        public SaveQuestionAdapter(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public Question GetQuestionFromFilterQuestionManager(Guid gameId)
        {
            var filterQuestion = new FilterQuestionManager(unitOfWork, gameId);
            var gameQuestion = filterQuestion.GetQuestion();

            if (gameQuestion?.FilterQuestion != null)
            {
                if (gameQuestion.FilterQuestionId == null || gameQuestion.FilterQuestionId <= 0)
                {
                    gameQuestion.GameId = gameId;
                    unitOfWork.GameQuestionRepository.Insert(gameQuestion);
                    unitOfWork.Save();
                }

                return new Question()
                {
                    StatusCode = Question.StatusEnum.Ok_TooEarlyForProposedOrFilteredMovies
                }.SetProperties(gameId.ToString(), gameQuestion);
            }

            return null;
        }

        public Question GetQuestionFromTagQuestionManager(Guid gameId, IEnumerable<Movie> movies)
        {
            var tagQuestion = new TagQuestionManager(unitOfWork, gameId, movies);
            var gameQuestion = tagQuestion.GetQuestion();

            if (gameQuestion?.TagQuestion != null)
            {
                if (gameQuestion.TagQuestionId == null || gameQuestion.TagQuestionId <= 0)
                {
                    gameQuestion.GameId = gameId;
                    unitOfWork.GameQuestionRepository.Insert(gameQuestion);
                    unitOfWork.Save();
                }

                return new Question()
                {
                    StatusCode = Question.StatusEnum.Ok,
                    IsAvailablePreviousQuestion = true
                }.SetProperties(gameId.ToString(), gameQuestion);
            }

            return null;
        }
    }
}
