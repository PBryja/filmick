
using FilmickDataAccess.Context;
using Filmick.Infrastructure;
using FilmickDataAccess.Model;
using FilmickDataAccess.Model.Relationships;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using FilmickRepositories;
using FilmickServices.Game.GameQuestionManagers;
using FilmickServices.Game.MovieManagers;

namespace Filmick.Tests.Infrastructure
{
    public class TagQuestionManagerTests
    {
        private DbContextOptions<FilmickContext> options;
        private TagQuestion childOfParentQuestion;
        private TagQuestion childOfMainParentQuestion;

        private TagQuestion parentQuestion;
        private TagQuestion mainParentQuestion;
        private TagQuestion nonFamilyQuestion;
        private TagQuestion nonFamilyQuestion2;

        [SetUp]
        public void Setup()
        {
            var temp = new TimeSpan(DateTime.Now.Ticks);

            options = new DbContextOptionsBuilder<FilmickContext>()
            .UseInMemoryDatabase(databaseName: "FilmickDB" + temp)
            .Options;

            var context = new FilmickContext(options);

            context.Movies.Add(new Movie { MovieId = 1, Title = "Movie 1" });
            context.Languages.Add(new Language { LanguageId = 1, EnglishName = "Polish", Iso639_1 = "pl", Name = "Polski" });
            context.SaveChanges();

            childOfParentQuestion = new TagQuestion() { TagQuestionId = 1, Name = "Child of parent" };

            parentQuestion = new TagQuestion()
            {
                TagQuestionId = 2,
                Name = "Parent 1",
                Dependents = new List<TagQuestionAssociation>() {
                        new TagQuestionAssociation() {
                            Dependent = childOfParentQuestion
                        }
                    }
            };

            childOfMainParentQuestion = new TagQuestion()
            {
                TagQuestionId = 6,
                Name = "Child of main Parent",
            };

            mainParentQuestion = new TagQuestion()
            {
                TagQuestionId = 3,
                Name = "Main parent 1",
                Dependents = new List<TagQuestionAssociation>() {
                        new TagQuestionAssociation() {
                            Dependent = parentQuestion
                        }
                    }
            };

            nonFamilyQuestion = new TagQuestion()
            {
                TagQuestionId = 4,
                Name = "Non family question"
            };


            nonFamilyQuestion2 = new TagQuestion()
            {
                TagQuestionId = 5,
                Name = "Non family question 2"
            };
        }

        private Movie AddAllTagToMovie(Movie movie, IEnumerable<TagQuestion> tagQuestions)
        {
            foreach (var tagQuestion in tagQuestions)
            {
                movie.MovieTags.Add(new FilmickDataAccess.Model.Assocs.MovieTag() { TagQuestion = tagQuestion });
            }

            return movie;
        }

        [Test]
        public void GetQuestion_AvailableAllTypesQuestionsButAllAreAnsweredAndExcluded_ReturnNull()
        {
            using (var contextTemp = new FilmickContext(options))
            {
                var game = new Game() { GameId = new Guid() };
                contextTemp.Games.Add(game);

                contextTemp.TagQuestions.Add(childOfParentQuestion);
                contextTemp.TagQuestions.Add(parentQuestion);

                var excludeParentQuestion = new TagQuestion()
                {
                    TagQuestionId = 6,
                    Name = "Child of main Parent",
                    Dependents = new List<TagQuestionAssociation>() {
                        new TagQuestionAssociation() {
                            Dependent = parentQuestion,
                            Type = TagQuestionAssociation.TypeEnum.Exclude_WhenYesAndNo
                        }
                    }
                };

                contextTemp.TagQuestions.Add(excludeParentQuestion);
                contextTemp.TagQuestions.Add(mainParentQuestion);
                contextTemp.TagQuestions.Add(nonFamilyQuestion);

                contextTemp.GameQuestions.Add(new GameQuestion() { Answer = GameQuestion.AnswerEnum.Yes, Game = game, TagQuestion = excludeParentQuestion });
                contextTemp.GameQuestions.Add(new GameQuestion() { Answer = GameQuestion.AnswerEnum.Yes, Game = game, TagQuestion = mainParentQuestion });
                contextTemp.GameQuestions.Add(new GameQuestion() { Answer = GameQuestion.AnswerEnum.Yes, Game = game, TagQuestion = nonFamilyQuestion });

                contextTemp.SaveChanges();

                var movie = contextTemp.Movies.First(x => x.MovieId == 1);
                movie = AddAllTagToMovie(movie, contextTemp.TagQuestions);
                contextTemp.SaveChanges();

                var qManager = new TagQuestionManager(new UnitOfWork(contextTemp), game.GameId, contextTemp.Movies);
                List<TagQuestion> questions = new List<TagQuestion>();
                List<TagQuestion> questionsExpected = new List<TagQuestion>();

                for (int i = 1; i <= 100; i++)
                {
                    questions.Add(qManager.GetQuestion()?.TagQuestion);
                }

                for (int i = 1; i <= 100; i++)
                {
                    questionsExpected.Add(null);
                }

                CollectionAssert.AreEqual(questionsExpected, questions);
            }
        }

        [Test]
        public void GetQuestion_AvailableOneMainParentAndOneChildTagQuestionWithNewGame_ReturnOnlyMainParentQuestion()
        {
            using (var contextTemp = new FilmickContext(options))
            {
                var game = new Game() { GameId = new Guid() };
                contextTemp.Games.Add(game);

                contextTemp.TagQuestions.Add(childOfParentQuestion);
                contextTemp.TagQuestions.Add(parentQuestion);
                contextTemp.SaveChanges();

                var movie = contextTemp.Movies.First(x => x.MovieId == 1);
                movie = AddAllTagToMovie(movie, contextTemp.TagQuestions);
                contextTemp.SaveChanges();

                var qManager = new TagQuestionManager(new UnitOfWork(contextTemp), game.GameId, contextTemp.Movies);
                List<TagQuestion> questions = new List<TagQuestion>();
                List<TagQuestion> questionsExpected = new List<TagQuestion>();

                for (int i = 1; i <= 100; i++)
                {
                    questions.Add(qManager.GetQuestion().TagQuestion);
                }

                for (int i = 1; i <= 100; i++)
                {
                    questionsExpected.Add(parentQuestion);
                }

                CollectionAssert.AreEqual(questionsExpected, questions);
            }
        }

        [Test]
        public void GetQuestion_AvailableOneMainParentAndOneParentAndOneChildTagQuestionWithNewGame_ReturnOnlyMainParentQuestion()
        {
            using (var contextTemp = new FilmickContext(options))
            {
                var game = new Game() { GameId = new Guid() };
                contextTemp.Games.Add(game);
                contextTemp.TagQuestions.Add(childOfParentQuestion);
                contextTemp.TagQuestions.Add(parentQuestion);
                contextTemp.TagQuestions.Add(mainParentQuestion);

                contextTemp.SaveChanges();

                var movie = contextTemp.Movies.First(x => x.MovieId == 1);
                movie = AddAllTagToMovie(movie, contextTemp.TagQuestions);
                contextTemp.SaveChanges();

                var qManager = new TagQuestionManager(new UnitOfWork(contextTemp), game.GameId, contextTemp.Movies);
                List<TagQuestion> questions = new List<TagQuestion>();
                List<TagQuestion> questionsExpected = new List<TagQuestion>();

                for (int i = 1; i <= 100; i++)
                {
                    questions.Add(qManager.GetQuestion().TagQuestion);
                }

                for (int i = 1; i <= 100; i++)
                {
                    questionsExpected.Add(mainParentQuestion);
                }

                CollectionAssert.AreEqual(questionsExpected, questions);
            }
        }

        [Test]
        public void GetQuestion_AvailableAllTypesButMainParentHasAnswerNo_ReturnOnlyNonFamilyQuestion()
        {
            using (var contextTemp = new FilmickContext(options))
            {
                var game = new Game() { GameId = new Guid() };
                contextTemp.Games.Add(game);

                contextTemp.TagQuestions.Add(childOfParentQuestion);
                contextTemp.TagQuestions.Add(parentQuestion);
                contextTemp.TagQuestions.Add(mainParentQuestion);
                contextTemp.TagQuestions.Add(nonFamilyQuestion);

                contextTemp.GameQuestions.Add(new GameQuestion() { Game = game, TagQuestion = mainParentQuestion, Answer = GameQuestion.AnswerEnum.No });

                contextTemp.SaveChanges();

                var movie = contextTemp.Movies.First(x => x.MovieId == 1);
                movie = AddAllTagToMovie(movie, contextTemp.TagQuestions);
                contextTemp.SaveChanges();

                var qManager = new TagQuestionManager(new UnitOfWork(contextTemp), game.GameId, contextTemp.Movies);
                List<TagQuestion> questions = new List<TagQuestion>();
                List<TagQuestion> questionsExpected = new List<TagQuestion>();

                for (int i = 1; i <= 100; i++)
                {
                    questions.Add(qManager.GetQuestion().TagQuestion);
                }

                for (int i = 1; i <= 100; i++)
                {
                    questionsExpected.Add(nonFamilyQuestion);
                }

                CollectionAssert.AreEqual(questionsExpected, questions);
            }
        }

        [Test]
        public void GetQuestion_AvailableTwoNonFamilyQuestionButOneWasAlreadyAnswered_ReturnOnlyNonFamilyQuestion()
        {
            using (var contextTemp = new FilmickContext(options))
            {
                var game = new Game() { GameId = new Guid() };
                contextTemp.Games.Add(game);

                contextTemp.TagQuestions.Add(nonFamilyQuestion);
                contextTemp.TagQuestions.Add(nonFamilyQuestion2);

                contextTemp.GameQuestions.Add(new GameQuestion() { Game = game, TagQuestion = nonFamilyQuestion, Answer = GameQuestion.AnswerEnum.Yes });

                contextTemp.SaveChanges();

                var movie = contextTemp.Movies.First(x => x.MovieId == 1);
                movie = AddAllTagToMovie(movie, contextTemp.TagQuestions);
                contextTemp.SaveChanges();

                var qManager = new TagQuestionManager(new UnitOfWork(contextTemp), game.GameId, contextTemp.Movies);
                List<TagQuestion> questions = new List<TagQuestion>();
                List<TagQuestion> questionsExpected = new List<TagQuestion>();

                for (int i = 1; i <= 100; i++)
                {
                    questions.Add(qManager.GetQuestion().TagQuestion);
                }

                for (int i = 1; i <= 100; i++)
                {
                    questionsExpected.Add(nonFamilyQuestion2);
                }

                CollectionAssert.AreEqual(questionsExpected, questions);
            }
        }

        [Test]
        public void GetQuestion_AvailableAllTypesQuestionsWithNewGame_ReturnMainParentOrNonFamilyQuestion()
        {
            using (var contextTemp = new FilmickContext(options))
            {
                var game = new Game() { GameId = new Guid() };
                contextTemp.Games.Add(game);

                contextTemp.TagQuestions.Add(childOfParentQuestion);
                contextTemp.TagQuestions.Add(parentQuestion);
                contextTemp.TagQuestions.Add(mainParentQuestion);
                contextTemp.TagQuestions.Add(nonFamilyQuestion);

                contextTemp.SaveChanges();

                var movie = contextTemp.Movies.First(x => x.MovieId == 1);
                movie = AddAllTagToMovie(movie, contextTemp.TagQuestions);
                contextTemp.SaveChanges();

                var qManager = new TagQuestionManager(new UnitOfWork(contextTemp), game.GameId, contextTemp.Movies);
                List<TagQuestion> questions = new List<TagQuestion>();
                List<TagQuestion> questionsExpected = new List<TagQuestion>();

                for (int i = 1; i <= 500; i++)
                {
                    questions.Add(qManager.GetQuestion()?.TagQuestion);
                }

                questionsExpected.Add(mainParentQuestion);
                questionsExpected.Add(nonFamilyQuestion);

                questions = questions.Distinct().OrderBy(x => x.TagQuestionId).ToList();

                CollectionAssert.AreEqual(questionsExpected, questions);
            }
        }

        [Test]
        public void GetQuestion_NonAnsweredLastQuestion_ReturnLastQuestion()
        {
            using (var contextTemp = new FilmickContext(options))
            {
                var game = new Game() { GameId = new Guid() };
                contextTemp.Games.Add(game);

                contextTemp.TagQuestions.Add(childOfParentQuestion);
                contextTemp.TagQuestions.Add(parentQuestion);
                contextTemp.TagQuestions.Add(mainParentQuestion);
                contextTemp.TagQuestions.Add(nonFamilyQuestion);

                contextTemp.GameQuestions.Add(new GameQuestion() { Game = game, TagQuestion = childOfParentQuestion, Answer = GameQuestion.AnswerEnum.NoAnswer });

                contextTemp.SaveChanges(); var movie = contextTemp.Movies.First(x => x.MovieId == 1); movie = AddAllTagToMovie(movie, contextTemp.TagQuestions); contextTemp.SaveChanges();

                var qManager = new TagQuestionManager(new UnitOfWork(contextTemp), game.GameId, contextTemp.Movies);
                List<TagQuestion> questions = new List<TagQuestion>();
                List<TagQuestion> questionsExpected = new List<TagQuestion>();

                for (int i = 1; i <= 100; i++)
                {
                    questions.Add(qManager.GetQuestion().TagQuestion);
                }

                for (int i = 1; i <= 100; i++)
                {
                    questionsExpected.Add(childOfParentQuestion);
                }

                CollectionAssert.AreEqual(questionsExpected, questions);
            }
        }

        [Test]
        public void GetQuestion_AvailableAllTypeQuestionsButMainParentQuestionWasAnsweredYes_ReturnParentQuestion()
        {
            using (var contextTemp = new FilmickContext(options))
            {
                var game = new Game() { GameId = new Guid() };
                contextTemp.Games.Add(game);


                contextTemp.TagQuestions.Add(childOfParentQuestion);
                contextTemp.TagQuestions.Add(parentQuestion);
                contextTemp.TagQuestions.Add(mainParentQuestion);
                contextTemp.TagQuestions.Add(nonFamilyQuestion);

                contextTemp.GameQuestions.Add(new GameQuestion() { Game = game, Answer = GameQuestion.AnswerEnum.Yes, TagQuestion = mainParentQuestion });

                contextTemp.SaveChanges(); var movie = contextTemp.Movies.First(x => x.MovieId == 1); movie = AddAllTagToMovie(movie, contextTemp.TagQuestions); contextTemp.SaveChanges();

                var qManager = new TagQuestionManager(new UnitOfWork(contextTemp), game.GameId, contextTemp.Movies);
                List<TagQuestion> questions = new List<TagQuestion>();
                List<TagQuestion> questionsExpected = new List<TagQuestion>();

                for (int i = 1; i <= 100; i++)
                {
                    questions.Add(qManager.GetQuestion().TagQuestion);
                }

                for (int i = 1; i <= 100; i++)
                {
                    questionsExpected.Add(parentQuestion);
                }



                CollectionAssert.AreEqual(questionsExpected, questions);
            }
        }

        private static object[] ExcludedCases =
        {
            new object[] { TagQuestionAssociation.TypeEnum.Exclude_WhenOnlyYes, GameQuestion.AnswerEnum.Yes },
            new object[] { TagQuestionAssociation.TypeEnum.Exclude_WhenYesAndIdk, GameQuestion.AnswerEnum.Yes },
            new object[] { TagQuestionAssociation.TypeEnum.Exclude_WhenYesAndIdk, GameQuestion.AnswerEnum.Idk },
            new object[] { TagQuestionAssociation.TypeEnum.Exclude_WhenYesAndNo, GameQuestion.AnswerEnum.Yes },
            new object[] { TagQuestionAssociation.TypeEnum.Exclude_WhenYesAndNo, GameQuestion.AnswerEnum.No },

            new object[] { TagQuestionAssociation.TypeEnum.Exclude_WhenOnlyNo, GameQuestion.AnswerEnum.No },
            new object[] { TagQuestionAssociation.TypeEnum.Exclude_WhenNoAndIdk, GameQuestion.AnswerEnum.No },
            new object[] { TagQuestionAssociation.TypeEnum.Exclude_WhenNoAndIdk, GameQuestion.AnswerEnum.Idk },

            new object[] { TagQuestionAssociation.TypeEnum.Exclude_WhenOnlyIdk, GameQuestion.AnswerEnum.Idk },
        };

        [TestCaseSource(nameof(ExcludedCases))]
        [Test]
        public void GetQuestion_AvailableAllTypeQuestionsOneQuestionAnsweredWithExcludeQuestions_ReturnNonExcludedQuestion(TagQuestionAssociation.TypeEnum typeEnum, GameQuestion.AnswerEnum answerEnum)
        {
            using (var contextTemp = new FilmickContext(options))
            {
                var game = new Game() { GameId = new Guid() };
                contextTemp.Games.Add(game);


                contextTemp.TagQuestions.Add(childOfParentQuestion);
                contextTemp.TagQuestions.Add(parentQuestion);
                contextTemp.TagQuestions.Add(mainParentQuestion);
                contextTemp.TagQuestions.Add(nonFamilyQuestion);

                nonFamilyQuestion2.Dependents.Add(new TagQuestionAssociation() { Dependent = mainParentQuestion, Type = typeEnum });
                contextTemp.TagQuestions.Add(nonFamilyQuestion2);

                contextTemp.GameQuestions.Add(new GameQuestion() { Game = game, Answer = answerEnum, TagQuestion = nonFamilyQuestion2 });

                contextTemp.SaveChanges(); var movie = contextTemp.Movies.First(x => x.MovieId == 1); movie = AddAllTagToMovie(movie, contextTemp.TagQuestions); contextTemp.SaveChanges();

                var qManager = new TagQuestionManager(new UnitOfWork(contextTemp), game.GameId, contextTemp.Movies);
                List<TagQuestion> questions = new List<TagQuestion>();
                List<TagQuestion> questionsExpected = new List<TagQuestion>();

                for (int i = 1; i <= 100; i++)
                {
                    questions.Add(qManager.GetQuestion().TagQuestion);
                }

                for (int i = 1; i <= 100; i++)
                {
                    questionsExpected.Add(nonFamilyQuestion);
                }



                CollectionAssert.AreEqual(questionsExpected, questions);
            }
        }

        [TestCaseSource(nameof(ExcludedCases))]
        [Test]
        public void GetQuestion_AvailableAllTypeQuestionsOneOfChildQuestionAnsweredWithExcludeAnotherChildQuestion_ReturnNonChildNonExcludedQuestion(TagQuestionAssociation.TypeEnum typeEnum, GameQuestion.AnswerEnum answerEnum)
        {
            using (var contextTemp = new FilmickContext(options))
            {
                var game = new Game() { GameId = new Guid() };
                contextTemp.Games.Add(game);


                contextTemp.TagQuestions.Add(childOfParentQuestion);
                contextTemp.TagQuestions.Add(parentQuestion);

                childOfMainParentQuestion.Dependents.Add(new TagQuestionAssociation() { Dependent = parentQuestion, Type = typeEnum });
                contextTemp.TagQuestions.Add(childOfMainParentQuestion);

                mainParentQuestion.Dependents.Add(new TagQuestionAssociation() { Dependent = childOfMainParentQuestion, Type = TagQuestionAssociation.TypeEnum.Parent_child });
                contextTemp.TagQuestions.Add(mainParentQuestion);
                contextTemp.TagQuestions.Add(nonFamilyQuestion);

                contextTemp.GameQuestions.Add(new GameQuestion() { Game = game, Answer = GameQuestion.AnswerEnum.Yes, TagQuestion = mainParentQuestion });
                contextTemp.GameQuestions.Add(new GameQuestion() { Game = game, Answer = answerEnum, TagQuestion = childOfMainParentQuestion });

                contextTemp.SaveChanges(); var movie = contextTemp.Movies.First(x => x.MovieId == 1); movie = AddAllTagToMovie(movie, contextTemp.TagQuestions); contextTemp.SaveChanges();

                var qManager = new TagQuestionManager(new UnitOfWork(contextTemp), game.GameId, contextTemp.Movies);
                List<TagQuestion> questions = new List<TagQuestion>();
                List<TagQuestion> questionsExpected = new List<TagQuestion>();

                for (int i = 1; i <= 100; i++)
                {
                    questions.Add(qManager.GetQuestion().TagQuestion);
                }

                for (int i = 1; i <= 100; i++)
                {
                    questionsExpected.Add(nonFamilyQuestion);
                }



                CollectionAssert.AreEqual(questionsExpected, questions);
            }
        }


        private static object[] NonExcludedCases =
        {
            new object[] { TagQuestionAssociation.TypeEnum.Exclude_WhenOnlyYes, GameQuestion.AnswerEnum.No },
            new object[] { TagQuestionAssociation.TypeEnum.Exclude_WhenOnlyYes, GameQuestion.AnswerEnum.Idk },
            new object[] { TagQuestionAssociation.TypeEnum.Exclude_WhenYesAndIdk, GameQuestion.AnswerEnum.No },
            new object[] { TagQuestionAssociation.TypeEnum.Exclude_WhenYesAndNo, GameQuestion.AnswerEnum.Idk },

            new object[] { TagQuestionAssociation.TypeEnum.Exclude_WhenOnlyNo, GameQuestion.AnswerEnum.Yes },
            new object[] { TagQuestionAssociation.TypeEnum.Exclude_WhenOnlyNo, GameQuestion.AnswerEnum.Idk },
            new object[] { TagQuestionAssociation.TypeEnum.Exclude_WhenNoAndIdk, GameQuestion.AnswerEnum.Yes },

            new object[] { TagQuestionAssociation.TypeEnum.Exclude_WhenOnlyIdk, GameQuestion.AnswerEnum.Yes },
            new object[] { TagQuestionAssociation.TypeEnum.Exclude_WhenOnlyIdk, GameQuestion.AnswerEnum.No },
        };

        [TestCaseSource(nameof(NonExcludedCases))]
        [Test]
        public void GetQuestion_AvailableAllTypeQuestionsOneQuestionAnsweredWithExcludeQuestionsButAnotherAnswer_ReturnNonExcludedQuestion(TagQuestionAssociation.TypeEnum typeEnum, GameQuestion.AnswerEnum answerEnum)
        {
            using (var contextTemp = new FilmickContext(options))
            {
                var game = new Game() { GameId = new Guid() };
                contextTemp.Games.Add(game);


                contextTemp.TagQuestions.Add(childOfParentQuestion);
                contextTemp.TagQuestions.Add(parentQuestion);

                mainParentQuestion.Dependents.Add(new TagQuestionAssociation() { Dependent = childOfMainParentQuestion, Type = TagQuestionAssociation.TypeEnum.Parent_child });
                contextTemp.TagQuestions.Add(mainParentQuestion);

                nonFamilyQuestion2.Dependents.Add(new TagQuestionAssociation() { Dependent = mainParentQuestion, Type = typeEnum });
                contextTemp.TagQuestions.Add(nonFamilyQuestion2);

                contextTemp.GameQuestions.Add(new GameQuestion() { Game = game, Answer = answerEnum, TagQuestion = nonFamilyQuestion2 });

                contextTemp.SaveChanges(); var movie = contextTemp.Movies.First(x => x.MovieId == 1); movie = AddAllTagToMovie(movie, contextTemp.TagQuestions); contextTemp.SaveChanges();

                var qManager = new TagQuestionManager(new UnitOfWork(contextTemp), game.GameId, contextTemp.Movies);
                List<TagQuestion> questions = new List<TagQuestion>();
                List<TagQuestion> questionsExpected = new List<TagQuestion>();

                for (int i = 1; i <= 100; i++)
                {
                    questions.Add(qManager.GetQuestion().TagQuestion);
                }

                for (int i = 1; i <= 100; i++)
                {
                    questionsExpected.Add(mainParentQuestion);
                }



                CollectionAssert.AreEqual(questionsExpected, questions);
            }
        }

        [TestCaseSource(nameof(NonExcludedCases))]
        [Test]
        public void GetQuestion_AvailableAllTypeQuestionsOneOfchildOfParentQuestionAnsweredWithExcludeAnotherchildOfParentQuestionButGaveAnotherAnswer_ReturnAnotherchildOfParentQuestion(TagQuestionAssociation.TypeEnum typeEnum, GameQuestion.AnswerEnum answerEnum)
        {
            using (var contextTemp = new FilmickContext(options))
            {
                var game = new Game() { GameId = new Guid() };
                contextTemp.Games.Add(game);


                contextTemp.TagQuestions.Add(childOfParentQuestion);
                contextTemp.TagQuestions.Add(parentQuestion);
                childOfMainParentQuestion.Dependents.Add(new TagQuestionAssociation() { Dependent = parentQuestion, Type = typeEnum });
                contextTemp.TagQuestions.Add(childOfMainParentQuestion);
                mainParentQuestion.Dependents.Add(new TagQuestionAssociation() { Dependent = childOfMainParentQuestion, Type = TagQuestionAssociation.TypeEnum.Parent_child });
                contextTemp.TagQuestions.Add(mainParentQuestion);
                contextTemp.TagQuestions.Add(nonFamilyQuestion);

                contextTemp.GameQuestions.Add(new GameQuestion() { Game = game, Answer = GameQuestion.AnswerEnum.Yes, TagQuestion = mainParentQuestion });
                contextTemp.GameQuestions.Add(new GameQuestion() { Game = game, Answer = answerEnum, TagQuestion = childOfMainParentQuestion });

                contextTemp.SaveChanges(); var movie = contextTemp.Movies.First(x => x.MovieId == 1); movie = AddAllTagToMovie(movie, contextTemp.TagQuestions); contextTemp.SaveChanges();

                var qManager = new TagQuestionManager(new UnitOfWork(contextTemp), game.GameId, contextTemp.Movies);
                List<TagQuestion> questions = new List<TagQuestion>();
                List<TagQuestion> questionsExpected = new List<TagQuestion>();

                for (int i = 1; i <= 100; i++)
                {
                    questions.Add(qManager.GetQuestion().TagQuestion);
                }

                for (int i = 1; i <= 100; i++)
                {
                    questionsExpected.Add(parentQuestion);
                }



                CollectionAssert.AreEqual(questionsExpected, questions);
            }
        }

        private List<TagQuestion> GetMainParentQuestions(int addToId, int howManyQuestions)
        {
            var list = new List<TagQuestion>();

            for (int i = 1; i <= howManyQuestions; i++)
            {
                list.Add(new TagQuestion() { TagQuestionId = addToId + i, Name = "TagQuestion " + (addToId + i) });
            }

            return list;
        }
        private List<TagQuestion> GetChildQuestionsToParent(int addToId, int howManyQuestions, TagQuestion parentQuestion)
        {
            var list = new List<TagQuestion>();

            for (int i = 1; i <= howManyQuestions; i++)
            {
                list.Add(new TagQuestion()
                {
                    TagQuestionId = addToId + i,
                    Name = "ChildQuestion " + (addToId + i),
                    DependentTo = new List<TagQuestionAssociation> {
                        new TagQuestionAssociation() { MainId = parentQuestion.TagQuestionId, Type = TagQuestionAssociation.TypeEnum.Parent_child } }
                });
            }

            return list;
        }

        [Test]
        public void GetQuestion_IncludeOnlyTop10MainParentQuestions_DontReturnNonTopQuestion()
        {
            using (var contextTemp = new FilmickContext(options))
            {
                var game = new Game() { GameId = new Guid() };
                contextTemp.Games.Add(game);

                contextTemp.TagQuestions.AddRange(GetMainParentQuestions(0, 100));

                contextTemp.SaveChanges();
                var movie = contextTemp.Movies.First(x => x.MovieId == 1);
                movie = AddAllTagToMovie(movie, contextTemp.TagQuestions);
                contextTemp.SaveChanges();

                var movie2 = new Movie() { MovieId = 2, OriginalTitle = "Movie 2" };
                movie2 = AddAllTagToMovie(movie2, contextTemp.TagQuestions.Take(10));
                contextTemp.Movies.Add(movie2);
                contextTemp.SaveChanges();

                var qManager = new TagQuestionManager(new UnitOfWork(contextTemp), game.GameId, contextTemp.Movies);
                List<TagQuestion> questions = new List<TagQuestion>();
                List<TagQuestion> questionsExpected = new List<TagQuestion>();

                for (int i = 1; i <= 400; i++)
                {
                    questions.Add(qManager.GetQuestion().TagQuestion);
                }

                for (int i = 1; i <= 100; i++)
                {
                    questionsExpected.Add(parentQuestion);
                }

                Assert.LessOrEqual(questions.OrderByDescending(x => x.TagQuestionId).First().TagQuestionId, 10);
            }
        }

        [Test]
        public void GetQuestion_Exists10TopQuestionsButOneIsAnswered_ReturnOneFromNonTopQuestions()
        {
            using (var contextTemp = new FilmickContext(options))
            {
                var game = new Game() { GameId = new Guid() };
                contextTemp.Games.Add(game);

                contextTemp.TagQuestions.AddRange(GetMainParentQuestions(0, 100));

                contextTemp.SaveChanges();
                var movie = contextTemp.Movies.First(x => x.MovieId == 1);
                movie = AddAllTagToMovie(movie, contextTemp.TagQuestions);
                contextTemp.SaveChanges();

                var movie2 = new Movie() { MovieId = 2, OriginalTitle = "Movie 2" };
                movie2 = AddAllTagToMovie(movie2, contextTemp.TagQuestions.Take(10));
                contextTemp.Movies.Add(movie2);
                contextTemp.GameQuestions.Add(new GameQuestion() { Game = game, Answer = GameQuestion.AnswerEnum.Yes, TagQuestion = contextTemp.TagQuestions.OrderBy(x => x.TagQuestionId).First() });

                contextTemp.SaveChanges();

                var qManager = new TagQuestionManager(new UnitOfWork(contextTemp), game.GameId, contextTemp.Movies);
                List<TagQuestion> questions = new List<TagQuestion>();
                List<TagQuestion> questionsExpected = new List<TagQuestion>();

                for (int i = 1; i <= 400; i++)
                {
                    var q = qManager.GetQuestion().TagQuestion;
                    questions.Add(q);

                    if (q.TagQuestionId >= 11)
                        break;
                }

                Assert.GreaterOrEqual(questions.OrderByDescending(x => x.TagQuestionId).First().TagQuestionId, 11);
            }
        }

        [Test]
        public void GetQuestion_ExcludeMoviesWithoutTag_WhenYesAnswer_OnlyMovieIsExcluded_ReturnNull()
        {
            using (var contextTemp = new FilmickContext(options))
            {
                var game = new Game() { GameId = new Guid() };
                contextTemp.Games.Add(game);

                var questionsToAdd = GetMainParentQuestions(0, 3);

                questionsToAdd.First().ExcludeMoviesType = TagQuestion.ExcludeMovieEnum.ExcludeMoviesWithoutTag_WhenYesAnswer;

                contextTemp.TagQuestions.AddRange(questionsToAdd);

                contextTemp.SaveChanges();
                var movie = contextTemp.Movies.First(x => x.MovieId == 1);
                movie = AddAllTagToMovie(movie, contextTemp.TagQuestions.Where(x => x.TagQuestionId == 1));
                contextTemp.SaveChanges();

                var movie2 = new Movie() { MovieId = 2, OriginalTitle = "Movie 2" };
                movie2 = AddAllTagToMovie(movie2, contextTemp.TagQuestions.Where(x => x.TagQuestionId >= 2));
                contextTemp.Movies.Add(movie2);
                contextTemp.GameQuestions.Add(new GameQuestion() { Game = game, Answer = GameQuestion.AnswerEnum.Yes, TagQuestion = contextTemp.TagQuestions.OrderBy(x => x.TagQuestionId).First() });

                contextTemp.SaveChanges();
                var unitOfWork = new UnitOfWork(contextTemp);
                var matchMovieManager = new MatchMovieManager(unitOfWork, game.GameId, contextTemp.Movies.ToList());

                var moviesToFilter = matchMovieManager.GetAvailableMovies();

                var qManager = new TagQuestionManager(unitOfWork, game.GameId, moviesToFilter);
                List<TagQuestion> questions = new List<TagQuestion>();
                List<TagQuestion> questionsExpected = new List<TagQuestion>();

                for (int i = 1; i <= 400; i++)
                {
                    var q = qManager.GetQuestion()?.TagQuestion;
                    questions.Add(q);
                }

                questions = questions.Distinct().ToList();
                questionsExpected.Add(null);

                CollectionAssert.AreEqual(questionsExpected, questions);
            }
        }

        [Test]
        public void GetQuestion_ExcludeMoviesWithoutTag_WhenYesAnswer_OnlyMovieIncludeTag_ReturnLeftQuestionToThisMovie()
        {
            using (var contextTemp = new FilmickContext(options))
            {
                var game = new Game() { GameId = new Guid() };
                contextTemp.Games.Add(game);

                var questionsToAdd = GetMainParentQuestions(0, 3);

                questionsToAdd.First().ExcludeMoviesType = TagQuestion.ExcludeMovieEnum.ExcludeMoviesWithoutTag_WhenYesAnswer;

                contextTemp.TagQuestions.AddRange(questionsToAdd);

                contextTemp.SaveChanges();
                var movie = contextTemp.Movies.First(x => x.MovieId == 1);
                movie = AddAllTagToMovie(movie, contextTemp.TagQuestions.Where(x => x.TagQuestionId == 1));
                contextTemp.SaveChanges();

                var movie2 = new Movie() { MovieId = 2, OriginalTitle = "Movie 2" };
                movie2 = AddAllTagToMovie(movie2, contextTemp.TagQuestions.Where(x => x.TagQuestionId <= 2));
                contextTemp.Movies.Add(movie2);
                contextTemp.GameQuestions.Add(new GameQuestion() { Game = game, Answer = GameQuestion.AnswerEnum.Yes, TagQuestion = contextTemp.TagQuestions.OrderBy(x => x.TagQuestionId).First() });

                contextTemp.SaveChanges();

                var qManager = new TagQuestionManager(new UnitOfWork(contextTemp), game.GameId, contextTemp.Movies);
                List<TagQuestion> questions = new List<TagQuestion>();
                List<TagQuestion> questionsExpected = new List<TagQuestion>();

                for (int i = 1; i <= 400; i++)
                {
                    var q = qManager.GetQuestion().TagQuestion;
                    questions.Add(q);
                }

                questions = questions.Distinct().ToList();
                questionsExpected.Add(contextTemp.TagQuestions.First(x => x.TagQuestionId == 2));

                CollectionAssert.AreEqual(questionsExpected, questions);
            }
        }

        [Test]
        public void GetQuestion_ExcludeMoviesWithTag_WhenNoAnswer_OnlyMovieIsExcluded_ReturnNull()
        {
            using (var contextTemp = new FilmickContext(options))
            {
                var game = new Game() { GameId = new Guid() };
                contextTemp.Games.Add(game);

                var questionsToAdd = GetMainParentQuestions(0, 3);

                questionsToAdd.First().ExcludeMoviesType = TagQuestion.ExcludeMovieEnum.ExcludeMoviesWithTag_WhenNoAnswer;

                contextTemp.TagQuestions.AddRange(questionsToAdd);

                contextTemp.SaveChanges();
                var movie = contextTemp.Movies.First(x => x.MovieId == 1);
                movie = AddAllTagToMovie(movie, contextTemp.TagQuestions.Where(x => x.TagQuestionId == 1));
                contextTemp.SaveChanges();

                var movie2 = new Movie() { MovieId = 2, OriginalTitle = "Movie 2" };
                movie2 = AddAllTagToMovie(movie2, contextTemp.TagQuestions);
                contextTemp.Movies.Add(movie2);
                contextTemp.GameQuestions.Add(new GameQuestion() { Game = game, Answer = GameQuestion.AnswerEnum.No, TagQuestion = contextTemp.TagQuestions.OrderBy(x => x.TagQuestionId).First() });

                contextTemp.SaveChanges();

                var unitOfWork = new UnitOfWork(contextTemp);
                var matchMovieManager = new MatchMovieManager(unitOfWork, game.GameId, contextTemp.Movies.ToList());

                var moviesToFilter = matchMovieManager.GetAvailableMovies();
                var qManager = new TagQuestionManager(unitOfWork, game.GameId, moviesToFilter);

                List<TagQuestion> questions = new List<TagQuestion>();
                List<TagQuestion> questionsExpected = new List<TagQuestion>();

                for (int i = 1; i <= 400; i++)
                {
                    var q = qManager.GetQuestion()?.TagQuestion;
                    questions.Add(q);
                }

                questions = questions.Distinct().ToList();
                questionsExpected.Add(null);

                CollectionAssert.AreEqual(questionsExpected, questions);
            }
        }

        [Test]
        public void GetQuestion_ExcludeMoviesWithTag_WhenNoAnswer_OnlyMovieDontIncludeTag_ReturnLeftQuestionToThisMovie()
        {
            using (var contextTemp = new FilmickContext(options))
            {
                var game = new Game() { GameId = new Guid() };
                contextTemp.Games.Add(game);

                var questionsToAdd = GetMainParentQuestions(0, 3);

                questionsToAdd.First().ExcludeMoviesType = TagQuestion.ExcludeMovieEnum.ExcludeMoviesWithTag_WhenNoAnswer;

                contextTemp.TagQuestions.AddRange(questionsToAdd);

                contextTemp.SaveChanges();
                var movie = contextTemp.Movies.First(x => x.MovieId == 1);
                movie = AddAllTagToMovie(movie, contextTemp.TagQuestions.Where(x => x.TagQuestionId == 1));
                contextTemp.SaveChanges();

                var movie2 = new Movie() { MovieId = 2, OriginalTitle = "Movie 2" };
                movie2 = AddAllTagToMovie(movie2, contextTemp.TagQuestions.Where(x => x.TagQuestionId == 2));
                contextTemp.Movies.Add(movie2);
                contextTemp.GameQuestions.Add(new GameQuestion() { Game = game, Answer = GameQuestion.AnswerEnum.No, TagQuestion = contextTemp.TagQuestions.OrderBy(x => x.TagQuestionId).First() });

                contextTemp.SaveChanges();

                var qManager = new TagQuestionManager(new UnitOfWork(contextTemp), game.GameId, contextTemp.Movies);
                List<TagQuestion> questions = new List<TagQuestion>();
                List<TagQuestion> questionsExpected = new List<TagQuestion>();

                for (int i = 1; i <= 400; i++)
                {
                    var q = qManager.GetQuestion().TagQuestion;
                    questions.Add(q);
                }

                questions = questions.Distinct().ToList();
                questionsExpected.Add(contextTemp.TagQuestions.First(x => x.TagQuestionId == 2));

                CollectionAssert.AreEqual(questionsExpected, questions);
            }
        }

        [Test]
        public void GetQuestion_ParentAnsweredYesAndLeft2ChildQuestionsButOneIsInExcludedMovie_ReturnOneChildQuestionOfNonExcludedMovie()
        {
            using (var contextTemp = new FilmickContext(options))
            {
                var game = new Game() { GameId = new Guid() };
                contextTemp.Games.Add(game);

                var questionsParentToAdd = GetMainParentQuestions(0, 2);
                var questionsChildToAdd = GetChildQuestionsToParent(10, 2, questionsParentToAdd.First(x => x.TagQuestionId == 1));

                contextTemp.TagQuestions.AddRange(questionsParentToAdd);
                contextTemp.TagQuestions.AddRange(questionsChildToAdd);

                contextTemp.SaveChanges();
                var movie = contextTemp.Movies.First(x => x.MovieId == 1);
                movie = AddAllTagToMovie(movie, contextTemp.TagQuestions.Where(x => x.TagQuestionId != 11));
                contextTemp.SaveChanges();

                var movie2 = new Movie() { MovieId = 2, OriginalTitle = "Movie 2" };
                movie2 = AddAllTagToMovie(movie2, contextTemp.TagQuestions);
                contextTemp.Movies.Add(movie2);
                contextTemp.GameQuestions.Add(new GameQuestion() { Game = game, Answer = GameQuestion.AnswerEnum.Yes, TagQuestion = contextTemp.TagQuestions.OrderBy(x => x.TagQuestionId).First() });

                contextTemp.SaveChanges();

                var qManager = new TagQuestionManager(new UnitOfWork(contextTemp), game.GameId, contextTemp.Movies.Where(x => x.MovieId == 1));
                List<TagQuestion> questions = new List<TagQuestion>();
                List<TagQuestion> questionsExpected = new List<TagQuestion>();

                for (int i = 1; i <= 400; i++)
                {
                    var q = qManager.GetQuestion().TagQuestion;
                    questions.Add(q);
                }

                questions = questions.Distinct().ToList();
                questionsExpected.Add(contextTemp.TagQuestions.First(x => x.TagQuestionId == 12));

                CollectionAssert.AreEqual(questionsExpected, questions);
            }
        }
        public void doSomething(IEnumerable<int> b)
        {
            List<int> _b = b.ToList();
        }
    }
}