﻿using FilmickDataAccess.Context;
using FilmickDataAccess.Model;
using FilmickDataAccess.Model.Assocs;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using FilmickRepositories;
using FilmickServices.Game.MovieManagers;

namespace Filmick.Infrastructure
{
    public class FilterMovieManagerTests
    {
        private DbContextOptions<FilmickContext> options;
        MovieFilterManager filterMovieManager;

        [SetUp]
        public void Setup()
        {
            var temp = new TimeSpan(DateTime.Now.Ticks);

            options = new DbContextOptionsBuilder<FilmickContext>()
            .UseInMemoryDatabase(databaseName: "FilmickDB" + temp)
            .Options;

            var context = new FilmickContext(options);
            context.Genres.Add(new Genre() { GenreId = 1, ApiName = "Comedy" });
            context.Genres.Add(new Genre() { GenreId = 2, ApiName = "Horror" });
            context.Genres.Add(new Genre() { GenreId = 3, ApiName = "Mystery" });

            context.Movies.Add(new Movie { MovieId = 1, VoteCount = 100, Title = "Movie 1", OriginalLanguage = "pl", ReleaseDate = new DateTime(1950, 10, 10), MovieGenres = new List<MovieGenre> { new MovieGenre() { GenreId = 1 } } });
            context.Movies.Add(new Movie { MovieId = 2, VoteCount = 10000, Title = "Movie 2", OriginalLanguage = "en", ReleaseDate = new DateTime(2011, 10, 10), MovieGenres = new List<MovieGenre> { new MovieGenre() { GenreId = 2 }, new MovieGenre() { GenreId = 3 } } });
            context.Movies.Add(new Movie { MovieId = 3, VoteCount = 3, Title = "Movie 3", OriginalLanguage = "fr", ReleaseDate = new DateTime(DateTime.Now.Year, 1, 1), MovieGenres = new List<MovieGenre> { new MovieGenre() { GenreId = 2 } } });

            context.FilterQuestions.Add(new FilterQuestion() { FilterEnum = FilmickDataAccess.Model.Enums.FilterEnum.Newest, FilterQuestionId = 1 });
            context.FilterQuestions.Add(new FilterQuestion() { FilterEnum = FilmickDataAccess.Model.Enums.FilterEnum.Known, FilterQuestionId = 2 });
            context.FilterQuestions.Add(new FilterQuestion() { FilterEnum = FilmickDataAccess.Model.Enums.FilterEnum.Language_French, FilterQuestionId = 3 });
            context.FilterQuestions.Add(new FilterQuestion() { FilterEnum = FilmickDataAccess.Model.Enums.FilterEnum.Language_English, FilterQuestionId = 4 });
            context.FilterQuestions.Add(new FilterQuestion() { FilterEnum = FilmickDataAccess.Model.Enums.FilterEnum.Old, FilterQuestionId = 5 });
            context.FilterQuestions.Add(new FilterQuestion() { FilterEnum = FilmickDataAccess.Model.Enums.FilterEnum.BeginXXI, FilterQuestionId = 6 });
            context.FilterQuestions.Add(new FilterQuestion() { FilterEnum = FilmickDataAccess.Model.Enums.FilterEnum.Language_German, FilterQuestionId = 7 });
            context.FilterQuestions.Add(new FilterQuestion() { FilterEnum = FilmickDataAccess.Model.Enums.FilterEnum.Language_Asian, FilterQuestionId = 8 });
            context.FilterQuestions.Add(new FilterQuestion() { FilterEnum = FilmickDataAccess.Model.Enums.FilterEnum.Language_Polish, FilterQuestionId = 9 });


            context.Languages.Add(new Language { LanguageId = 1, EnglishName = "Polish", Iso639_1 = "pl", Name = "Polski" });
            context.SaveChanges();

            filterMovieManager = new MovieFilterManager();
        }

        [Test]
        public void FilterMovies_AnswersExcludeAllMovies_Return0Movies()
        {
            using (var tempContext = new FilmickContext(options))
            {
                tempContext.GameQuestions.Add(new GameQuestion() { Answer = GameQuestion.AnswerEnum.Yes, FilterQuestionId = 4 });
                tempContext.GameQuestions.Add(new GameQuestion() { Answer = GameQuestion.AnswerEnum.No, FilterQuestionId = 2 });

                tempContext.SaveChanges();
            }

            var context = new FilmickContext(options);
            var movies = context.Movies.Include(x => x.MovieGenres).ThenInclude(y => y.Genre).AsEnumerable();
            var answers = context.GameQuestions.Include(x=>x.FilterQuestion);
            var expressionMovies = filterMovieManager.GetExpressionFilterMovies(answers);
            movies = new UnitOfWork(context).MovieRepository.Get(filter: expressionMovies, includeProperties: "MovieGenres,MovieGenres.Genre");

            Assert.AreEqual(0, movies.Count());
        }

        [Test]
        public void FilterMovies_FilterMoviesButDontOverrideSourceOfMovies_ReturnFilteredMoviesButSourceIsNotChanged()
        {
            using (var tempContext = new FilmickContext(options))
            {
                tempContext.GameQuestions.Add(new GameQuestion() { Answer = GameQuestion.AnswerEnum.Yes, FilterQuestionId = 4 });
                tempContext.GameQuestions.Add(new GameQuestion() { Answer = GameQuestion.AnswerEnum.No, FilterQuestionId = 2 });

                tempContext.SaveChanges();
            }

            var context = new FilmickContext(options);
            var movies = context.Movies.Include(x => x.MovieGenres).ThenInclude(y => y.Genre).AsEnumerable();
            var answers = context.GameQuestions.Include(x => x.FilterQuestion);
            var expressionMovies = filterMovieManager.GetExpressionFilterMovies(answers);
            var moviesResult = new UnitOfWork(context).MovieRepository.Get(filter: expressionMovies, includeProperties: "MovieGenres,MovieGenres.Genre");


            CollectionAssert.AreNotEqual(movies, moviesResult);
            Assert.AreNotEqual(movies.Count(), moviesResult.Count());
        }

        [Test]
        public void FilterMovies_FilterMoviesAndFindOne_ReturnOneExpectedMovie()
        {
            using (var tempContext = new FilmickContext(options))
            {
                tempContext.GameQuestions.Add(new GameQuestion() { Answer = GameQuestion.AnswerEnum.Yes, FilterQuestionId = 4 });
                tempContext.GameQuestions.Add(new GameQuestion() { Answer = GameQuestion.AnswerEnum.Yes, FilterQuestionId = 2 });

                tempContext.SaveChanges();
            }

            var context = new FilmickContext(options);
            var movies = context.Movies.Include(x => x.MovieGenres).ThenInclude(y => y.Genre).AsEnumerable();
            var answers = context.GameQuestions.Include(x => x.FilterQuestion);
            var expressionMovies = filterMovieManager.GetExpressionFilterMovies(answers);
            var moviesResult = new UnitOfWork(context).MovieRepository.Get(filter: expressionMovies, includeProperties: "MovieGenres,MovieGenres.Genre");


            Assert.AreEqual(moviesResult.First().MovieId, 2);
            Assert.AreEqual(moviesResult.Count(), 1);
        }

        [Test]
        public void GetEnglishMoviesWhenNoLanguageIsPicked_EnglishIdkRestNo_ReturnOnlyEnglishMovie()
        {
            using (var tempContext = new FilmickContext(options))
            {
                tempContext.GameQuestions.Add(new GameQuestion() { Answer = GameQuestion.AnswerEnum.Idk, FilterQuestionId = 4 });
                tempContext.GameQuestions.Add(new GameQuestion() { Answer = GameQuestion.AnswerEnum.No, FilterQuestionId = 3 });
                tempContext.GameQuestions.Add(new GameQuestion() { Answer = GameQuestion.AnswerEnum.No, FilterQuestionId = 7 });
                tempContext.GameQuestions.Add(new GameQuestion() { Answer = GameQuestion.AnswerEnum.No, FilterQuestionId = 8 });
                tempContext.GameQuestions.Add(new GameQuestion() { Answer = GameQuestion.AnswerEnum.No, FilterQuestionId = 9 });

                tempContext.SaveChanges();
            }

            var context = new FilmickContext(options);
            var movies = context.Movies.Include(x => x.MovieGenres).ThenInclude(y => y.Genre).AsEnumerable();
            var answers = context.GameQuestions.Include(x => x.FilterQuestion);
            var expressionMovies = filterMovieManager.GetExpressionFilterEnglishMoviesWhenNoLanguageIsPicked(answers);
            var moviesResult = new UnitOfWork(context).MovieRepository.Get(filter: expressionMovies, includeProperties: "MovieGenres,MovieGenres.Genre");


            Assert.AreEqual(moviesResult.First().MovieId, 2);
            Assert.AreEqual(moviesResult.Count(), 1);
        }

        [Test]
        public void GetEnglishMoviesWhenNoLanguageIsPicked_EnglishIdkRestIdk_ReturnOnlyEnglishMovie()
        {
            using (var tempContext = new FilmickContext(options))
            {
                tempContext.GameQuestions.Add(new GameQuestion() { Answer = GameQuestion.AnswerEnum.Idk, FilterQuestionId = 4 });
                tempContext.GameQuestions.Add(new GameQuestion() { Answer = GameQuestion.AnswerEnum.Idk, FilterQuestionId = 3 });
                tempContext.GameQuestions.Add(new GameQuestion() { Answer = GameQuestion.AnswerEnum.Idk, FilterQuestionId = 7 });
                tempContext.GameQuestions.Add(new GameQuestion() { Answer = GameQuestion.AnswerEnum.Idk, FilterQuestionId = 8 });
                tempContext.GameQuestions.Add(new GameQuestion() { Answer = GameQuestion.AnswerEnum.Idk, FilterQuestionId = 9 });

                tempContext.SaveChanges();
            }

            var context = new FilmickContext(options);
            var movies = context.Movies.Include(x => x.MovieGenres).ThenInclude(y => y.Genre).AsEnumerable();
            var answers = context.GameQuestions.Include(x => x.FilterQuestion);
            var expressionMovies = filterMovieManager.GetExpressionFilterEnglishMoviesWhenNoLanguageIsPicked(answers);
            var moviesResult = new UnitOfWork(context).MovieRepository.Get(filter: expressionMovies, includeProperties: "MovieGenres,MovieGenres.Genre");


            Assert.AreEqual(moviesResult.First().MovieId, 2);
            Assert.AreEqual(moviesResult.Count(), 1);
        }

        [Test]
        public void GetEnglishMoviesWhenNoLanguageIsPicked_EnglishIdkFrenchYes_DoNotExtraEnglishFilter()
        {
            using (var tempContext = new FilmickContext(options))
            {
                tempContext.GameQuestions.Add(new GameQuestion() { Answer = GameQuestion.AnswerEnum.Idk, FilterQuestionId = 4 });
                tempContext.GameQuestions.Add(new GameQuestion() { Answer = GameQuestion.AnswerEnum.Yes, FilterQuestionId = 3 });
                tempContext.GameQuestions.Add(new GameQuestion() { Answer = GameQuestion.AnswerEnum.No, FilterQuestionId = 7 });
                tempContext.GameQuestions.Add(new GameQuestion() { Answer = GameQuestion.AnswerEnum.Idk, FilterQuestionId = 8 });
                tempContext.GameQuestions.Add(new GameQuestion() { Answer = GameQuestion.AnswerEnum.Idk, FilterQuestionId = 9 });

                tempContext.SaveChanges();
            }

            var context = new FilmickContext(options);
            var movies = context.Movies.Include(x => x.MovieGenres).ThenInclude(y => y.Genre).AsEnumerable();
            var answers = context.GameQuestions.Include(x => x.FilterQuestion);
            var expressionMovies = filterMovieManager.GetExpressionFilterEnglishMoviesWhenNoLanguageIsPicked(answers);
            var moviesResult = new UnitOfWork(context).MovieRepository.Get(filter: expressionMovies, includeProperties: "MovieGenres,MovieGenres.Genre");


            Assert.AreEqual(moviesResult.Count(), 3);
        }

        [Test]
        public void GetEnglishMoviesWhenNoLanguageIsPicked_EnglishYesRestNo_DoNotExtraEnglishFilter()
        {
            using (var tempContext = new FilmickContext(options))
            {
                tempContext.GameQuestions.Add(new GameQuestion() { Answer = GameQuestion.AnswerEnum.Yes, FilterQuestionId = 4 });
                tempContext.GameQuestions.Add(new GameQuestion() { Answer = GameQuestion.AnswerEnum.No, FilterQuestionId = 3 });
                tempContext.GameQuestions.Add(new GameQuestion() { Answer = GameQuestion.AnswerEnum.No, FilterQuestionId = 7 });
                tempContext.GameQuestions.Add(new GameQuestion() { Answer = GameQuestion.AnswerEnum.No, FilterQuestionId = 8 });
                tempContext.GameQuestions.Add(new GameQuestion() { Answer = GameQuestion.AnswerEnum.No, FilterQuestionId = 9 });

                tempContext.SaveChanges();
            }

            var context = new FilmickContext(options);
            var movies = context.Movies.Include(x => x.MovieGenres).ThenInclude(y => y.Genre).AsEnumerable();
            var answers = context.GameQuestions.Include(x => x.FilterQuestion);
            var expressionMovies = filterMovieManager.GetExpressionFilterEnglishMoviesWhenNoLanguageIsPicked(answers);
            var moviesResult = new UnitOfWork(context).MovieRepository.Get(filter: expressionMovies, includeProperties: "MovieGenres,MovieGenres.Genre");


            Assert.AreEqual(moviesResult.Count(), 3);
        }

        [Test]
        public void GetEnglishMoviesWhenNoLanguageIsPicked_EnglishNoRestNo_DoNotExtraEnglishFilter()
        {
            using (var tempContext = new FilmickContext(options))
            {
                tempContext.GameQuestions.Add(new GameQuestion() { Answer = GameQuestion.AnswerEnum.No, FilterQuestionId = 4 });
                tempContext.GameQuestions.Add(new GameQuestion() { Answer = GameQuestion.AnswerEnum.No, FilterQuestionId = 3 });
                tempContext.GameQuestions.Add(new GameQuestion() { Answer = GameQuestion.AnswerEnum.No, FilterQuestionId = 7 });
                tempContext.GameQuestions.Add(new GameQuestion() { Answer = GameQuestion.AnswerEnum.No, FilterQuestionId = 8 });
                tempContext.GameQuestions.Add(new GameQuestion() { Answer = GameQuestion.AnswerEnum.No, FilterQuestionId = 9 });

                tempContext.SaveChanges();
            }

            var context = new FilmickContext(options);
            var movies = context.Movies.Include(x => x.MovieGenres).ThenInclude(y => y.Genre).AsEnumerable();
            var answers = context.GameQuestions.Include(x => x.FilterQuestion);
            var expressionMovies = filterMovieManager.GetExpressionFilterEnglishMoviesWhenNoLanguageIsPicked(answers);
            var moviesResult = new UnitOfWork(context).MovieRepository.Get(filter: expressionMovies, includeProperties: "MovieGenres,MovieGenres.Genre");


            Assert.AreEqual(moviesResult.Count(), 3);
        }
    }
}

