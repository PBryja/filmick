
using FilmickDataAccess.Context;
using Filmick.Infrastructure;
using FilmickDataAccess.Model;
using FilmickDataAccess.Model.Relationships;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using FilmickRepositories;
using FilmickServices.Game.GameQuestionManagers;

namespace Filmick.Tests.Infrastructure
{
    public class FilterQuestionManagerTests
    {
        private DbContextOptions<FilmickContext> options;

        [SetUp]
        public void Setup()
        {
            var temp = new TimeSpan(DateTime.Now.Ticks);

            options = new DbContextOptionsBuilder<FilmickContext>()
            .UseInMemoryDatabase(databaseName: "FilmickDB" + temp)
            .Options;

            var context = new FilmickContext(options);

            context.Movies.Add(new Movie { MovieId = 1, Title = "Movie 1" });
            context.Languages.Add(new Language { LanguageId = 1, EnglishName = "Polish", Iso639_1 = "pl", Name = "Polski" });
            context.SaveChanges();
        }

        [Test]
        public void GetQuestion_AvailableAllTypesQuestionsButAllAreAnsweredAndExcluded_ReturnNull()
        {
            using (var contextTemp = new FilmickContext(options))
            {
                var game = new Game() { GameId = new Guid() };
                contextTemp.Games.Add(game);

                var childQuestion = new FilterQuestion() { FilterQuestionId = 1, Name = "Test child filter" };
                contextTemp.FilterQuestions.Add(childQuestion);

                var parentQuestion = new FilterQuestion()
                {
                    FilterQuestionId = 2,
                    Name = "Test parent filter",
                    Dependents = new List<FilterQuestionAssociation>() {
                        new FilterQuestionAssociation() {
                            Dependent = childQuestion
                        }
                    }
                };
                contextTemp.FilterQuestions.Add(parentQuestion);

                var excludeParentQuestion = new FilterQuestion()
                {
                    FilterQuestionId = 6,
                    Name = "Test child filter 2",
                    Dependents = new List<FilterQuestionAssociation>() {
                        new FilterQuestionAssociation() {
                            Dependent = parentQuestion,
                            Type = FilterQuestionAssociation.TypeEnum.Exclude_WhenYesAndNo
                        }
                    }
                };
                contextTemp.FilterQuestions.Add(excludeParentQuestion);

                var mainParentQuestion = new FilterQuestion()
                {
                    FilterQuestionId = 3,
                    Name = "Test Main parent filter",
                    Dependents = new List<FilterQuestionAssociation>() {
                        new FilterQuestionAssociation() {
                            Dependent = parentQuestion
                        }
                    }
                };
                contextTemp.FilterQuestions.Add(mainParentQuestion);

                var nonFamilyQuestion = new FilterQuestion()
                {
                    FilterQuestionId = 4,
                    Name = "Test Non family filter"
                };
                contextTemp.FilterQuestions.Add(nonFamilyQuestion);

                contextTemp.GameQuestions.Add(new GameQuestion() { Answer = GameQuestion.AnswerEnum.Yes, Game = game, FilterQuestion = excludeParentQuestion });
                contextTemp.GameQuestions.Add(new GameQuestion() { Answer = GameQuestion.AnswerEnum.Yes, Game = game, FilterQuestion = mainParentQuestion });
                contextTemp.GameQuestions.Add(new GameQuestion() { Answer = GameQuestion.AnswerEnum.Yes, Game = game, FilterQuestion = nonFamilyQuestion });

                contextTemp.SaveChanges();

                var qManager = new FilterQuestionManager(new UnitOfWork(contextTemp), game.GameId);
                List<FilterQuestion> questions = new List<FilterQuestion>();
                List<FilterQuestion> questionsExpected = new List<FilterQuestion>();

                for (int i = 1; i <= 100; i++)
                {
                    questions.Add(qManager.GetQuestion()?.FilterQuestion);
                }

                for (int i = 1; i <= 100; i++)
                {
                    questionsExpected.Add(null);
                }

                CollectionAssert.AreEqual(questionsExpected, questions);
            }
        }

        [Test]
        public void GetQuestion_AvailableOneMainParentAndOneChildFilterQuestionWithNewGame_ReturnOnlyMainParentQuestion()
        {
            using (var contextTemp = new FilmickContext(options))
            {
                var game = new Game() { GameId = new Guid() };
                contextTemp.Games.Add(game);

                var childQuestion = new FilterQuestion() { FilterQuestionId = 1, Name = "Test child filter" };
                contextTemp.FilterQuestions.Add(childQuestion);
                var parentQuestion = new FilterQuestion()
                {
                    FilterQuestionId = 2,
                    Name = "Test parent filter",
                    Dependents = new List<FilterQuestionAssociation>() {
                        new FilterQuestionAssociation() {
                            Dependent = childQuestion
                        }
                    }
                };

                contextTemp.FilterQuestions.Add(parentQuestion);
                contextTemp.SaveChanges();

                var qManager = new FilterQuestionManager(new UnitOfWork(contextTemp), game.GameId);
                List<FilterQuestion> questions = new List<FilterQuestion>();
                List<FilterQuestion> questionsExpected = new List<FilterQuestion>();

                for (int i = 1; i <= 100; i++)
                {
                    questions.Add(qManager.GetQuestion().FilterQuestion);
                }

                for (int i = 1; i <= 100; i++)
                {
                    questionsExpected.Add(parentQuestion);
                }

                CollectionAssert.AreEqual(questionsExpected, questions);
            }
        }

        [Test]
        public void GetQuestion_AvailableOneMainParentAndOneParentAndOneChildFilterQuestionWithNewGame_ReturnOnlyMainParentQuestion()
        {
            using (var contextTemp = new FilmickContext(options))
            {
                var game = new Game() { GameId = new Guid() };
                contextTemp.Games.Add(game);

                var childQuestion = new FilterQuestion() { FilterQuestionId = 1, Name = "Test child filter" };
                contextTemp.FilterQuestions.Add(childQuestion);

                var parentQuestion = new FilterQuestion()
                {
                    FilterQuestionId = 2,
                    Name = "Test parent filter",
                    Dependents = new List<FilterQuestionAssociation>() {
                        new FilterQuestionAssociation() {
                            Dependent = childQuestion
                        }
                    }
                };
                contextTemp.FilterQuestions.Add(parentQuestion);

                var mainParentQuestion = new FilterQuestion()
                {
                    FilterQuestionId = 3,
                    Name = "Test Main parent filter",
                    Dependents = new List<FilterQuestionAssociation>() {
                        new FilterQuestionAssociation() {
                            Dependent = parentQuestion
                        },
                        new FilterQuestionAssociation()
                        {
                            Dependent = childQuestion
                        }
                    }
                };
                contextTemp.FilterQuestions.Add(mainParentQuestion);

                contextTemp.SaveChanges();

                var qManager = new FilterQuestionManager(new UnitOfWork(contextTemp), game.GameId);
                List<FilterQuestion> questions = new List<FilterQuestion>();
                List<FilterQuestion> questionsExpected = new List<FilterQuestion>();

                for (int i = 1; i <= 100; i++)
                {
                    questions.Add(qManager.GetQuestion().FilterQuestion);
                }

                for (int i = 1; i <= 100; i++)
                {
                    questionsExpected.Add(mainParentQuestion);
                }

                CollectionAssert.AreEqual(questionsExpected, questions);
            }
        }

        [Test]
        public void GetQuestion_AvailableAllTypesButMainParentHasAnswerNo_ReturnOnlyNonFamilyQuestion()
        {
            using (var contextTemp = new FilmickContext(options))
            {
                var game = new Game() { GameId = new Guid() };
                contextTemp.Games.Add(game);

                var childQuestion = new FilterQuestion() { FilterQuestionId = 1, Name = "Test child filter" };
                contextTemp.FilterQuestions.Add(childQuestion);

                var parentQuestion = new FilterQuestion()
                {
                    FilterQuestionId = 2,
                    Name = "Test parent filter",
                    Dependents = new List<FilterQuestionAssociation>() {
                        new FilterQuestionAssociation() {
                            Dependent = childQuestion
                        }
                    }
                };
                contextTemp.FilterQuestions.Add(parentQuestion);

                var mainParentQuestion = new FilterQuestion()
                {
                    FilterQuestionId = 3,
                    Name = "Test Main parent filter",
                    Dependents = new List<FilterQuestionAssociation>() {
                        new FilterQuestionAssociation() {
                            Dependent = parentQuestion
                        },
                        new FilterQuestionAssociation()
                        {
                            Dependent = childQuestion
                        }
                    }
                };
                contextTemp.FilterQuestions.Add(mainParentQuestion);

                var nonFamilyQuestion = new FilterQuestion()
                {
                    FilterQuestionId = 4,
                    Name = "Test Non family filter"
                };
                contextTemp.FilterQuestions.Add(nonFamilyQuestion);

                contextTemp.GameQuestions.Add(new GameQuestion() { Game = game, FilterQuestion = mainParentQuestion, Answer = GameQuestion.AnswerEnum.No });

                contextTemp.SaveChanges();

                var qManager = new FilterQuestionManager(new UnitOfWork(contextTemp), game.GameId);
                List<FilterQuestion> questions = new List<FilterQuestion>();
                List<FilterQuestion> questionsExpected = new List<FilterQuestion>();

                for (int i = 1; i <= 100; i++)
                {
                    questions.Add(qManager.GetQuestion().FilterQuestion);
                }

                for (int i = 1; i <= 100; i++)
                {
                    questionsExpected.Add(nonFamilyQuestion);
                }

                CollectionAssert.AreEqual(questionsExpected, questions);
            }
        }

        [Test]
        public void GetQuestion_AvailableTwoNonFamilyQuestionButOneWasAlreadyAnswered_ReturnOnlyNonFamilyQuestion()
        {
            using (var contextTemp = new FilmickContext(options))
            {
                var game = new Game() { GameId = new Guid() };
                contextTemp.Games.Add(game);

                var nonFamilyQuestion = new FilterQuestion()
                {
                    FilterQuestionId = 4,
                    Name = "Test Non family filter"
                };
                contextTemp.FilterQuestions.Add(nonFamilyQuestion);

                var nonFamilyQuestion2 = new FilterQuestion()
                {
                    FilterQuestionId = 5,
                    Name = "Test Non family filter 2"
                };
                contextTemp.FilterQuestions.Add(nonFamilyQuestion2);

                contextTemp.GameQuestions.Add(new GameQuestion() { Game = game, FilterQuestion = nonFamilyQuestion, Answer = GameQuestion.AnswerEnum.Yes });

                contextTemp.SaveChanges();

                var qManager = new FilterQuestionManager(new UnitOfWork(contextTemp), game.GameId);
                List<FilterQuestion> questions = new List<FilterQuestion>();
                List<FilterQuestion> questionsExpected = new List<FilterQuestion>();

                for (int i = 1; i <= 100; i++)
                {
                    questions.Add(qManager.GetQuestion().FilterQuestion);
                }

                for (int i = 1; i <= 100; i++)
                {
                    questionsExpected.Add(nonFamilyQuestion2);
                }

                CollectionAssert.AreEqual(questionsExpected, questions);
            }
        }

        [Test]
        public void GetQuestion_AvailableAllTypesQuestionsWithNewGame_ReturnMainParentOrNonFamilyQuestion()
        {
            using (var contextTemp = new FilmickContext(options))
            {
                var game = new Game() { GameId = new Guid() };
                contextTemp.Games.Add(game);

                var childQuestion = new FilterQuestion() { FilterQuestionId = 1, Name = "Test child filter" };
                contextTemp.FilterQuestions.Add(childQuestion);

                var parentQuestion = new FilterQuestion()
                {
                    FilterQuestionId = 2,
                    Name = "Test parent filter",
                    Dependents = new List<FilterQuestionAssociation>() {
                        new FilterQuestionAssociation() {
                            Dependent = childQuestion
                        }
                    }
                };
                contextTemp.FilterQuestions.Add(parentQuestion);

                var mainParentQuestion = new FilterQuestion()
                {
                    FilterQuestionId = 3,
                    Name = "Test Main parent filter",
                    Dependents = new List<FilterQuestionAssociation>() {
                        new FilterQuestionAssociation() {
                            Dependent = parentQuestion
                        },
                        new FilterQuestionAssociation()
                        {
                            Dependent = childQuestion
                        }
                    }
                };
                contextTemp.FilterQuestions.Add(mainParentQuestion);

                var nonFamilyQuestion = new FilterQuestion()
                {
                    FilterQuestionId = 4,
                    Name = "Test Non family filter"
                };
                contextTemp.FilterQuestions.Add(nonFamilyQuestion);

                contextTemp.SaveChanges();

                var qManager = new FilterQuestionManager(new UnitOfWork(contextTemp), game.GameId);
                List<FilterQuestion> questions = new List<FilterQuestion>();
                List<FilterQuestion> questionsExpected = new List<FilterQuestion>();

                for (int i = 1; i <= 500; i++)
                {
                    questions.Add(qManager.GetQuestion()?.FilterQuestion);
                }

                questionsExpected.Add(mainParentQuestion);
                questionsExpected.Add(nonFamilyQuestion);

                questions = questions.Distinct().OrderBy(x => x.FilterQuestionId).ToList();

                CollectionAssert.AreEqual(questionsExpected, questions);
            }
        }

        [Test]
        public void GetQuestion_NonAnsweredLastQuestion_ReturnLastQuestion()
        {
            using (var contextTemp = new FilmickContext(options))
            {
                var game = new Game() { GameId = new Guid() };
                contextTemp.Games.Add(game);

                var childQuestion = new FilterQuestion() { FilterQuestionId = 1, Name = "Test child filter" };
                contextTemp.FilterQuestions.Add(childQuestion);

                var parentQuestion = new FilterQuestion()
                {
                    FilterQuestionId = 2,
                    Name = "Test parent filter",
                    Dependents = new List<FilterQuestionAssociation>() {
                        new FilterQuestionAssociation() {
                            Dependent = childQuestion
                        }
                    }
                };
                contextTemp.FilterQuestions.Add(parentQuestion);

                var mainParentQuestion = new FilterQuestion()
                {
                    FilterQuestionId = 3,
                    Name = "Test Main parent filter",
                    Dependents = new List<FilterQuestionAssociation>() {
                        new FilterQuestionAssociation() {
                            Dependent = parentQuestion
                        },
                        new FilterQuestionAssociation()
                        {
                            Dependent = childQuestion
                        }
                    }
                };
                contextTemp.FilterQuestions.Add(mainParentQuestion);

                var nonFamilyQuestion = new FilterQuestion()
                {
                    FilterQuestionId = 4,
                    Name = "Test Non family filter"
                };
                contextTemp.FilterQuestions.Add(nonFamilyQuestion);

                contextTemp.GameQuestions.Add(new GameQuestion() { Game = game, FilterQuestion = childQuestion, Answer = GameQuestion.AnswerEnum.NoAnswer });

                contextTemp.SaveChanges();

                var qManager = new FilterQuestionManager(new UnitOfWork(contextTemp), game.GameId);
                List<FilterQuestion> questions = new List<FilterQuestion>();
                List<FilterQuestion> questionsExpected = new List<FilterQuestion>();

                for (int i = 1; i <= 100; i++)
                {
                    questions.Add(qManager.GetQuestion().FilterQuestion);
                }

                for (int i = 1; i <= 100; i++)
                {
                    questionsExpected.Add(childQuestion);
                }

                CollectionAssert.AreEqual(questionsExpected, questions);
            }
        }

        [Test]
        public void GetQuestion_AvailableAllTypeQuestionsButMainParentQuestionWasAnsweredYes_ReturnChildQuestionOfMainParentQuestion()
        {
            using (var contextTemp = new FilmickContext(options))
            {
                var game = new Game() { GameId = new Guid() };
                contextTemp.Games.Add(game);

                var childQuestion = new FilterQuestion() { FilterQuestionId = 1, Name = "Test child filter" };
                contextTemp.FilterQuestions.Add(childQuestion);

                var parentQuestion = new FilterQuestion()
                {
                    FilterQuestionId = 2,
                    Name = "Test parent filter",
                    Dependents = new List<FilterQuestionAssociation>() {
                        new FilterQuestionAssociation() {
                            Dependent = childQuestion
                        }
                    }
                };
                contextTemp.FilterQuestions.Add(parentQuestion);

                var mainParentQuestion = new FilterQuestion()
                {
                    FilterQuestionId = 3,
                    Name = "Test Main parent filter",
                    Dependents = new List<FilterQuestionAssociation>() {
                        new FilterQuestionAssociation() {
                            Dependent = parentQuestion
                        }
                    }
                };
                contextTemp.FilterQuestions.Add(mainParentQuestion);

                var nonFamilyQuestion = new FilterQuestion()
                {
                    FilterQuestionId = 4,
                    Name = "Test Non family filter"
                };
                contextTemp.FilterQuestions.Add(nonFamilyQuestion);

                contextTemp.GameQuestions.Add(new GameQuestion() { Game = game, Answer = GameQuestion.AnswerEnum.Yes, FilterQuestion = mainParentQuestion });

                contextTemp.SaveChanges();

                var qManager = new FilterQuestionManager(new UnitOfWork(contextTemp), game.GameId);
                List<FilterQuestion> questions = new List<FilterQuestion>();
                List<FilterQuestion> questionsExpected = new List<FilterQuestion>();

                for (int i = 1; i <= 100; i++)
                {
                    questions.Add(qManager.GetQuestion().FilterQuestion);
                }

                for (int i = 1; i <= 100; i++)
                {
                    questionsExpected.Add(parentQuestion);
                }



                CollectionAssert.AreEqual(questionsExpected, questions);
            }
        }

        private static object[] ExcludedCases =
        {
            new object[] { FilterQuestionAssociation.TypeEnum.Exclude_WhenOnlyYes, GameQuestion.AnswerEnum.Yes },
            new object[] { FilterQuestionAssociation.TypeEnum.Exclude_WhenYesAndIdk, GameQuestion.AnswerEnum.Yes },
            new object[] { FilterQuestionAssociation.TypeEnum.Exclude_WhenYesAndIdk, GameQuestion.AnswerEnum.Idk },
            new object[] { FilterQuestionAssociation.TypeEnum.Exclude_WhenYesAndNo, GameQuestion.AnswerEnum.Yes },
            new object[] { FilterQuestionAssociation.TypeEnum.Exclude_WhenYesAndNo, GameQuestion.AnswerEnum.No },

            new object[] { FilterQuestionAssociation.TypeEnum.Exclude_WhenOnlyNo, GameQuestion.AnswerEnum.No },
            new object[] { FilterQuestionAssociation.TypeEnum.Exclude_WhenNoAndIdk, GameQuestion.AnswerEnum.No },
            new object[] { FilterQuestionAssociation.TypeEnum.Exclude_WhenNoAndIdk, GameQuestion.AnswerEnum.Idk },

            new object[] { FilterQuestionAssociation.TypeEnum.Exclude_WhenOnlyIdk, GameQuestion.AnswerEnum.Idk },
        };

        [TestCaseSource(nameof(ExcludedCases))]
        [Test]
        public void GetQuestion_AvailableAllTypeQuestionsOneQuestionAnsweredWithExcludeQuestions_ReturnNonExcludedQuestion(FilterQuestionAssociation.TypeEnum typeEnum, GameQuestion.AnswerEnum answerEnum)
        {
            using (var contextTemp = new FilmickContext(options))
            {
                var game = new Game() { GameId = new Guid() };
                contextTemp.Games.Add(game);

                var childQuestion = new FilterQuestion() { FilterQuestionId = 1, Name = "Test child filter" };
                contextTemp.FilterQuestions.Add(childQuestion);

                var parentQuestion = new FilterQuestion()
                {
                    FilterQuestionId = 2,
                    Name = "Test parent filter",
                    Dependents = new List<FilterQuestionAssociation>() {
                        new FilterQuestionAssociation() {
                            Dependent = childQuestion
                        }
                    }
                };
                contextTemp.FilterQuestions.Add(parentQuestion);

                var mainParentQuestion = new FilterQuestion()
                {
                    FilterQuestionId = 3,
                    Name = "Test Main parent filter",
                    Dependents = new List<FilterQuestionAssociation>() {
                        new FilterQuestionAssociation() {
                            Dependent = parentQuestion
                        }
                    }
                };
                contextTemp.FilterQuestions.Add(mainParentQuestion);

                var nonFamilyQuestion = new FilterQuestion()
                {
                    FilterQuestionId = 4,
                    Name = "Test Non family filter"
                };
                contextTemp.FilterQuestions.Add(nonFamilyQuestion);

                var nonFamilyQuestion2 = new FilterQuestion()
                {
                    FilterQuestionId = 5,
                    Name = "Test Non family filter 2",
                    Dependents = new List<FilterQuestionAssociation>()
                    {
                        new FilterQuestionAssociation()
                        {
                            Dependent = mainParentQuestion,
                            Type = typeEnum
                        }
                    }
                };

                contextTemp.FilterQuestions.Add(nonFamilyQuestion2);

                contextTemp.GameQuestions.Add(new GameQuestion() { Game = game, Answer = answerEnum, FilterQuestion = nonFamilyQuestion2 });

                contextTemp.SaveChanges();

                var qManager = new FilterQuestionManager(new UnitOfWork(contextTemp), game.GameId);
                List<FilterQuestion> questions = new List<FilterQuestion>();
                List<FilterQuestion> questionsExpected = new List<FilterQuestion>();

                for (int i = 1; i <= 100; i++)
                {
                    questions.Add(qManager.GetQuestion().FilterQuestion);
                }

                for (int i = 1; i <= 100; i++)
                {
                    questionsExpected.Add(nonFamilyQuestion);
                }



                CollectionAssert.AreEqual(questionsExpected, questions);
            }
        }

        [TestCaseSource(nameof(ExcludedCases))]
        [Test]
        public void GetQuestion_AvailableAllTypeQuestionsOneOfChildQuestionAnsweredWithExcludeAnotherChildQuestion_ReturnNonChildNonExcludedQuestion(FilterQuestionAssociation.TypeEnum typeEnum, GameQuestion.AnswerEnum answerEnum)
        {
            using (var contextTemp = new FilmickContext(options))
            {
                var game = new Game() { GameId = new Guid() };
                contextTemp.Games.Add(game);

                var childQuestion = new FilterQuestion() { FilterQuestionId = 1, Name = "Test child filter" };
                contextTemp.FilterQuestions.Add(childQuestion);



                var parentQuestion = new FilterQuestion()
                {
                    FilterQuestionId = 2,
                    Name = "Test parent filter",
                    Dependents = new List<FilterQuestionAssociation>() {
                        new FilterQuestionAssociation() {
                            Dependent = childQuestion
                        }
                    }
                };
                contextTemp.FilterQuestions.Add(parentQuestion);

                var childQuestion2 = new FilterQuestion()
                {
                    FilterQuestionId = 6,
                    Name = "Test child filter 2",
                    Dependents = new List<FilterQuestionAssociation>() {
                        new FilterQuestionAssociation() {
                            Dependent = parentQuestion,
                            Type = typeEnum
                        }
                    }
                };
                contextTemp.FilterQuestions.Add(childQuestion2);

                var mainParentQuestion = new FilterQuestion()
                {
                    FilterQuestionId = 3,
                    Name = "Test Main parent filter",
                    Dependents = new List<FilterQuestionAssociation>() {
                        new FilterQuestionAssociation() {
                            Dependent = parentQuestion
                        },
                         new FilterQuestionAssociation() {
                            Dependent = childQuestion2
                        }
                    }
                };
                contextTemp.FilterQuestions.Add(mainParentQuestion);

                var nonFamilyQuestion = new FilterQuestion()
                {
                    FilterQuestionId = 4,
                    Name = "Test Non family filter"
                };
                contextTemp.FilterQuestions.Add(nonFamilyQuestion);

                contextTemp.GameQuestions.Add(new GameQuestion() { Game = game, Answer = GameQuestion.AnswerEnum.Yes, FilterQuestion = mainParentQuestion });
                contextTemp.GameQuestions.Add(new GameQuestion() { Game = game, Answer = answerEnum, FilterQuestion = childQuestion2 });

                contextTemp.SaveChanges();

                var qManager = new FilterQuestionManager(new UnitOfWork(contextTemp), game.GameId);
                List<FilterQuestion> questions = new List<FilterQuestion>();
                List<FilterQuestion> questionsExpected = new List<FilterQuestion>();

                for (int i = 1; i <= 100; i++)
                {
                    questions.Add(qManager.GetQuestion().FilterQuestion);
                }

                for (int i = 1; i <= 100; i++)
                {
                    questionsExpected.Add(nonFamilyQuestion);
                }



                CollectionAssert.AreEqual(questionsExpected, questions);
            }
        }


        private static object[] NonExcludedCases =
        {
            new object[] { FilterQuestionAssociation.TypeEnum.Exclude_WhenOnlyYes, GameQuestion.AnswerEnum.No },
            new object[] { FilterQuestionAssociation.TypeEnum.Exclude_WhenOnlyYes, GameQuestion.AnswerEnum.Idk },
            new object[] { FilterQuestionAssociation.TypeEnum.Exclude_WhenYesAndIdk, GameQuestion.AnswerEnum.No },
            new object[] { FilterQuestionAssociation.TypeEnum.Exclude_WhenYesAndNo, GameQuestion.AnswerEnum.Idk },

            new object[] { FilterQuestionAssociation.TypeEnum.Exclude_WhenOnlyNo, GameQuestion.AnswerEnum.Yes },
            new object[] { FilterQuestionAssociation.TypeEnum.Exclude_WhenOnlyNo, GameQuestion.AnswerEnum.Idk },
            new object[] { FilterQuestionAssociation.TypeEnum.Exclude_WhenNoAndIdk, GameQuestion.AnswerEnum.Yes },

            new object[] { FilterQuestionAssociation.TypeEnum.Exclude_WhenOnlyIdk, GameQuestion.AnswerEnum.Yes },
            new object[] { FilterQuestionAssociation.TypeEnum.Exclude_WhenOnlyIdk, GameQuestion.AnswerEnum.No },
        };

        [TestCaseSource(nameof(NonExcludedCases))]
        [Test]
        public void GetQuestion_AvailableAllTypeQuestionsOneQuestionAnsweredWithExcludeQuestionsButAnotherAnswer_ReturnNonExcludedQuestion(FilterQuestionAssociation.TypeEnum typeEnum, GameQuestion.AnswerEnum answerEnum)
        {
            using (var contextTemp = new FilmickContext(options))
            {
                var game = new Game() { GameId = new Guid() };
                contextTemp.Games.Add(game);

                var childQuestion = new FilterQuestion() { FilterQuestionId = 1, Name = "Test child filter" };
                contextTemp.FilterQuestions.Add(childQuestion);

                var parentQuestion = new FilterQuestion()
                {
                    FilterQuestionId = 2,
                    Name = "Test parent filter",
                    Dependents = new List<FilterQuestionAssociation>() {
                        new FilterQuestionAssociation() {
                            Dependent = childQuestion
                        }
                    }
                };
                contextTemp.FilterQuestions.Add(parentQuestion);

                var mainParentQuestion = new FilterQuestion()
                {
                    FilterQuestionId = 3,
                    Name = "Test Main parent filter",
                    Dependents = new List<FilterQuestionAssociation>() {
                        new FilterQuestionAssociation() {
                            Dependent = parentQuestion
                        }
                    }
                };
                contextTemp.FilterQuestions.Add(mainParentQuestion);

                var nonFamilyQuestion2 = new FilterQuestion()
                {
                    FilterQuestionId = 5,
                    Name = "Test Non family filter 2",
                    Dependents = new List<FilterQuestionAssociation>()
                    {
                        new FilterQuestionAssociation()
                        {
                            Dependent = mainParentQuestion,
                            Type = typeEnum
                        }
                    }
                };

                contextTemp.FilterQuestions.Add(nonFamilyQuestion2);

                contextTemp.GameQuestions.Add(new GameQuestion() { Game = game, Answer = answerEnum, FilterQuestion = nonFamilyQuestion2 });

                contextTemp.SaveChanges();

                var qManager = new FilterQuestionManager(new UnitOfWork(contextTemp), game.GameId);
                List<FilterQuestion> questions = new List<FilterQuestion>();
                List<FilterQuestion> questionsExpected = new List<FilterQuestion>();

                for (int i = 1; i <= 100; i++)
                {
                    questions.Add(qManager.GetQuestion().FilterQuestion);
                }

                for (int i = 1; i <= 100; i++)
                {
                    questionsExpected.Add(mainParentQuestion);
                }



                CollectionAssert.AreEqual(questionsExpected, questions);
            }
        }

        [TestCaseSource(nameof(NonExcludedCases))]
        [Test]
        public void GetQuestion_AvailableAllTypeQuestionsOneOfChildQuestionAnsweredWithExcludeAnotherChildQuestionButGaveAnotherAnswer_ReturnAnotherChildQuestion(FilterQuestionAssociation.TypeEnum typeEnum, GameQuestion.AnswerEnum answerEnum)
        {
            using (var contextTemp = new FilmickContext(options))
            {
                var game = new Game() { GameId = new Guid() };
                contextTemp.Games.Add(game);

                var childQuestion = new FilterQuestion() { FilterQuestionId = 1, Name = "Test child filter" };
                contextTemp.FilterQuestions.Add(childQuestion);



                var parentQuestion = new FilterQuestion()
                {
                    FilterQuestionId = 2,
                    Name = "Test parent filter",
                    Dependents = new List<FilterQuestionAssociation>() {
                        new FilterQuestionAssociation() {
                            Dependent = childQuestion
                        }
                    }
                };
                contextTemp.FilterQuestions.Add(parentQuestion);

                var childQuestion2 = new FilterQuestion()
                {
                    FilterQuestionId = 6,
                    Name = "Test child filter 2",
                    Dependents = new List<FilterQuestionAssociation>() {
                        new FilterQuestionAssociation() {
                            Dependent = parentQuestion,
                            Type = typeEnum
                        }
                    }
                };
                contextTemp.FilterQuestions.Add(childQuestion2);

                var mainParentQuestion = new FilterQuestion()
                {
                    FilterQuestionId = 3,
                    Name = "Test Main parent filter",
                    Dependents = new List<FilterQuestionAssociation>() {
                        new FilterQuestionAssociation() {
                            Dependent = parentQuestion
                        },
                         new FilterQuestionAssociation() {
                            Dependent = childQuestion2
                        }
                    }
                };
                contextTemp.FilterQuestions.Add(mainParentQuestion);

                var nonFamilyQuestion = new FilterQuestion()
                {
                    FilterQuestionId = 4,
                    Name = "Test Non family filter"
                };
                contextTemp.FilterQuestions.Add(nonFamilyQuestion);

                contextTemp.GameQuestions.Add(new GameQuestion() { Game = game, Answer = GameQuestion.AnswerEnum.Yes, FilterQuestion = mainParentQuestion });
                contextTemp.GameQuestions.Add(new GameQuestion() { Game = game, Answer = answerEnum, FilterQuestion = childQuestion2 });

                contextTemp.SaveChanges();

                var qManager = new FilterQuestionManager(new UnitOfWork(contextTemp), game.GameId);
                List<FilterQuestion> questions = new List<FilterQuestion>();
                List<FilterQuestion> questionsExpected = new List<FilterQuestion>();

                for (int i = 1; i <= 100; i++)
                {
                    questions.Add(qManager.GetQuestion().FilterQuestion);
                }

                for (int i = 1; i <= 100; i++)
                {
                    questionsExpected.Add(parentQuestion);
                }



                CollectionAssert.AreEqual(questionsExpected, questions);
            }
        }


    }
}