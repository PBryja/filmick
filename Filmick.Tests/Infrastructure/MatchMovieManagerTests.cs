
using FilmickDataAccess.Context;
using Filmick.Infrastructure;
using FilmickDataAccess.Model;
using FilmickDataAccess.Model.Relationships;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using FilmickRepositories;
using FilmickServices.Game.GameQuestionManagers;
using FilmickServices.Game.MovieManagers;

namespace Filmick.Tests.Infrastructure
{
    public class MatchMovieManagerTests
    {
        private DbContextOptions<FilmickContext> options;
        private TagQuestion childOfParentQuestion;

        private TagQuestion parentQuestion;
        private TagQuestion mainParentQuestion;
        private TagQuestion nonFamilyQuestion;
        private TagQuestion nonFamilyQuestion2;

        [SetUp]
        public void Setup()
        {
            var temp = new TimeSpan(DateTime.Now.Ticks);

            options = new DbContextOptionsBuilder<FilmickContext>()
            .UseInMemoryDatabase(databaseName: "FilmickDB" + temp)
            .Options;

            var context = new FilmickContext(options);

            context.Movies.Add(new Movie { MovieId = 1, Title = "Movie 1" });
            context.Languages.Add(new Language { LanguageId = 1, EnglishName = "Polish", Iso639_1 = "pl", Name = "Polski" });
            context.SaveChanges();

            childOfParentQuestion = new TagQuestion() { TagQuestionId = 1, Name = "Child of parent" };

            parentQuestion = new TagQuestion()
            {
                TagQuestionId = 2,
                Name = "Parent 1",
                Dependents = new List<TagQuestionAssociation>() {
                        new TagQuestionAssociation() {
                            Dependent = childOfParentQuestion
                        }
                    }
            };

            mainParentQuestion = new TagQuestion()
            {
                TagQuestionId = 3,
                Name = "Main parent 1",
                Dependents = new List<TagQuestionAssociation>() {
                        new TagQuestionAssociation() {
                            Dependent = parentQuestion
                        }
                    }
            };

            nonFamilyQuestion = new TagQuestion()
            {
                TagQuestionId = 4,
                Name = "Non family question"
            };


            nonFamilyQuestion2 = new TagQuestion()
            {
                TagQuestionId = 5,
                Name = "Non family question 2"
            };
        }

        private Movie AddAllTagToMovie(Movie movie, IEnumerable<TagQuestion> tagQuestions)
        {
            foreach (var tagQuestion in tagQuestions)
            {
                movie.MovieTags.Add(new FilmickDataAccess.Model.Assocs.MovieTag() { TagQuestion = tagQuestion });
            }

            return movie;
        }

        [Test]
        public void GetAvailableMovies_AddedMovieWithNoEnoughQuestions_ReturnZeroMovies()
        {
            using (var contextTemp = new FilmickContext(options))
            {
                var game = new Game() { GameId = new Guid() };
                contextTemp.Games.Add(game);

                contextTemp.TagQuestions.Add(childOfParentQuestion);
                contextTemp.TagQuestions.Add(parentQuestion);

                var excludeParentQuestion = new TagQuestion()
                {
                    TagQuestionId = 6,
                    Name = "Child of main Parent",
                    Dependents = new List<TagQuestionAssociation>()
                    {
                        new TagQuestionAssociation()
                        {
                            Dependent = parentQuestion,
                            Type = TagQuestionAssociation.TypeEnum.Exclude_WhenYesAndNo
                        }
                    }
                };

                contextTemp.TagQuestions.Add(excludeParentQuestion);
                contextTemp.TagQuestions.Add(mainParentQuestion);
                contextTemp.TagQuestions.Add(nonFamilyQuestion);

                contextTemp.SaveChanges();

                var movie = contextTemp.Movies.First(x => x.MovieId == 1);
                movie = AddAllTagToMovie(movie, contextTemp.TagQuestions);
                contextTemp.SaveChanges();

                var movieManager =
                    new MatchMovieManager(new UnitOfWork(contextTemp), game.GameId, contextTemp.Movies.ToList())
                    { MinimumRequiredAmountOfQuestionsForMovie = 6 };

                List<Movie> returnedList = movieManager.GetAvailableMovies().ToList();


                CollectionAssert.DoesNotContain(returnedList, movie);
            }
        }

        [Test]
        public void GetAvailableMovies_MovieWithNoLeftQuestions_ReturnZeroMovies()
        {
            using (var contextTemp = new FilmickContext(options))
            {
                var game = new Game() { GameId = new Guid() };
                contextTemp.Games.Add(game);

                contextTemp.TagQuestions.Add(childOfParentQuestion);
                contextTemp.TagQuestions.Add(parentQuestion);

                contextTemp.TagQuestions.Add(mainParentQuestion);
                contextTemp.TagQuestions.Add(nonFamilyQuestion);

                contextTemp.GameQuestions.Add(new GameQuestion() { Answer = GameQuestion.AnswerEnum.Yes, Game = game, TagQuestion = parentQuestion });
                contextTemp.GameQuestions.Add(new GameQuestion() { Answer = GameQuestion.AnswerEnum.Yes, Game = game, TagQuestion = mainParentQuestion });
                contextTemp.GameQuestions.Add(new GameQuestion() { Answer = GameQuestion.AnswerEnum.No, Game = game, TagQuestion = nonFamilyQuestion });
                contextTemp.GameQuestions.Add(new GameQuestion() { Answer = GameQuestion.AnswerEnum.No, Game = game, TagQuestion = childOfParentQuestion });

                contextTemp.SaveChanges();

                var movie = contextTemp.Movies.First(x => x.MovieId == 1);
                movie = AddAllTagToMovie(movie, contextTemp.TagQuestions);
                contextTemp.SaveChanges();

                var movieManager = new MatchMovieManager(new UnitOfWork(contextTemp), game.GameId, contextTemp.Movies.ToList()) { MinimumRequiredAmountOfQuestionsForMovie = 0 };

                List<Movie> returnedList = movieManager.GetAvailableMovies().ToList();


                CollectionAssert.DoesNotContain(returnedList, movie);
            }
        }

        [Test]
        public void GetAvailableMovies_MovieWith1LeftQuestionButNoEnoughYesAnswers_ReturnZeroMovies()
        {
            using (var contextTemp = new FilmickContext(options))
            {
                var game = new Game() { GameId = new Guid() };
                contextTemp.Games.Add(game);

                contextTemp.TagQuestions.Add(parentQuestion);
                contextTemp.TagQuestions.Add(nonFamilyQuestion);
                contextTemp.TagQuestions.Add(nonFamilyQuestion2);

                contextTemp.GameQuestions.Add(new GameQuestion() { Answer = GameQuestion.AnswerEnum.Yes, Game = game, TagQuestion = parentQuestion });
                contextTemp.GameQuestions.Add(new GameQuestion() { Answer = GameQuestion.AnswerEnum.No, Game = game, TagQuestion = nonFamilyQuestion });
                contextTemp.GameQuestions.Add(new GameQuestion() { Answer = GameQuestion.AnswerEnum.No, Game = game, TagQuestion = nonFamilyQuestion2 });

                contextTemp.SaveChanges();

                var movie = contextTemp.Movies.First(x => x.MovieId == 1);
                movie = AddAllTagToMovie(movie, contextTemp.TagQuestions);
                contextTemp.SaveChanges();

                var movieManager = new MatchMovieManager(new UnitOfWork(contextTemp), game.GameId, contextTemp.Movies.ToList()) { MinimumRequiredAmountOfQuestionsForMovie = 0, MinimumPercentToMarkMovieAsGoodMatch = 60 };

                List<Movie> returnedList = movieManager.GetAvailableMovies().ToList();


                CollectionAssert.DoesNotContain(returnedList, movie);
            }
        }

        [Test]
        public void GetAvailableMovies_MovieWith1LeftQuestionSimulateReturnTrue_ReturnMovie()
        {
            using (var contextTemp = new FilmickContext(options))
            {
                var game = new Game() { GameId = new Guid() };
                contextTemp.Games.Add(game);

                contextTemp.TagQuestions.Add(parentQuestion);
                contextTemp.TagQuestions.Add(nonFamilyQuestion);
                contextTemp.TagQuestions.Add(nonFamilyQuestion2);

                contextTemp.GameQuestions.Add(new GameQuestion() { Answer = GameQuestion.AnswerEnum.Yes, Game = game, TagQuestion = parentQuestion });
                contextTemp.GameQuestions.Add(new GameQuestion() { Answer = GameQuestion.AnswerEnum.Yes, Game = game, TagQuestion = nonFamilyQuestion });
                contextTemp.GameQuestions.Add(new GameQuestion() { Answer = GameQuestion.AnswerEnum.No, Game = game, TagQuestion = nonFamilyQuestion2 });

                contextTemp.SaveChanges();

                var movie = contextTemp.Movies.First(x => x.MovieId == 1);
                movie = AddAllTagToMovie(movie, contextTemp.TagQuestions);
                contextTemp.SaveChanges();

                var movieManager = new MatchMovieManager(new UnitOfWork(contextTemp), game.GameId, contextTemp.Movies.ToList()) { MinimumRequiredAmountOfQuestionsForMovie = 0, MinimumPercentToMarkMovieAsGoodMatch = 60 };

                List<Movie> returnedList = movieManager.GetAvailableMovies().ToList();


                CollectionAssert.Contains(returnedList, movie);
            }
        }

        [Test]
        public void GetAvailableMovies_MovieWithOnlyChildQuestionLeftButParentAnswerIsNo_SimulateReturnFalse_ReturnNoMovie()
        {
            using (var contextTemp = new FilmickContext(options))
            {
                var game = new Game() { GameId = new Guid() };
                contextTemp.Games.Add(game);

                contextTemp.TagQuestions.Add(parentQuestion);
                contextTemp.TagQuestions.Add(nonFamilyQuestion);
                contextTemp.TagQuestions.Add(nonFamilyQuestion2);

                contextTemp.GameQuestions.Add(new GameQuestion() { Answer = GameQuestion.AnswerEnum.No, Game = game, TagQuestion = parentQuestion });
                contextTemp.GameQuestions.Add(new GameQuestion() { Answer = GameQuestion.AnswerEnum.Yes, Game = game, TagQuestion = nonFamilyQuestion });
                contextTemp.GameQuestions.Add(new GameQuestion() { Answer = GameQuestion.AnswerEnum.Yes, Game = game, TagQuestion = nonFamilyQuestion2 });

                contextTemp.SaveChanges();

                var movie = contextTemp.Movies.First(x => x.MovieId == 1);
                movie = AddAllTagToMovie(movie, contextTemp.TagQuestions);
                contextTemp.SaveChanges();

                var movieManager = new MatchMovieManager(new UnitOfWork(contextTemp), game.GameId, contextTemp.Movies.ToList()) { MinimumRequiredAmountOfQuestionsForMovie = 0, MinimumPercentToMarkMovieAsGoodMatch = 60 };

                List<Movie> returnedList = movieManager.GetAvailableMovies().ToList();


                CollectionAssert.DoesNotContain(returnedList, movie);
            }
        }

        [Test]
        public void GetAvailableMovies_GoodMatchMovie_ReturnMovie()
        {
            using (var contextTemp = new FilmickContext(options))
            {
                var game = new Game() { GameId = new Guid() };
                contextTemp.Games.Add(game);

                contextTemp.TagQuestions.Add(parentQuestion);
                contextTemp.TagQuestions.Add(nonFamilyQuestion);
                contextTemp.TagQuestions.Add(nonFamilyQuestion2);

                contextTemp.GameQuestions.Add(new GameQuestion() { Answer = GameQuestion.AnswerEnum.Yes, Game = game, TagQuestion = parentQuestion });
                contextTemp.GameQuestions.Add(new GameQuestion() { Answer = GameQuestion.AnswerEnum.No, Game = game, TagQuestion = nonFamilyQuestion });
                contextTemp.GameQuestions.Add(new GameQuestion() { Answer = GameQuestion.AnswerEnum.Yes, Game = game, TagQuestion = nonFamilyQuestion2 });

                contextTemp.SaveChanges();

                var movie = contextTemp.Movies.First(x => x.MovieId == 1);
                movie = AddAllTagToMovie(movie, contextTemp.TagQuestions);
                contextTemp.SaveChanges();

                var movieManager = new MatchMovieManager(new UnitOfWork(contextTemp), game.GameId, contextTemp.Movies.ToList()) { MinimumRequiredAmountOfQuestionsForMovie = 0, MinimumPercentToMarkMovieAsGoodMatch = 50 };

                List<Movie> returnedList = movieManager.GetGoodMatchMovies().ToList();


                CollectionAssert.Contains(returnedList, movie);
            }
        }

        [Test]
        public void GetAvailableMovies_GoodMatchMovie_ReturnNoMovie()
        {
            using (var contextTemp = new FilmickContext(options))
            {
                var game = new Game() { GameId = new Guid() };
                contextTemp.Games.Add(game);

                contextTemp.TagQuestions.Add(parentQuestion);
                contextTemp.TagQuestions.Add(nonFamilyQuestion);
                contextTemp.TagQuestions.Add(nonFamilyQuestion2);

                contextTemp.GameQuestions.Add(new GameQuestion() { Answer = GameQuestion.AnswerEnum.Yes, Game = game, TagQuestion = parentQuestion });
                contextTemp.GameQuestions.Add(new GameQuestion() { Answer = GameQuestion.AnswerEnum.No, Game = game, TagQuestion = nonFamilyQuestion });
                contextTemp.GameQuestions.Add(new GameQuestion() { Answer = GameQuestion.AnswerEnum.No, Game = game, TagQuestion = nonFamilyQuestion2 });

                contextTemp.SaveChanges();

                var movie = contextTemp.Movies.First(x => x.MovieId == 1);
                movie = AddAllTagToMovie(movie, contextTemp.TagQuestions);
                contextTemp.SaveChanges();

                var movieManager = new MatchMovieManager(new UnitOfWork(contextTemp), game.GameId, contextTemp.Movies.ToList()) { MinimumRequiredAmountOfQuestionsForMovie = 0, MinimumPercentToMarkMovieAsGoodMatch = 50 };

                List<Movie> returnedList = movieManager.GetGoodMatchMovies().ToList();


                CollectionAssert.DoesNotContain(returnedList, movie);
            }
        }
    }
}
