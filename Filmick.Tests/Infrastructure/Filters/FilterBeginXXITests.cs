﻿
using FilmickDataAccess.Context;
using Filmick.Infrastructure;
using FilmickDataAccess.Model;
using FilmickDataAccess.Model.Relationships;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using FilmickServices.Game.Filters;

namespace Filmick.Tests.Infrastructure.Filters
{

    public class FilterBeginXXITests
    {
        private DbContextOptions<FilmickContext> options;
        Movie movie1;
        Movie movie2;
        Movie movie3;
        Movie movie4;

        [SetUp]
        public void Setup()
        {
            var temp = new TimeSpan(DateTime.Now.Ticks);

            options = new DbContextOptionsBuilder<FilmickContext>()
            .UseInMemoryDatabase(databaseName: "FilmickDB" + temp)
            .Options;

            movie1 = new Movie() { ReleaseDate = new DateTime(1950, 5, 5), MovieId = 1 };
            movie2 = new Movie() { ReleaseDate = new DateTime(2010, 5, 5), MovieId = 2 };
            movie3 = new Movie() { ReleaseDate = new DateTime(2009, 5, 5), MovieId = 3 };
            movie4 = new Movie() { ReleaseDate = new DateTime(2019, 5, 5), MovieId = 4 };

            var context = new FilmickContext(options);

            context.Movies.Add(movie1);
            context.Movies.Add(movie2);
            context.Movies.Add(movie3);
            context.Movies.Add(movie4);
            context.SaveChanges();

        }

        [Test]
        public void Yes_MoviesButTwoIsInTimeRange_ReturnTwoMovies()
        {
            var filter = new FilterBeginXXI();

            using (var tempContext = new FilmickContext(options))
            {
                var expectedMovies = new List<int> { movie1.MovieId, movie3.MovieId };

                var movies = tempContext.Movies;
                var resultMovies = movies.Where(filter.Yes()).OrderBy(x => x.MovieId)?.Select(x=>x.MovieId).ToList();

                CollectionAssert.AreEqual(expectedMovies, resultMovies);
            }
        }

        [Test]
        public void No_MoviesButTwoIsInTimeRange_ReturnTwoMovies()
        {
            var filter = new FilterBeginXXI();

            using (var tempContext = new FilmickContext(options))
            {
                var expectedMovies = new List<int> { movie2.MovieId, movie4.MovieId };

                var movies = tempContext.Movies;
                var resultMovies = movies.Where(filter.No()).OrderBy(x => x.MovieId)?.Select(x => x.MovieId).ToList();

                CollectionAssert.AreEqual(expectedMovies, resultMovies);
            }
        }

        [Test]
        public void Idk_MoviesButThreeIsInTimeRange_ReturnTwoMovies()
        {
            var filter = new FilterBeginXXI();

            using (var tempContext = new FilmickContext(options))
            {
                var expectedMovies = new List<int> { movie2.MovieId, movie3.MovieId, movie4.MovieId };

                var movies = tempContext.Movies;
                var resultMovies = movies.Where(filter.Idk()).OrderBy(x => x.MovieId)?.Select(x => x.MovieId).ToList();

                CollectionAssert.AreEqual(expectedMovies, resultMovies);
            }
        }
    }
}
