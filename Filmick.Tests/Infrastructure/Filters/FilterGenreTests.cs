﻿
using FilmickDataAccess.Context;
using Filmick.Infrastructure;
using FilmickDataAccess.Model;
using FilmickDataAccess.Model.Relationships;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using FilmickDataAccess.Model.Assocs;
using FilmickServices.Game.Filters;
using FilmickServices.Game.Filters.GenreFilters;

namespace Filmick.Tests.Infrastructure.Filters
{

    public class FilterGenreTests
    {
        private DbContextOptions<FilmickContext> options;
        private Genre fakeGenre;

        [SetUp]
        public void Setup()
        {
            var temp = new TimeSpan(DateTime.Now.Ticks);

            options = new DbContextOptionsBuilder<FilmickContext>()
            .UseInMemoryDatabase(databaseName: "FilmickDB" + temp)
            .Options;

            var context = new FilmickContext(options);

            fakeGenre = new Genre() { GenreId = 1, ApiName = "Fake" };
            context.Genres.Add(fakeGenre);

            context.SaveChanges();
        }
        private static List<string> NamesOfMoviesForFiltersWithTwoGenresYesStrings = new List<string>()
        {
            NamesOfMoviesForFiltersWithTwoGenres.BothTargets.ToString(),
            NamesOfMoviesForFiltersWithTwoGenres.TargetOne.ToString(),
            NamesOfMoviesForFiltersWithTwoGenres.TargetTwo.ToString(),
            NamesOfMoviesForFiltersWithTwoGenres.FakeAndTargetOne.ToString(),
            NamesOfMoviesForFiltersWithTwoGenres.FakeAndTargetTwo.ToString(),
            NamesOfMoviesForFiltersWithTwoGenres.All.ToString(),
        };

        private enum NamesOfMoviesForFiltersWithTwoGenres
        {
            BothTargets,
            TargetOne,
            TargetTwo,
            FakeAndTargetOne,
            FakeAndTargetTwo,
            All,
            None,
            Fake
        }

        private enum NamesOfMovies
        {
            Both,
            Target,
            Fake,
            None
        }

        private static Dictionary<string, Filter> genres = new Dictionary<string, Filter>()
            {
                { "Action", new FilterGenreAction() },
                { "Comedy", new FilterGenreComedy() },
                { "Animation", new FilterGenreAnimation() },
                { "Crime", new FilterGenreCrime() },
                { "Documentary", new FilterGenreDocumentaryOrHistory() },
                { "History", new FilterGenreDocumentaryOrHistory() },
                { "Horror", new FilterGenreHorrorOrThriller() },
                { "Thriller", new FilterGenreHorrorOrThriller() },
                { "Music", new FilterGenreMusic() },
                { "Mystery", new FilterGenreMystery() },
                { "Romance", new FilterGenreRomance() },
                { "Science Fiction", new FilterGenreSciFiOrFantasy() },
                { "Fantasy", new FilterGenreSciFiOrFantasy() },
                { "War", new FilterGenreWar() },
                { "Western", new FilterGenreWestern() },

            };

        private static Dictionary<Filter, (string, string)> filtersWithTwoGenres = new Dictionary<Filter, (string, string)>()
            {
                {  new FilterGenreDocumentaryOrHistory(), ("Documentary","History")  },
                {  new FilterGenreHorrorOrThriller(), ("Horror", "Thriller") },
                {  new FilterGenreSciFiOrFantasy(), ("Science Fiction", "Fantasy") },


            };
        private static IEnumerable<object> YesCasesForFiltersWithTwoGenres()
        {
            foreach (var filterGenres in filtersWithTwoGenres)
            {
                yield return new object[] { new Genre() { ApiName = filterGenres.Value.Item1 }, new Genre() { ApiName = filterGenres.Value.Item2 }, NamesOfMoviesForFiltersWithTwoGenresYesStrings, filterGenres.Key };
            }
        }

        private static IEnumerable<object> NoCasesForFiltersWithTwoGenres()
        {
            foreach (var filterGenres in filtersWithTwoGenres)
            {
                yield return new object[] { new Genre() { ApiName = filterGenres.Value.Item1 }, new Genre() { ApiName = filterGenres.Value.Item2 }, new List<string> { NamesOfMoviesForFiltersWithTwoGenres.Fake.ToString(), NamesOfMoviesForFiltersWithTwoGenres.None.ToString() }, filterGenres.Key };
            }
        }

        private static IEnumerable<object> IdkCasesForFiltersWithTwoGenres()
        {
            foreach (var filterGenres in filtersWithTwoGenres)
            {
                if (filterGenres.Key.GetType() == typeof(FilterGenreDocumentaryOrHistory))
                {
                    yield return new object[] { new Genre() { ApiName = filterGenres.Value.Item1 }, new Genre() { ApiName = filterGenres.Value.Item2 }, new List<string> { NamesOfMoviesForFiltersWithTwoGenres.Fake.ToString(), NamesOfMoviesForFiltersWithTwoGenres.None.ToString() }, filterGenres.Key };
                    continue;
                }

                var expected = NamesOfMoviesForFiltersWithTwoGenresYesStrings.ToList();
                expected.Add(NamesOfMoviesForFiltersWithTwoGenres.Fake.ToString());
                expected.Add(NamesOfMoviesForFiltersWithTwoGenres.None.ToString());

                yield return new object[] { new Genre() { ApiName = filterGenres.Value.Item1 }, new Genre() { ApiName = filterGenres.Value.Item2 }, expected, filterGenres.Key };

            }
        }

        private static IEnumerable<object> IdkGenresCases()
        {
            foreach (var genre in genres)
            {
                List<string> genresWhichAreExcludingMovies = new List<string>() { "Animation", "Documentary", "History", "Music", "War", "Western" };

                if (genresWhichAreExcludingMovies.Contains(genre.Key))
                {
                    yield return new object[] { new Genre() { ApiName = genre.Key }, new List<string> { NamesOfMovies.None.ToString(), NamesOfMovies.Fake.ToString() }, genre.Value };
                    continue;
                }

                yield return new object[] { new Genre() { ApiName = genre.Key }, new List<string> { NamesOfMovies.None.ToString(), NamesOfMovies.Fake.ToString(), NamesOfMovies.Target.ToString(), NamesOfMovies.Both.ToString() }, genre.Value };
            }
        }

        private static IEnumerable<object> NoGenresCases()
        {
            foreach (var genre in genres)
            {
                if (genre.Key == "Action")
                {
                    yield return new object[] { new Genre() { ApiName = genre.Key }, new List<string> { NamesOfMovies.None.ToString(), NamesOfMovies.Fake.ToString(), NamesOfMovies.Target.ToString(), NamesOfMovies.Both.ToString() }, genre.Value };
                    continue;
                }

                yield return new object[] { new Genre() { ApiName = genre.Key }, new List<string> { NamesOfMovies.None.ToString(), NamesOfMovies.Fake.ToString() }, genre.Value };
            }
        }

        private static IEnumerable<object> YesGenresCases()
        {
            foreach (var genre in genres)
            {
                if (genre.Key == "Mystery")
                {
                    yield return new object[] { new Genre() { ApiName = genre.Key }, new List<string> { NamesOfMovies.Both.ToString(), NamesOfMovies.Target.ToString(), NamesOfMovies.None.ToString(), NamesOfMovies.Fake.ToString() }, genre.Value };
                    continue;
                }

                yield return new object[] { new Genre() { ApiName = genre.Key }, new List<string> { NamesOfMovies.Both.ToString(), NamesOfMovies.Target.ToString() }, genre.Value };
            }
        }

        private static IEnumerable<object> YesZeroMoviesGenresCases()
        {
            foreach (var genre in genres)
            {
                if (genre.Key == "Mystery")
                {
                    yield return new object[] { new Genre() { ApiName = genre.Key }, new List<string> { NamesOfMovies.Fake.ToString(), NamesOfMovies.None.ToString() }, genre.Value };
                    continue;
                }

                yield return new object[] { new Genre() { ApiName = genre.Key }, new List<string>(), genre.Value };
            }
        }

        [TestCaseSource(nameof(YesGenresCases))]
        [Test]
        public void Yes_FourMoviesButTwoHasTargetGenre_ExpectExpectedMovies(Genre genreTarget, List<string> expectedNamesMovies, Filter filter)
        {
            using (var tempContext = new FilmickContext(options))
            {
                tempContext.Genres.Add(genreTarget);
                tempContext.SaveChanges();
            }

            using (var tempContext = new FilmickContext(options))
            {
                var movie1 = new Movie()
                {
                    OriginalTitle = NamesOfMovies.Both.ToString(),
                };
                movie1.MovieGenres.Add(new MovieGenre() { GenreId = genreTarget.GenreId });
                movie1.MovieGenres.Add(new MovieGenre() { GenreId = fakeGenre.GenreId });

                var movie2 = new Movie() { OriginalTitle = NamesOfMovies.Target.ToString(), MovieGenres = new List<MovieGenre>() { new MovieGenre() { GenreId = genreTarget.GenreId } } };
                var movie3 = new Movie() { OriginalTitle = NamesOfMovies.Fake.ToString(), MovieGenres = new List<MovieGenre>() { new MovieGenre() { GenreId = fakeGenre.GenreId } } };
                var movie4 = new Movie() { OriginalTitle = NamesOfMovies.None.ToString() };

                tempContext.Movies.Add(movie1);
                tempContext.Movies.Add(movie2);
                tempContext.Movies.Add(movie3);
                tempContext.Movies.Add(movie4);
                tempContext.SaveChanges();

                var movies = tempContext.Movies.Include(x => x.MovieGenres).ThenInclude(y => y.Genre);
                var resultMovies = movies.Where(filter.Yes()).OrderBy(x => x.OriginalTitle).ToList();

                var expectedMovies = tempContext.Movies.Where(x => expectedNamesMovies.Contains(x.OriginalTitle)).OrderBy(x => x.OriginalTitle).ToList();

                CollectionAssert.AreEqual(expectedMovies, resultMovies);
            }
        }

        [TestCaseSource(nameof(YesZeroMoviesGenresCases))]
        [Test]
        public void Yes_ZeroMoviesWithTargetGenre_ReturnExpectedMovies(Genre genreTarget, List<string> expectedNamesMovies, Filter filter)
        {
            using (var tempContext = new FilmickContext(options))
            {
                tempContext.Genres.Add(genreTarget);
                tempContext.SaveChanges();
            }

            using (var tempContext = new FilmickContext(options))
            {
                var movie3 = new Movie() { OriginalTitle = NamesOfMovies.Fake.ToString(), MovieGenres = new List<MovieGenre>() { new MovieGenre() { GenreId = fakeGenre.GenreId } } };
                var movie4 = new Movie() { OriginalTitle = NamesOfMovies.None.ToString() };

                tempContext.Movies.Add(movie3);
                tempContext.Movies.Add(movie4);
                tempContext.SaveChanges();

                var movies = tempContext.Movies.Include(x => x.MovieGenres).ThenInclude(y => y.Genre);
                var resultMovies = movies.Where(filter.Yes()).OrderBy(x => x.OriginalTitle).ToList();

                var expectedMovies = tempContext.Movies.Where(x => expectedNamesMovies.Contains(x.OriginalTitle)).OrderBy(x => x.OriginalTitle).ToList();

                CollectionAssert.AreEqual(expectedMovies, resultMovies);
            }
        }

        [TestCaseSource(nameof(NoGenresCases))]
        [Test]
        public void No_FourMoviesButTwoHasTargetGenre_ReturnExpectedMovies(Genre genreTarget, List<string> expectedNamesMovies, Filter filter)
        {
            using (var tempContext = new FilmickContext(options))
            {
                tempContext.Genres.Add(genreTarget);
                tempContext.SaveChanges();
            }

            using (var tempContext = new FilmickContext(options))
            {
                var movie1 = new Movie()
                {
                    OriginalTitle = NamesOfMovies.Both.ToString(),
                };
                movie1.MovieGenres.Add(new MovieGenre() { GenreId = genreTarget.GenreId });
                movie1.MovieGenres.Add(new MovieGenre() { GenreId = fakeGenre.GenreId });

                var movie2 = new Movie() { OriginalTitle = NamesOfMovies.Target.ToString(), MovieGenres = new List<MovieGenre>() { new MovieGenre() { GenreId = genreTarget.GenreId } } };
                var movie3 = new Movie() { OriginalTitle = NamesOfMovies.Fake.ToString(), MovieGenres = new List<MovieGenre>() { new MovieGenre() { GenreId = fakeGenre.GenreId } } };
                var movie4 = new Movie() { OriginalTitle = NamesOfMovies.None.ToString() };

                tempContext.Movies.Add(movie1);
                tempContext.Movies.Add(movie2);
                tempContext.Movies.Add(movie3);
                tempContext.Movies.Add(movie4);
                tempContext.SaveChanges();

                var movies = tempContext.Movies.Include(x => x.MovieGenres).ThenInclude(y => y.Genre);
                var resultMovies = movies.Where(filter.No()).OrderBy(x => x.OriginalTitle).ToList();

                var expectedMovies = tempContext.Movies.Where(x => expectedNamesMovies.Contains(x.OriginalTitle)).OrderBy(x => x.OriginalTitle).ToList();

                CollectionAssert.AreEqual(expectedMovies, resultMovies);
            }
        }

        [TestCaseSource(nameof(IdkGenresCases))]
        [Test]
        public void Idk_FourMoviesButTwoHasTargetGenre_ReturnExpectedMovies(Genre genreTarget, List<string> expectedNamesMovies, Filter filter)
        {
            using (var tempContext = new FilmickContext(options))
            {
                tempContext.Genres.Add(genreTarget);
                tempContext.SaveChanges();
            }

            using (var tempContext = new FilmickContext(options))
            {
                var movie1 = new Movie()
                {
                    OriginalTitle = NamesOfMovies.Both.ToString(),
                };
                movie1.MovieGenres.Add(new MovieGenre() { GenreId = genreTarget.GenreId });
                movie1.MovieGenres.Add(new MovieGenre() { GenreId = fakeGenre.GenreId });

                var movie2 = new Movie() { OriginalTitle = NamesOfMovies.Target.ToString(), MovieGenres = new List<MovieGenre>() { new MovieGenre() { GenreId = genreTarget.GenreId } } };
                var movie3 = new Movie() { OriginalTitle = NamesOfMovies.Fake.ToString(), MovieGenres = new List<MovieGenre>() { new MovieGenre() { GenreId = fakeGenre.GenreId } } };
                var movie4 = new Movie() { OriginalTitle = NamesOfMovies.None.ToString() };

                tempContext.Movies.Add(movie1);
                tempContext.Movies.Add(movie2);
                tempContext.Movies.Add(movie3);
                tempContext.Movies.Add(movie4);
                tempContext.SaveChanges();

                var movies = tempContext.Movies.Include(x => x.MovieGenres).ThenInclude(y => y.Genre);
                var resultMovies = movies.Where(filter.Idk()).OrderBy(x => x.OriginalTitle).ToList();

                var expectedMovies = tempContext.Movies.Where(x => expectedNamesMovies.Contains(x.OriginalTitle)).OrderBy(x => x.OriginalTitle).ToList();

                CollectionAssert.AreEqual(expectedMovies, resultMovies);
            }
        }

        [TestCaseSource(nameof(YesCasesForFiltersWithTwoGenres))]
        [Test]
        public void Yes_EightMoviesBut6HaveTargetGenre_ReturnExpectedMovies(Genre genreTargetOne, Genre genreTargetTwo, List<string> expectedNamesMovies, Filter filter)
        {
            using (var tempContext = new FilmickContext(options))
            {
                tempContext.Genres.Add(genreTargetOne);
                tempContext.Genres.Add(genreTargetTwo);
                tempContext.SaveChanges();
            }

            using (var tempContext = new FilmickContext(options))
            {
                var movie1 = new Movie()
                {
                    OriginalTitle = NamesOfMoviesForFiltersWithTwoGenres.All.ToString(),
                };
                movie1.MovieGenres.Add(new MovieGenre() { GenreId = genreTargetOne.GenreId });
                movie1.MovieGenres.Add(new MovieGenre() { GenreId = genreTargetTwo.GenreId });
                movie1.MovieGenres.Add(new MovieGenre() { GenreId = fakeGenre.GenreId });

                var movie2 = new Movie() { OriginalTitle = NamesOfMoviesForFiltersWithTwoGenres.TargetOne.ToString(), MovieGenres = new List<MovieGenre>() { new MovieGenre() { GenreId = genreTargetOne.GenreId } } };
                var movie5 = new Movie() { OriginalTitle = NamesOfMoviesForFiltersWithTwoGenres.TargetTwo.ToString(), MovieGenres = new List<MovieGenre>() { new MovieGenre() { GenreId = genreTargetTwo.GenreId } } };
                var movie6 = new Movie() { OriginalTitle = NamesOfMoviesForFiltersWithTwoGenres.BothTargets.ToString(), MovieGenres = new List<MovieGenre>() { new MovieGenre() { GenreId = genreTargetTwo.GenreId }, new MovieGenre() { GenreId = genreTargetOne.GenreId } } };
                var movie3 = new Movie() { OriginalTitle = NamesOfMoviesForFiltersWithTwoGenres.Fake.ToString(), MovieGenres = new List<MovieGenre>() { new MovieGenre() { GenreId = fakeGenre.GenreId } } };
                var movie7 = new Movie() { OriginalTitle = NamesOfMoviesForFiltersWithTwoGenres.FakeAndTargetOne.ToString(), MovieGenres = new List<MovieGenre>() { new MovieGenre() { GenreId = fakeGenre.GenreId }, new MovieGenre() { GenreId = genreTargetOne.GenreId } } };
                var movie8 = new Movie() { OriginalTitle = NamesOfMoviesForFiltersWithTwoGenres.FakeAndTargetTwo.ToString(), MovieGenres = new List<MovieGenre>() { new MovieGenre() { GenreId = fakeGenre.GenreId }, new MovieGenre() { GenreId = genreTargetTwo.GenreId } } };
                var movie4 = new Movie() { OriginalTitle = NamesOfMoviesForFiltersWithTwoGenres.None.ToString() };

                tempContext.Movies.Add(movie1);
                tempContext.Movies.Add(movie2);
                tempContext.Movies.Add(movie3);
                tempContext.Movies.Add(movie4);
                tempContext.Movies.Add(movie5);
                tempContext.Movies.Add(movie6);
                tempContext.Movies.Add(movie7);
                tempContext.Movies.Add(movie8);
                tempContext.SaveChanges();

                var movies = tempContext.Movies.Include(x => x.MovieGenres).ThenInclude(y => y.Genre);
                var resultMovies = movies.Where(filter.Yes()).OrderBy(x => x.OriginalTitle).ToList();

                var expectedMovies = tempContext.Movies.Where(x => expectedNamesMovies.Contains(x.OriginalTitle)).OrderBy(x => x.OriginalTitle).ToList();

                CollectionAssert.AreEqual(expectedMovies, resultMovies);
            }
        }

        [TestCaseSource(nameof(NoCasesForFiltersWithTwoGenres))]
        [Test]
        public void No_EightMoviesBut6HaveTargetGenre_ReturnExpectedMovies(Genre genreTargetOne, Genre genreTargetTwo, List<string> expectedNamesMovies, Filter filter)
        {
            using (var tempContext = new FilmickContext(options))
            {
                tempContext.Genres.Add(genreTargetOne);
                tempContext.Genres.Add(genreTargetTwo);
                tempContext.SaveChanges();
            }

            using (var tempContext = new FilmickContext(options))
            {
                var movie1 = new Movie()
                {
                    OriginalTitle = NamesOfMoviesForFiltersWithTwoGenres.All.ToString(),
                };
                movie1.MovieGenres.Add(new MovieGenre() { GenreId = genreTargetOne.GenreId });
                movie1.MovieGenres.Add(new MovieGenre() { GenreId = genreTargetTwo.GenreId });
                movie1.MovieGenres.Add(new MovieGenre() { GenreId = fakeGenre.GenreId });

                var movie2 = new Movie() { OriginalTitle = NamesOfMoviesForFiltersWithTwoGenres.TargetOne.ToString(), MovieGenres = new List<MovieGenre>() { new MovieGenre() { GenreId = genreTargetOne.GenreId } } };
                var movie5 = new Movie() { OriginalTitle = NamesOfMoviesForFiltersWithTwoGenres.TargetTwo.ToString(), MovieGenres = new List<MovieGenre>() { new MovieGenre() { GenreId = genreTargetTwo.GenreId } } };
                var movie6 = new Movie() { OriginalTitle = NamesOfMoviesForFiltersWithTwoGenres.BothTargets.ToString(), MovieGenres = new List<MovieGenre>() { new MovieGenre() { GenreId = genreTargetTwo.GenreId }, new MovieGenre() { GenreId = genreTargetOne.GenreId } } };
                var movie3 = new Movie() { OriginalTitle = NamesOfMoviesForFiltersWithTwoGenres.Fake.ToString(), MovieGenres = new List<MovieGenre>() { new MovieGenre() { GenreId = fakeGenre.GenreId } } };
                var movie7 = new Movie() { OriginalTitle = NamesOfMoviesForFiltersWithTwoGenres.FakeAndTargetOne.ToString(), MovieGenres = new List<MovieGenre>() { new MovieGenre() { GenreId = fakeGenre.GenreId }, new MovieGenre() { GenreId = genreTargetOne.GenreId } } };
                var movie8 = new Movie() { OriginalTitle = NamesOfMoviesForFiltersWithTwoGenres.FakeAndTargetTwo.ToString(), MovieGenres = new List<MovieGenre>() { new MovieGenre() { GenreId = fakeGenre.GenreId }, new MovieGenre() { GenreId = genreTargetTwo.GenreId } } };
                var movie4 = new Movie() { OriginalTitle = NamesOfMoviesForFiltersWithTwoGenres.None.ToString() };

                tempContext.Movies.Add(movie1);
                tempContext.Movies.Add(movie2);
                tempContext.Movies.Add(movie3);
                tempContext.Movies.Add(movie4);
                tempContext.Movies.Add(movie5);
                tempContext.Movies.Add(movie6);
                tempContext.Movies.Add(movie7);
                tempContext.Movies.Add(movie8);
                tempContext.SaveChanges();

                var movies = tempContext.Movies.Include(x => x.MovieGenres).ThenInclude(y => y.Genre);
                var resultMovies = movies.Where(filter.No()).OrderBy(x => x.OriginalTitle).ToList();

                var expectedMovies = tempContext.Movies.Where(x => expectedNamesMovies.Contains(x.OriginalTitle)).OrderBy(x => x.OriginalTitle).ToList();

                CollectionAssert.AreEqual(expectedMovies, resultMovies);
            }
        }

        [TestCaseSource(nameof(IdkCasesForFiltersWithTwoGenres))]
        [Test]
        public void Idk_EightMoviesBut6HaveTargetGenre_ReturnExpectedMovies(Genre genreTargetOne, Genre genreTargetTwo, List<string> expectedNamesMovies, Filter filter)
        {
            using (var tempContext = new FilmickContext(options))
            {
                tempContext.Genres.Add(genreTargetOne);
                tempContext.Genres.Add(genreTargetTwo);
                tempContext.SaveChanges();
            }

            using (var tempContext = new FilmickContext(options))
            {
                var movie1 = new Movie()
                {
                    OriginalTitle = NamesOfMoviesForFiltersWithTwoGenres.All.ToString(),
                };
                movie1.MovieGenres.Add(new MovieGenre() { GenreId = genreTargetOne.GenreId });
                movie1.MovieGenres.Add(new MovieGenre() { GenreId = genreTargetTwo.GenreId });
                movie1.MovieGenres.Add(new MovieGenre() { GenreId = fakeGenre.GenreId });

                var movie2 = new Movie() { OriginalTitle = NamesOfMoviesForFiltersWithTwoGenres.TargetOne.ToString(), MovieGenres = new List<MovieGenre>() { new MovieGenre() { GenreId = genreTargetOne.GenreId } } };
                var movie5 = new Movie() { OriginalTitle = NamesOfMoviesForFiltersWithTwoGenres.TargetTwo.ToString(), MovieGenres = new List<MovieGenre>() { new MovieGenre() { GenreId = genreTargetTwo.GenreId } } };
                var movie6 = new Movie() { OriginalTitle = NamesOfMoviesForFiltersWithTwoGenres.BothTargets.ToString(), MovieGenres = new List<MovieGenre>() { new MovieGenre() { GenreId = genreTargetTwo.GenreId }, new MovieGenre() { GenreId = genreTargetOne.GenreId } } };
                var movie3 = new Movie() { OriginalTitle = NamesOfMoviesForFiltersWithTwoGenres.Fake.ToString(), MovieGenres = new List<MovieGenre>() { new MovieGenre() { GenreId = fakeGenre.GenreId } } };
                var movie7 = new Movie() { OriginalTitle = NamesOfMoviesForFiltersWithTwoGenres.FakeAndTargetOne.ToString(), MovieGenres = new List<MovieGenre>() { new MovieGenre() { GenreId = fakeGenre.GenreId }, new MovieGenre() { GenreId = genreTargetOne.GenreId } } };
                var movie8 = new Movie() { OriginalTitle = NamesOfMoviesForFiltersWithTwoGenres.FakeAndTargetTwo.ToString(), MovieGenres = new List<MovieGenre>() { new MovieGenre() { GenreId = fakeGenre.GenreId }, new MovieGenre() { GenreId = genreTargetTwo.GenreId } } };
                var movie4 = new Movie() { OriginalTitle = NamesOfMoviesForFiltersWithTwoGenres.None.ToString() };

                tempContext.Movies.Add(movie1);
                tempContext.Movies.Add(movie2);
                tempContext.Movies.Add(movie3);
                tempContext.Movies.Add(movie4);
                tempContext.Movies.Add(movie5);
                tempContext.Movies.Add(movie6);
                tempContext.Movies.Add(movie7);
                tempContext.Movies.Add(movie8);
                tempContext.SaveChanges();

                var movies = tempContext.Movies.Include(x => x.MovieGenres).ThenInclude(y => y.Genre);
                var resultMovies = movies.Where(filter.Idk()).OrderBy(x => x.OriginalTitle).ToList();

                var expectedMovies = tempContext.Movies.Where(x => expectedNamesMovies.Contains(x.OriginalTitle)).OrderBy(x => x.OriginalTitle).ToList();

                CollectionAssert.AreEqual(expectedMovies, resultMovies);
            }
        }
    }
}
