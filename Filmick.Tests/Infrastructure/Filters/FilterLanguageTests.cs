﻿
using FilmickDataAccess.Context;
using Filmick.Infrastructure;
using FilmickDataAccess.Model;
using FilmickDataAccess.Model.Relationships;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using FilmickServices.Game.Filters;
using FilmickServices.Game.Filters.LanguageFilters;

namespace Filmick.Tests.Infrastructure.Filters
{

    public class FilterLanguageTests
    {
        private DbContextOptions<FilmickContext> options;

        [SetUp]
        public void Setup()
        {
            var temp = new TimeSpan(DateTime.Now.Ticks);

            options = new DbContextOptionsBuilder<FilmickContext>()
            .UseInMemoryDatabase(databaseName: "FilmickDB" + temp)
            .Options;

            var context = new FilmickContext(options);

            context.SaveChanges();
        }

        private enum NamesOfMovies
        {
            Target,
            Fake,
            None
        }

        private static Dictionary<string, Filter> languages = new Dictionary<string, Filter>()
            {
                { "en", new FilterLanguageEnglish() },
                { "ja", new FilterLanguageAsian() },
                { "zh", new FilterLanguageAsian() },
                { "ko", new FilterLanguageAsian() },
                { "fr", new FilterLanguageFrench() },
                { "de", new FilterLanguageGerman() },
                { "pl", new FilterLanguagePolish() }
            };

        private static IEnumerable<object> CasesFewLanguages()
        {
                yield return new object[] { new List<string>() { "ja", "zh", "ko" }, 3, new FilterLanguageAsian().Yes() };
                yield return new object[] { new List<string>() { "ja", "zh", "ko" }, 2, new FilterLanguageAsian().No() };
                yield return new object[] { new List<string>() { "ja", "zh", "ko" }, 5, new FilterLanguageAsian().Idk() };
        }

        private static IEnumerable<object> Cases()
        {
            foreach (var genre in languages)
            {
                yield return new object[] { genre.Key, new List<string> { NamesOfMovies.Target.ToString() }, genre.Value.Yes() };
                yield return new object[] { genre.Key, new List<string> { NamesOfMovies.None.ToString(), NamesOfMovies.Fake.ToString() }, genre.Value.No() };
                yield return new object[] { genre.Key, new List<string> { NamesOfMovies.Target.ToString(), NamesOfMovies.None.ToString(), NamesOfMovies.Fake.ToString() }, genre.Value.Idk() };
            }
        }

        [TestCaseSource(nameof(Cases))]
        [Test]
        public void YesNoIdk_3MoviesButOneHasTargetLanguage_ReturnExpectedMovies(string language, List<string> expectedNamesMovies, Expression<Func<Movie, bool>> filter)
        {
            using (var tempContext = new FilmickContext(options))
            {
                var movie1 = new Movie() { OriginalTitle = NamesOfMovies.Target.ToString(), OriginalLanguage = language };
                var movie2 = new Movie() { OriginalTitle = NamesOfMovies.None.ToString() };
                var movie3 = new Movie() { OriginalTitle = NamesOfMovies.Fake.ToString(), OriginalLanguage = "fake" };

                tempContext.Movies.Add(movie1);
                tempContext.Movies.Add(movie2);
                tempContext.Movies.Add(movie3);
                tempContext.SaveChanges();

                var movies = tempContext.Movies;
                var resultMovies = tempContext.Movies.Where(filter).OrderBy(x => x.OriginalTitle).ToList();

                var expectedMovies = tempContext.Movies.Where(x => expectedNamesMovies.Contains(x.OriginalTitle)).OrderBy(x => x.OriginalTitle).ToList();

                CollectionAssert.AreEqual(expectedMovies, resultMovies);
            }
        }

        [TestCaseSource(nameof(CasesFewLanguages))]
        [Test]
        public void YesNoIdk_5MoviesButThreeHasTargetLanguage_ReturnExpectedCountMovies(List<string> languages, int expectedCountMovies, Expression<Func<Movie, bool>> filter)
        {
            using (var tempContext = new FilmickContext(options))
            {

                foreach (var language in languages)
                {
                    tempContext.Movies.Add(new Movie() { OriginalTitle = NamesOfMovies.Target.ToString(), OriginalLanguage = language });
                }

                var movie4 = new Movie() { OriginalTitle = NamesOfMovies.None.ToString() };
                var movie5 = new Movie() { OriginalTitle = NamesOfMovies.Fake.ToString(), OriginalLanguage = "fake" };

                tempContext.Movies.Add(movie4);
                tempContext.Movies.Add(movie5);
                tempContext.SaveChanges();

                var movies = tempContext.Movies;
                var resultMovies = movies.Where(filter).OrderBy(x => x.OriginalTitle).ToList();

                Assert.AreEqual(resultMovies.Count, expectedCountMovies);
            }
        }
    }
}
