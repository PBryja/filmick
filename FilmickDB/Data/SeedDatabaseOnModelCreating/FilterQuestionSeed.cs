﻿using FilmickDataAccess.Model;
using FilmickDataAccess.Model.Relationships;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FilmickDataAccess.Data.SeedDatabaseOnModelCreating
{
    public static class FilterQuestionData
    {
        public static void FilterQuestionSeed(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<FilterQuestion>().HasData(
                new FilterQuestion
                {
                    FilterQuestionId = 1,
                    DefaultText = "Czy jest to film, w którym aktorzy głównie posługują się językiem azjatyckim?",
                    Name = "Asian",
                    FilterEnum = Model.Enums.FilterEnum.Language_Asian
                },
                new FilterQuestion
                {
                    FilterQuestionId = 2,
                    DefaultText = "Czy jest to film anglojęzyczny?",
                    Name = "English",
                    FilterEnum = Model.Enums.FilterEnum.Language_English
                },
                new FilterQuestion
                {
                    FilterQuestionId = 3,
                    DefaultText = "Czy w filmie aktorzy używają w większości czasu języka francuskiego?",
                    Name = "French",
                    FilterEnum = Model.Enums.FilterEnum.Language_French
                },
                new FilterQuestion
                {
                    FilterQuestionId = 4,
                    DefaultText = "Czy jest to film niemieckojęzyczny?",
                    Name = "German",
                    FilterEnum = Model.Enums.FilterEnum.Language_German
                },
                new FilterQuestion
                {
                    FilterQuestionId = 5,
                    DefaultText = "Czy w filmie bohaterowie mówią głównie po polsku?",
                    Name = "Polish",
                    FilterEnum = Model.Enums.FilterEnum.Language_Polish
                },
                new FilterQuestion
                {
                    FilterQuestionId = 6,
                    DefaultText = "Czy film był wyprodukowany w tym lub poprzednim roku?",
                    Name = "Newest",
                    FilterEnum = Model.Enums.FilterEnum.Newest
                },
                new FilterQuestion
                {
                    FilterQuestionId = 7,
                    DefaultText = "Czy film jest sprzed 2000 roku?",
                    Name = "Old",
                    FilterEnum = Model.Enums.FilterEnum.Old
                },
                new FilterQuestion
                {
                    FilterQuestionId = 8,
                    DefaultText = "Czy film jest sprzed 2010 roku?",
                    Name = "BeginXXI",
                    FilterEnum = Model.Enums.FilterEnum.BeginXXI
                },
                new FilterQuestion
                {
                    FilterQuestionId = 9,
                    DefaultText = "Czy film jest znany na skalę światową?",
                    Name = "Known",
                    FilterEnum = Model.Enums.FilterEnum.Known
                },
                new FilterQuestion
                {
                    FilterQuestionId = 10,
                    DefaultText = "Czy jest to film akcji?",
                    Name = "Action",
                    FilterEnum = Model.Enums.FilterEnum.Genre_Action
                },
                new FilterQuestion
                {
                    FilterQuestionId = 11,
                    DefaultText = "Czy film jest komedią?",
                    Name = "Comedy",
                    FilterEnum = Model.Enums.FilterEnum.Genre_Comedy
                },
                new FilterQuestion
                {
                    FilterQuestionId = 12,
                    DefaultText = "Czy jest to film animowany?",
                    Name = "Animation",
                    FilterEnum = Model.Enums.FilterEnum.Genre_Animation
                },
                new FilterQuestion
                {
                    FilterQuestionId = 13,
                    DefaultText = "Czy szukasz filmu kryminalnego?",
                    Name = "Crime",
                    FilterEnum = Model.Enums.FilterEnum.Genre_Crime
                },
                new FilterQuestion
                {
                    FilterQuestionId = 14,
                    DefaultText = "Czy może był to film dokumentalny lub historyczny?",
                    Name = "DocumentaryOrHistory",
                    FilterEnum = Model.Enums.FilterEnum.Genre_DocumentaryOrHistory
                },
                new FilterQuestion
                {
                    FilterQuestionId = 15,
                    DefaultText = "Czy film należy do gatunków horror lub thriller?",
                    Name = "HorrorOrThriller",
                    FilterEnum = Model.Enums.FilterEnum.Genre_HorrorOrThriller
                },
                new FilterQuestion
                 {
                     FilterQuestionId = 18,
                     DefaultText = "Czy film należy do filmów romantycznych?",
                     Name = "Romance",
                     FilterEnum = Model.Enums.FilterEnum.Genre_Romance
                 },
                 new FilterQuestion
                 {
                     FilterQuestionId = 19,
                     DefaultText = "Czy film pasuje do filmów Fantasy lub Science-Fiction?",
                     Name = "SciFiOrFantasy",
                     FilterEnum = Model.Enums.FilterEnum.Genre_SciFiOrFantasy
                 },
                 new FilterQuestion
                 {
                     FilterQuestionId = 20,
                     DefaultText = "Czy to jest film wojenny?",
                     Name = "War",
                     FilterEnum = Model.Enums.FilterEnum.Genre_War
                 },
                 new FilterQuestion
                 {
                     FilterQuestionId = 21,
                     DefaultText = "Czy akcja dzieje się na dzikim zachodzie (western)?",
                     Name = "Western",
                     FilterEnum = Model.Enums.FilterEnum.Genre_Western
                 }
            );

            //languages
            modelBuilder.Entity<FilterQuestionAssociation>().HasData(CreateAssociations(2, new List<int> { 1, 3, 4, 5 }, FilterQuestionAssociation.TypeEnum.Exclude_WhenOnlyYes));
            modelBuilder.Entity<FilterQuestionAssociation>().HasData(CreateAssociations(1, new List<int> { 2, 3, 4, 5 }, FilterQuestionAssociation.TypeEnum.Exclude_WhenOnlyYes));
            modelBuilder.Entity<FilterQuestionAssociation>().HasData(CreateAssociations(3, new List<int> { 2, 1, 4, 5 }, FilterQuestionAssociation.TypeEnum.Exclude_WhenOnlyYes));
            modelBuilder.Entity<FilterQuestionAssociation>().HasData(CreateAssociations(4, new List<int> { 2, 1, 3, 5 }, FilterQuestionAssociation.TypeEnum.Exclude_WhenOnlyYes));
            modelBuilder.Entity<FilterQuestionAssociation>().HasData(CreateAssociations(5, new List<int> { 2, 1, 3, 4 }, FilterQuestionAssociation.TypeEnum.Exclude_WhenOnlyYes));

            modelBuilder.Entity<FilterQuestionAssociation>().HasData(
                new FilterQuestionAssociation
                {
                    MainId = 6,
                    DependentId = 8,
                    Type = FilterQuestionAssociation.TypeEnum.Exclude_WhenOnlyYes
                },
                new FilterQuestionAssociation
                {
                    MainId = 8,
                    DependentId = 6,
                    Type = FilterQuestionAssociation.TypeEnum.Exclude_WhenOnlyYes
                },
                new FilterQuestionAssociation
                {
                    MainId = 8,
                    DependentId = 7,
                    Type = FilterQuestionAssociation.TypeEnum.Parent_child
                }
            );
        }

        private static List<FilterQuestionAssociation> CreateAssociations(int mainId, List<int> depentIds, FilterQuestionAssociation.TypeEnum typeEnum)
        {
            List<FilterQuestionAssociation> listToReturn = new List<FilterQuestionAssociation>();

            foreach (var id in depentIds)
            {
                listToReturn.Add(new FilterQuestionAssociation
                {
                    DependentId = id,
                    MainId = mainId,
                    Type = typeEnum
                });
            }

            return listToReturn;
        }

    }
}
