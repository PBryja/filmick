﻿using FilmickDataAccess.Model;
using FilmickDataAccess.Model.Relationships;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using FilmickDataAccess.Context;

namespace FilmickDataAccess.Data
{
    public partial class SeedDatabase
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            var context = serviceProvider.GetRequiredService<FilmickContext>();
            var userManager = serviceProvider.GetRequiredService<UserManager<ApplicationUser>>();
            var roleManager = serviceProvider.GetRequiredService<RoleManager<IdentityRole>>();
            context.Database.Migrate();

            if (!context.Roles.Any(x => x.Name == "Admin"))
            {
                roleManager.CreateAsync(new IdentityRole()
                {
                    Name = "Admin",
                    NormalizedName = "Admin",
                    ConcurrencyStamp = Guid.NewGuid().ToString()
                });

            }
        }
    }
}
