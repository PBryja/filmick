﻿using FilmickDataAccess.Model.Assocs;
using FilmickDataAccess.Model.Relationships;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using FilmickDataAccess.Model;
using FilmickDataAccess.Data.SeedDatabaseOnModelCreating;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace FilmickDataAccess.Context
{
    public class FilmickContext : IdentityDbContext<ApplicationUser>
    {
        public FilmickContext(DbContextOptions<FilmickContext> options) : base(options)
        {

        }

        public DbSet<FilterQuestion> FilterQuestions { get; set; }
        public DbSet<Genre> Genres { get; set; }
        public DbSet<Language> Languages { get; set; }
        public DbSet<Movie> Movies { get; set; }
        public DbSet<Text> Texts { get; set; }
        public DbSet<Game> Games { get; set; }
        public DbSet<GameQuestion> GameQuestions { get; set; }
        public DbSet<TagQuestion> TagQuestions { get; set; }
        public DbSet<Keyword> Keywords { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<MovieGenre>()
                .HasKey(x => new { x.MovieId, x.GenreId });

            builder.Entity<MovieTag>()
                .HasKey(x => new { x.MovieId, x.TagQuestionId });

            builder.Entity<FilterQuestionAssociation>()
                .HasKey(x => new { x.MainId, x.DependentId });


            builder.Entity<FilterQuestionAssociation>()
                .HasOne(e => e.Main)
                .WithMany(e => e.Dependents)
                .HasForeignKey(e => e.MainId);

            builder.Entity<FilterQuestionAssociation>()
                .HasOne(e => e.Dependent)
                .WithMany(e => e.DependentTo)
                .HasForeignKey(e => e.DependentId)
                .OnDelete(DeleteBehavior.Restrict);


            builder.Entity<TagQuestionAssociation>()
                .HasKey(x => new { x.MainId, x.DependentId });


            builder.Entity<TagQuestionAssociation>()
                .HasOne(e => e.Main)
                .WithMany(e => e.Dependents)
                .HasForeignKey(e => e.MainId);

            builder.Entity<TagQuestionAssociation>()
                .HasOne(e => e.Dependent)
                .WithMany(e => e.DependentTo)
                .HasForeignKey(e => e.DependentId)
                .OnDelete(DeleteBehavior.Restrict);


            builder.Entity<Game>().HasOne(a => a.ApplicationUser)
                                .WithMany(au => au.Games)
                                .HasForeignKey(a => a.ApplicationUserId);

            builder.Entity<Language>().HasData(
                new Language() { EnglishName = "Polish", Iso639_1 = "Pl", Name = "Polski", LanguageId = 1 }
                );

            builder.Entity<TagKeyword>()
                .HasKey(x => new {x.TagQuestionId, x.KeywordId});

            builder.Entity<TagKeyword>().HasIndex(x => x.TagQuestionId).IsUnique();
            builder.Entity<TagKeyword>().HasIndex(x => x.KeywordId).IsUnique();

            builder.FilterQuestionSeed();

            base.OnModelCreating(builder);
        }

    }
}
