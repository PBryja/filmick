﻿using FilmickDataAccess.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace FilmickDataAccess.ExtensionMethods
{
    public static class DbSetCollection
    {
        public static IQueryable<T> IncludeAllCollections<T>(this IQueryable<T> queryable) where T : class
        {
            var type = typeof(T);
            var properties = type.GetProperties();
            foreach (var property in properties)
            {
                if (property.PropertyType.IsGenericType && typeof(ICollection<>).IsAssignableFrom(property.PropertyType.GetGenericTypeDefinition()))
                 {
                    queryable = queryable.Include(property.Name);
                }
            }
            return queryable;
        }

        public static IQueryable<TagQuestion> IncludeAll(this DbSet<TagQuestion> queryable)
        {
           var newqueryable = (IQueryable<TagQuestion>)queryable.Include(x=>x.Dependents).ThenInclude(x=>x.Dependent)
                .Include(x=>x.DependentTo).ThenInclude(x=>x.Main)
                .Include(x=>x.MovieTags).ThenInclude(x=>x.Movie)
                .Include(x=>x.TagKeyword).ThenInclude(x=>x.Keyword);

            return newqueryable;
        }
    }
}
