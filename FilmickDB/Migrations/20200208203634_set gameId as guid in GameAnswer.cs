﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Filmick.Migrations
{
    public partial class setgameIdasguidinGameAnswer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_GameAnswers_Games_GameId1",
                table: "GameAnswers");

            migrationBuilder.DropIndex(
                name: "IX_GameAnswers_GameId1",
                table: "GameAnswers");

            migrationBuilder.DropColumn(
                name: "GameId1",
                table: "GameAnswers");

            migrationBuilder.AlterColumn<Guid>(
                name: "GameId",
                table: "GameAnswers",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_GameAnswers_GameId",
                table: "GameAnswers",
                column: "GameId");

            migrationBuilder.AddForeignKey(
                name: "FK_GameAnswers_Games_GameId",
                table: "GameAnswers",
                column: "GameId",
                principalTable: "Games",
                principalColumn: "GameId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_GameAnswers_Games_GameId",
                table: "GameAnswers");

            migrationBuilder.DropIndex(
                name: "IX_GameAnswers_GameId",
                table: "GameAnswers");

            migrationBuilder.AlterColumn<string>(
                name: "GameId",
                table: "GameAnswers",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(Guid));

            migrationBuilder.AddColumn<Guid>(
                name: "GameId1",
                table: "GameAnswers",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_GameAnswers_GameId1",
                table: "GameAnswers",
                column: "GameId1");

            migrationBuilder.AddForeignKey(
                name: "FK_GameAnswers_Games_GameId1",
                table: "GameAnswers",
                column: "GameId1",
                principalTable: "Games",
                principalColumn: "GameId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
