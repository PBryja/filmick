﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Filmick.Migrations
{
    public partial class AdddefaultvaluetoDescription : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "FilterQuestions",
                keyColumn: "FilterQuestionId",
                keyValue: 1,
                column: "Description",
                value: "");

            migrationBuilder.UpdateData(
                table: "FilterQuestions",
                keyColumn: "FilterQuestionId",
                keyValue: 2,
                column: "Description",
                value: "");

            migrationBuilder.UpdateData(
                table: "FilterQuestions",
                keyColumn: "FilterQuestionId",
                keyValue: 3,
                column: "Description",
                value: "");

            migrationBuilder.UpdateData(
                table: "FilterQuestions",
                keyColumn: "FilterQuestionId",
                keyValue: 4,
                column: "Description",
                value: "");

            migrationBuilder.UpdateData(
                table: "FilterQuestions",
                keyColumn: "FilterQuestionId",
                keyValue: 5,
                column: "Description",
                value: "");

            migrationBuilder.UpdateData(
                table: "FilterQuestions",
                keyColumn: "FilterQuestionId",
                keyValue: 6,
                column: "Description",
                value: "");

            migrationBuilder.UpdateData(
                table: "FilterQuestions",
                keyColumn: "FilterQuestionId",
                keyValue: 7,
                column: "Description",
                value: "");

            migrationBuilder.UpdateData(
                table: "FilterQuestions",
                keyColumn: "FilterQuestionId",
                keyValue: 8,
                column: "Description",
                value: "");

            migrationBuilder.UpdateData(
                table: "FilterQuestions",
                keyColumn: "FilterQuestionId",
                keyValue: 9,
                column: "Description",
                value: "");

            migrationBuilder.UpdateData(
                table: "FilterQuestions",
                keyColumn: "FilterQuestionId",
                keyValue: 10,
                column: "Description",
                value: "");

            migrationBuilder.UpdateData(
                table: "FilterQuestions",
                keyColumn: "FilterQuestionId",
                keyValue: 11,
                column: "Description",
                value: "");

            migrationBuilder.UpdateData(
                table: "FilterQuestions",
                keyColumn: "FilterQuestionId",
                keyValue: 12,
                column: "Description",
                value: "");

            migrationBuilder.UpdateData(
                table: "FilterQuestions",
                keyColumn: "FilterQuestionId",
                keyValue: 13,
                column: "Description",
                value: "");

            migrationBuilder.UpdateData(
                table: "FilterQuestions",
                keyColumn: "FilterQuestionId",
                keyValue: 14,
                column: "Description",
                value: "");

            migrationBuilder.UpdateData(
                table: "FilterQuestions",
                keyColumn: "FilterQuestionId",
                keyValue: 15,
                column: "Description",
                value: "");

            migrationBuilder.UpdateData(
                table: "FilterQuestions",
                keyColumn: "FilterQuestionId",
                keyValue: 18,
                column: "Description",
                value: "");

            migrationBuilder.UpdateData(
                table: "FilterQuestions",
                keyColumn: "FilterQuestionId",
                keyValue: 19,
                column: "Description",
                value: "");

            migrationBuilder.UpdateData(
                table: "FilterQuestions",
                keyColumn: "FilterQuestionId",
                keyValue: 20,
                column: "Description",
                value: "");

            migrationBuilder.UpdateData(
                table: "FilterQuestions",
                keyColumn: "FilterQuestionId",
                keyValue: 21,
                column: "Description",
                value: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "FilterQuestions",
                keyColumn: "FilterQuestionId",
                keyValue: 1,
                column: "Description",
                value: null);

            migrationBuilder.UpdateData(
                table: "FilterQuestions",
                keyColumn: "FilterQuestionId",
                keyValue: 2,
                column: "Description",
                value: null);

            migrationBuilder.UpdateData(
                table: "FilterQuestions",
                keyColumn: "FilterQuestionId",
                keyValue: 3,
                column: "Description",
                value: null);

            migrationBuilder.UpdateData(
                table: "FilterQuestions",
                keyColumn: "FilterQuestionId",
                keyValue: 4,
                column: "Description",
                value: null);

            migrationBuilder.UpdateData(
                table: "FilterQuestions",
                keyColumn: "FilterQuestionId",
                keyValue: 5,
                column: "Description",
                value: null);

            migrationBuilder.UpdateData(
                table: "FilterQuestions",
                keyColumn: "FilterQuestionId",
                keyValue: 6,
                column: "Description",
                value: null);

            migrationBuilder.UpdateData(
                table: "FilterQuestions",
                keyColumn: "FilterQuestionId",
                keyValue: 7,
                column: "Description",
                value: null);

            migrationBuilder.UpdateData(
                table: "FilterQuestions",
                keyColumn: "FilterQuestionId",
                keyValue: 8,
                column: "Description",
                value: null);

            migrationBuilder.UpdateData(
                table: "FilterQuestions",
                keyColumn: "FilterQuestionId",
                keyValue: 9,
                column: "Description",
                value: null);

            migrationBuilder.UpdateData(
                table: "FilterQuestions",
                keyColumn: "FilterQuestionId",
                keyValue: 10,
                column: "Description",
                value: null);

            migrationBuilder.UpdateData(
                table: "FilterQuestions",
                keyColumn: "FilterQuestionId",
                keyValue: 11,
                column: "Description",
                value: null);

            migrationBuilder.UpdateData(
                table: "FilterQuestions",
                keyColumn: "FilterQuestionId",
                keyValue: 12,
                column: "Description",
                value: null);

            migrationBuilder.UpdateData(
                table: "FilterQuestions",
                keyColumn: "FilterQuestionId",
                keyValue: 13,
                column: "Description",
                value: null);

            migrationBuilder.UpdateData(
                table: "FilterQuestions",
                keyColumn: "FilterQuestionId",
                keyValue: 14,
                column: "Description",
                value: null);

            migrationBuilder.UpdateData(
                table: "FilterQuestions",
                keyColumn: "FilterQuestionId",
                keyValue: 15,
                column: "Description",
                value: null);

            migrationBuilder.UpdateData(
                table: "FilterQuestions",
                keyColumn: "FilterQuestionId",
                keyValue: 18,
                column: "Description",
                value: null);

            migrationBuilder.UpdateData(
                table: "FilterQuestions",
                keyColumn: "FilterQuestionId",
                keyValue: 19,
                column: "Description",
                value: null);

            migrationBuilder.UpdateData(
                table: "FilterQuestions",
                keyColumn: "FilterQuestionId",
                keyValue: 20,
                column: "Description",
                value: null);

            migrationBuilder.UpdateData(
                table: "FilterQuestions",
                keyColumn: "FilterQuestionId",
                keyValue: 21,
                column: "Description",
                value: null);
        }
    }
}
