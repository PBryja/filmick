﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Filmick.Migrations
{
    public partial class RenameGameAnswertoGameQuestion2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_GameAnswers_FilterQuestions_FilterQuestionId",
                table: "GameAnswers");

            migrationBuilder.DropForeignKey(
                name: "FK_GameAnswers_Games_GameId",
                table: "GameAnswers");

            migrationBuilder.DropForeignKey(
                name: "FK_GameAnswers_TagQuestions_TagQuestionId",
                table: "GameAnswers");

            migrationBuilder.DropPrimaryKey(
                name: "PK_GameAnswers",
                table: "GameAnswers");

            migrationBuilder.RenameTable(
                name: "GameAnswers",
                newName: "GameQuestions");

            migrationBuilder.RenameIndex(
                name: "IX_GameAnswers_TagQuestionId",
                table: "GameQuestions",
                newName: "IX_GameQuestions_TagQuestionId");

            migrationBuilder.RenameIndex(
                name: "IX_GameAnswers_GameId",
                table: "GameQuestions",
                newName: "IX_GameQuestions_GameId");

            migrationBuilder.RenameIndex(
                name: "IX_GameAnswers_FilterQuestionId",
                table: "GameQuestions",
                newName: "IX_GameQuestions_FilterQuestionId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_GameQuestions",
                table: "GameQuestions",
                column: "GameQuestionId");

            migrationBuilder.AddForeignKey(
                name: "FK_GameQuestions_FilterQuestions_FilterQuestionId",
                table: "GameQuestions",
                column: "FilterQuestionId",
                principalTable: "FilterQuestions",
                principalColumn: "FilterQuestionId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_GameQuestions_Games_GameId",
                table: "GameQuestions",
                column: "GameId",
                principalTable: "Games",
                principalColumn: "GameId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_GameQuestions_TagQuestions_TagQuestionId",
                table: "GameQuestions",
                column: "TagQuestionId",
                principalTable: "TagQuestions",
                principalColumn: "TagQuestionId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_GameQuestions_FilterQuestions_FilterQuestionId",
                table: "GameQuestions");

            migrationBuilder.DropForeignKey(
                name: "FK_GameQuestions_Games_GameId",
                table: "GameQuestions");

            migrationBuilder.DropForeignKey(
                name: "FK_GameQuestions_TagQuestions_TagQuestionId",
                table: "GameQuestions");

            migrationBuilder.DropPrimaryKey(
                name: "PK_GameQuestions",
                table: "GameQuestions");

            migrationBuilder.RenameTable(
                name: "GameQuestions",
                newName: "GameAnswers");

            migrationBuilder.RenameIndex(
                name: "IX_GameQuestions_TagQuestionId",
                table: "GameAnswers",
                newName: "IX_GameAnswers_TagQuestionId");

            migrationBuilder.RenameIndex(
                name: "IX_GameQuestions_GameId",
                table: "GameAnswers",
                newName: "IX_GameAnswers_GameId");

            migrationBuilder.RenameIndex(
                name: "IX_GameQuestions_FilterQuestionId",
                table: "GameAnswers",
                newName: "IX_GameAnswers_FilterQuestionId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_GameAnswers",
                table: "GameAnswers",
                column: "GameQuestionId");

            migrationBuilder.AddForeignKey(
                name: "FK_GameAnswers_FilterQuestions_FilterQuestionId",
                table: "GameAnswers",
                column: "FilterQuestionId",
                principalTable: "FilterQuestions",
                principalColumn: "FilterQuestionId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_GameAnswers_Games_GameId",
                table: "GameAnswers",
                column: "GameId",
                principalTable: "Games",
                principalColumn: "GameId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_GameAnswers_TagQuestions_TagQuestionId",
                table: "GameAnswers",
                column: "TagQuestionId",
                principalTable: "TagQuestions",
                principalColumn: "TagQuestionId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
