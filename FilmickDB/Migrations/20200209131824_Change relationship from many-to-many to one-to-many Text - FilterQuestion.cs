﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Filmick.Migrations
{
    public partial class ChangerelationshipfrommanytomanytoonetomanyTextFilterQuestion : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "FilterQuestionText");

            migrationBuilder.DropColumn(
                name: "FilterQuestionTextId",
                table: "Texts");

            migrationBuilder.AddColumn<int>(
                name: "FilterQuestionId",
                table: "Texts",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Texts_FilterQuestionId",
                table: "Texts",
                column: "FilterQuestionId");

            migrationBuilder.AddForeignKey(
                name: "FK_Texts_FilterQuestions_FilterQuestionId",
                table: "Texts",
                column: "FilterQuestionId",
                principalTable: "FilterQuestions",
                principalColumn: "FilterQuestionId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Texts_FilterQuestions_FilterQuestionId",
                table: "Texts");

            migrationBuilder.DropIndex(
                name: "IX_Texts_FilterQuestionId",
                table: "Texts");

            migrationBuilder.DropColumn(
                name: "FilterQuestionId",
                table: "Texts");

            migrationBuilder.AddColumn<int>(
                name: "FilterQuestionTextId",
                table: "Texts",
                type: "int",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "FilterQuestionText",
                columns: table => new
                {
                    FilterQuestionId = table.Column<int>(type: "int", nullable: false),
                    TextId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FilterQuestionText", x => new { x.FilterQuestionId, x.TextId });
                    table.ForeignKey(
                        name: "FK_FilterQuestionText_FilterQuestions_FilterQuestionId",
                        column: x => x.FilterQuestionId,
                        principalTable: "FilterQuestions",
                        principalColumn: "FilterQuestionId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_FilterQuestionText_Texts_TextId",
                        column: x => x.TextId,
                        principalTable: "Texts",
                        principalColumn: "TextId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_FilterQuestionText_TextId",
                table: "FilterQuestionText",
                column: "TextId",
                unique: true);
        }
    }
}
