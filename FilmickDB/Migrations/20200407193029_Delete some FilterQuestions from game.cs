﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Filmick.Migrations
{
    public partial class DeletesomeFilterQuestionsfromgame : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "FilterQuestions",
                keyColumn: "FilterQuestionId",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "FilterQuestions",
                keyColumn: "FilterQuestionId",
                keyValue: 17);

            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "FilterQuestions",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "FilterQuestions",
                keyColumn: "FilterQuestionId",
                keyValue: 1,
                column: "DefaultText",
                value: "Czy jest to film, w którym aktorzy głównie posługują się językiem azjatyckim?");

            migrationBuilder.UpdateData(
                table: "FilterQuestions",
                keyColumn: "FilterQuestionId",
                keyValue: 2,
                column: "DefaultText",
                value: "Czy jest to film anglojęzyczny?");

            migrationBuilder.UpdateData(
                table: "FilterQuestions",
                keyColumn: "FilterQuestionId",
                keyValue: 3,
                column: "DefaultText",
                value: "Czy w filmie aktorzy używają w większości czasu języka francuskiego?");

            migrationBuilder.UpdateData(
                table: "FilterQuestions",
                keyColumn: "FilterQuestionId",
                keyValue: 4,
                column: "DefaultText",
                value: "Czy jest to film niemieckojęzyczny?");

            migrationBuilder.UpdateData(
                table: "FilterQuestions",
                keyColumn: "FilterQuestionId",
                keyValue: 5,
                column: "DefaultText",
                value: "Czy w filmie bohaterowie mówią głównie po polsku?");

            migrationBuilder.UpdateData(
                table: "FilterQuestions",
                keyColumn: "FilterQuestionId",
                keyValue: 6,
                column: "DefaultText",
                value: "Czy film był wyprodukowany w tym lub poprzednim roku?");

            migrationBuilder.UpdateData(
                table: "FilterQuestions",
                keyColumn: "FilterQuestionId",
                keyValue: 13,
                column: "DefaultText",
                value: "Czy szukasz filmu kryminalnego?");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Description",
                table: "FilterQuestions");

            migrationBuilder.UpdateData(
                table: "FilterQuestions",
                keyColumn: "FilterQuestionId",
                keyValue: 1,
                column: "DefaultText",
                value: "Czy w filmie aktorzy mają posługiwać się głównie azjatyckim językiem?");

            migrationBuilder.UpdateData(
                table: "FilterQuestions",
                keyColumn: "FilterQuestionId",
                keyValue: 2,
                column: "DefaultText",
                value: "Czy w filmie ma przeważać język angielski?");

            migrationBuilder.UpdateData(
                table: "FilterQuestions",
                keyColumn: "FilterQuestionId",
                keyValue: 3,
                column: "DefaultText",
                value: "Czy film ma być głównie francuskojęzyczny?");

            migrationBuilder.UpdateData(
                table: "FilterQuestions",
                keyColumn: "FilterQuestionId",
                keyValue: 4,
                column: "DefaultText",
                value: "Czy mają to być filmy niemieckojęzyczne?");

            migrationBuilder.UpdateData(
                table: "FilterQuestions",
                keyColumn: "FilterQuestionId",
                keyValue: 5,
                column: "DefaultText",
                value: "Czy chcesz otrzymać tylko filmy polskojęzyczne?");

            migrationBuilder.UpdateData(
                table: "FilterQuestions",
                keyColumn: "FilterQuestionId",
                keyValue: 6,
                column: "DefaultText",
                value: "Czy film jest z tego lub poprzedniego roku?");

            migrationBuilder.UpdateData(
                table: "FilterQuestions",
                keyColumn: "FilterQuestionId",
                keyValue: 13,
                column: "DefaultText",
                value: "Czy był to film kryminalny?");

            migrationBuilder.InsertData(
                table: "FilterQuestions",
                columns: new[] { "FilterQuestionId", "DefaultText", "FilterEnum", "Name" },
                values: new object[,]
                {
                    { 16, "Czy film jest muzyczny?", 108, "Music" },
                    { 17, "Czy w filmie występuje motyw tajemniczości lub zagadki?", 112, "Mystery" }
                });
        }
    }
}
