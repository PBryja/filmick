﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Filmick.Migrations
{
    public partial class AddinitdatafilterQuestions : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "FilterQuestions",
                columns: new[] { "FilterQuestionId", "DefaultText", "FilterEnum", "Name" },
                values: new object[,]
                {
                    { 1, "Czy w filmie aktorzy mają posługiwać się głównie azjatyckim językiem?", 205, "Asian" },
                    { 19, "Czy film pasuje do filmów Fantasy lub Science-Fiction?", 107, "SciFiOrFantasy" },
                    { 18, "Czy film należy do filmów romantycznych?", 104, "Romance" },
                    { 17, "Czy w filmie występuje motyw tajemniczości lub zagadki?", 112, "Mystery" },
                    { 16, "Czy film jest muzyczny?", 108, "Music" },
                    { 15, "Czy film należy do gatunków horror lub thriller?", 103, "HorrorOrThriller" },
                    { 14, "Czy może był to film dokumentalny lub historyczny?", 106, "DocumentaryOrHistory" },
                    { 13, "Czy był to film kryminalny?", 105, "Crime" },
                    { 12, "Czy jest to film animowany?", 111, "Animation" },
                    { 20, "Czy to jest film wojenny?", 109, "War" },
                    { 11, "Czy film jest komedią?", 102, "Comedy" },
                    { 9, "Czy film jest znany na skalę światową?", 301, "Known" },
                    { 8, "Czy film jest sprzed 2010 roku?", 305, "BeginXXI" },
                    { 7, "Czy film jest sprzed 2000 roku?", 304, "Old" },
                    { 6, "Czy film jest z tego lub poprzedniego roku?", 303, "Newest" },
                    { 5, "Czy chcesz otrzymać tylko filmy polskojęzyczne?", 202, "Polish" },
                    { 4, "Czy mają to być filmy niemieckojęzyczne?", 203, "German" },
                    { 3, "Czy film ma być głównie francuskojęzyczny?", 204, "French" },
                    { 2, "Czy w filmie ma przeważać język angielski?", 201, "English" },
                    { 10, "Czy jest to film akcji?", 101, "Action" },
                    { 21, "Czy akcja dzieje się na dzikim zachodzie (western)?", 110, "Western" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "FilterQuestions",
                keyColumn: "FilterQuestionId",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "FilterQuestions",
                keyColumn: "FilterQuestionId",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "FilterQuestions",
                keyColumn: "FilterQuestionId",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "FilterQuestions",
                keyColumn: "FilterQuestionId",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "FilterQuestions",
                keyColumn: "FilterQuestionId",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "FilterQuestions",
                keyColumn: "FilterQuestionId",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "FilterQuestions",
                keyColumn: "FilterQuestionId",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "FilterQuestions",
                keyColumn: "FilterQuestionId",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "FilterQuestions",
                keyColumn: "FilterQuestionId",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "FilterQuestions",
                keyColumn: "FilterQuestionId",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "FilterQuestions",
                keyColumn: "FilterQuestionId",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "FilterQuestions",
                keyColumn: "FilterQuestionId",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "FilterQuestions",
                keyColumn: "FilterQuestionId",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "FilterQuestions",
                keyColumn: "FilterQuestionId",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "FilterQuestions",
                keyColumn: "FilterQuestionId",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "FilterQuestions",
                keyColumn: "FilterQuestionId",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "FilterQuestions",
                keyColumn: "FilterQuestionId",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "FilterQuestions",
                keyColumn: "FilterQuestionId",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "FilterQuestions",
                keyColumn: "FilterQuestionId",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "FilterQuestions",
                keyColumn: "FilterQuestionId",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "FilterQuestions",
                keyColumn: "FilterQuestionId",
                keyValue: 21);
        }
    }
}
