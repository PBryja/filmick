﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Filmick.Migrations
{
    public partial class RenameMovieTagcolumn : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Movies_TagQuestions_TagQuestionId",
                table: "Movies");

            migrationBuilder.DropIndex(
                name: "IX_Movies_TagQuestionId",
                table: "Movies");

            migrationBuilder.DropColumn(
                name: "TagQuestionId",
                table: "Movies");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "TagQuestionId",
                table: "Movies",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Movies_TagQuestionId",
                table: "Movies",
                column: "TagQuestionId");

            migrationBuilder.AddForeignKey(
                name: "FK_Movies_TagQuestions_TagQuestionId",
                table: "Movies",
                column: "TagQuestionId",
                principalTable: "TagQuestions",
                principalColumn: "TagQuestionId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
