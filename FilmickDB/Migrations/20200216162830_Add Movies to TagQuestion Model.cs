﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Filmick.Migrations
{
    public partial class AddMoviestoTagQuestionModel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ExcludeMovies",
                table: "TagQuestions");

            migrationBuilder.AddColumn<int>(
                name: "ExcludeMoviesType",
                table: "TagQuestions",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "TagQuestionId",
                table: "Movies",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Movies_TagQuestionId",
                table: "Movies",
                column: "TagQuestionId");

            migrationBuilder.AddForeignKey(
                name: "FK_Movies_TagQuestions_TagQuestionId",
                table: "Movies",
                column: "TagQuestionId",
                principalTable: "TagQuestions",
                principalColumn: "TagQuestionId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Movies_TagQuestions_TagQuestionId",
                table: "Movies");

            migrationBuilder.DropIndex(
                name: "IX_Movies_TagQuestionId",
                table: "Movies");

            migrationBuilder.DropColumn(
                name: "ExcludeMoviesType",
                table: "TagQuestions");

            migrationBuilder.DropColumn(
                name: "TagQuestionId",
                table: "Movies");

            migrationBuilder.AddColumn<int>(
                name: "ExcludeMovies",
                table: "TagQuestions",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}
