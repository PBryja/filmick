﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Filmick.Migrations
{
    public partial class Createnecessarymodelsforgameandquestion : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_FilterQuestionAssociation_FilterQuestion_DependentId",
                table: "FilterQuestionAssociation");

            migrationBuilder.DropForeignKey(
                name: "FK_FilterQuestionAssociation_FilterQuestion_MainId",
                table: "FilterQuestionAssociation");

            migrationBuilder.DropForeignKey(
                name: "FK_FilterQuestionText_FilterQuestion_FilterQuestionId",
                table: "FilterQuestionText");

            migrationBuilder.DropForeignKey(
                name: "FK_FilterQuestionText_Text_TextId",
                table: "FilterQuestionText");

            migrationBuilder.DropForeignKey(
                name: "FK_MovieGenre_Genre_GenreId",
                table: "MovieGenre");

            migrationBuilder.DropForeignKey(
                name: "FK_MovieGenre_Movie_MovieId",
                table: "MovieGenre");

            migrationBuilder.DropIndex(
                name: "IX_FilterQuestionText_TextId",
                table: "FilterQuestionText");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Text",
                table: "Texts");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Movie",
                table: "Movie");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Genre",
                table: "Genre");

            migrationBuilder.DropPrimaryKey(
                name: "PK_FilterQuestion",
                table: "FilterQuestion");

            migrationBuilder.RenameTable(
                name: "Movie",
                newName: "Movies");

            migrationBuilder.RenameTable(
                name: "Genre",
                newName: "Genres");

            migrationBuilder.RenameTable(
                name: "FilterQuestion",
                newName: "FilterQuestions");

            migrationBuilder.AddColumn<int>(
                name: "FilterQuestionTextId",
                table: "Texts",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "LanguageId",
                table: "Texts",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<bool>(
                name: "IsTag",
                table: "FilterQuestions",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Texts",
                table: "Texts",
                column: "TextId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Movies",
                table: "Movies",
                column: "MovieId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Genres",
                table: "Genres",
                column: "GenreId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_FilterQuestions",
                table: "FilterQuestions",
                column: "FilterQuestionId");

            migrationBuilder.CreateTable(
                name: "Games",
                columns: table => new
                {
                    GameId = table.Column<Guid>(nullable: false),
                    ApplicationUserId = table.Column<string>(nullable: true),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    Status = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Games", x => x.GameId);
                    table.ForeignKey(
                        name: "FK_Games_AspNetUsers_ApplicationUserId",
                        column: x => x.ApplicationUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Languages",
                columns: table => new
                {
                    LanguageId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true),
                    EnglishName = table.Column<string>(nullable: true),
                    Iso639_1 = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Languages", x => x.LanguageId);
                });

            migrationBuilder.CreateTable(
                name: "GameAnswers",
                columns: table => new
                {
                    GameAnswerId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    GameId = table.Column<int>(nullable: false),
                    GameId1 = table.Column<Guid>(nullable: true),
                    FilterQuestionId = table.Column<int>(nullable: false),
                    Answer = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GameAnswers", x => x.GameAnswerId);
                    table.ForeignKey(
                        name: "FK_GameAnswers_FilterQuestions_FilterQuestionId",
                        column: x => x.FilterQuestionId,
                        principalTable: "FilterQuestions",
                        principalColumn: "FilterQuestionId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_GameAnswers_Games_GameId1",
                        column: x => x.GameId1,
                        principalTable: "Games",
                        principalColumn: "GameId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "Languages",
                columns: new[] { "LanguageId", "EnglishName", "Iso639_1", "Name" },
                values: new object[] { 1, "Polish", "pl", "Polski" });

            migrationBuilder.CreateIndex(
                name: "IX_FilterQuestionText_TextId",
                table: "FilterQuestionText",
                column: "TextId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Texts_LanguageId",
                table: "Texts",
                column: "LanguageId");

            migrationBuilder.CreateIndex(
                name: "IX_GameAnswers_FilterQuestionId",
                table: "GameAnswers",
                column: "FilterQuestionId");

            migrationBuilder.CreateIndex(
                name: "IX_GameAnswers_GameId1",
                table: "GameAnswers",
                column: "GameId1");

            migrationBuilder.CreateIndex(
                name: "IX_Games_ApplicationUserId",
                table: "Games",
                column: "ApplicationUserId");

            migrationBuilder.AddForeignKey(
                name: "FK_FilterQuestionAssociation_FilterQuestions_DependentId",
                table: "FilterQuestionAssociation",
                column: "DependentId",
                principalTable: "FilterQuestions",
                principalColumn: "FilterQuestionId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_FilterQuestionAssociation_FilterQuestions_MainId",
                table: "FilterQuestionAssociation",
                column: "MainId",
                principalTable: "FilterQuestions",
                principalColumn: "FilterQuestionId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_FilterQuestionText_FilterQuestions_FilterQuestionId",
                table: "FilterQuestionText",
                column: "FilterQuestionId",
                principalTable: "FilterQuestions",
                principalColumn: "FilterQuestionId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_FilterQuestionText_Texts_TextId",
                table: "FilterQuestionText",
                column: "TextId",
                principalTable: "Texts",
                principalColumn: "TextId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_MovieGenre_Genres_GenreId",
                table: "MovieGenre",
                column: "GenreId",
                principalTable: "Genres",
                principalColumn: "GenreId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_MovieGenre_Movies_MovieId",
                table: "MovieGenre",
                column: "MovieId",
                principalTable: "Movies",
                principalColumn: "MovieId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Texts_Languages_LanguageId",
                table: "Texts",
                column: "LanguageId",
                principalTable: "Languages",
                principalColumn: "LanguageId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_FilterQuestionAssociation_FilterQuestions_DependentId",
                table: "FilterQuestionAssociation");

            migrationBuilder.DropForeignKey(
                name: "FK_FilterQuestionAssociation_FilterQuestions_MainId",
                table: "FilterQuestionAssociation");

            migrationBuilder.DropForeignKey(
                name: "FK_FilterQuestionText_FilterQuestions_FilterQuestionId",
                table: "FilterQuestionText");

            migrationBuilder.DropForeignKey(
                name: "FK_FilterQuestionText_Texts_TextId",
                table: "FilterQuestionText");

            migrationBuilder.DropForeignKey(
                name: "FK_MovieGenre_Genres_GenreId",
                table: "MovieGenre");

            migrationBuilder.DropForeignKey(
                name: "FK_MovieGenre_Movies_MovieId",
                table: "MovieGenre");

            migrationBuilder.DropForeignKey(
                name: "FK_Texts_Languages_LanguageId",
                table: "Texts");

            migrationBuilder.DropTable(
                name: "GameAnswers");

            migrationBuilder.DropTable(
                name: "Languages");

            migrationBuilder.DropTable(
                name: "Games");

            migrationBuilder.DropIndex(
                name: "IX_FilterQuestionText_TextId",
                table: "FilterQuestionText");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Texts",
                table: "Texts");

            migrationBuilder.DropIndex(
                name: "IX_Texts_LanguageId",
                table: "Texts");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Movies",
                table: "Movies");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Genres",
                table: "Genres");

            migrationBuilder.DropPrimaryKey(
                name: "PK_FilterQuestions",
                table: "FilterQuestions");

            migrationBuilder.DropColumn(
                name: "FilterQuestionTextId",
                table: "Texts");

            migrationBuilder.DropColumn(
                name: "LanguageId",
                table: "Texts");

            migrationBuilder.DropColumn(
                name: "IsTag",
                table: "FilterQuestions");

            migrationBuilder.RenameTable(
                name: "Movies",
                newName: "Movie");

            migrationBuilder.RenameTable(
                name: "Genres",
                newName: "Genre");

            migrationBuilder.RenameTable(
                name: "FilterQuestions",
                newName: "FilterQuestion");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Text",
                table: "Texts",
                column: "TextId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Movie",
                table: "Movie",
                column: "MovieId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Genre",
                table: "Genre",
                column: "GenreId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_FilterQuestion",
                table: "FilterQuestion",
                column: "FilterQuestionId");

            migrationBuilder.CreateIndex(
                name: "IX_FilterQuestionText_TextId",
                table: "FilterQuestionText",
                column: "TextId");

            migrationBuilder.AddForeignKey(
                name: "FK_FilterQuestionAssociation_FilterQuestion_DependentId",
                table: "FilterQuestionAssociation",
                column: "DependentId",
                principalTable: "FilterQuestion",
                principalColumn: "FilterQuestionId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_FilterQuestionAssociation_FilterQuestion_MainId",
                table: "FilterQuestionAssociation",
                column: "MainId",
                principalTable: "FilterQuestion",
                principalColumn: "FilterQuestionId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_FilterQuestionText_FilterQuestion_FilterQuestionId",
                table: "FilterQuestionText",
                column: "FilterQuestionId",
                principalTable: "FilterQuestion",
                principalColumn: "FilterQuestionId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_FilterQuestionText_Text_TextId",
                table: "FilterQuestionText",
                column: "TextId",
                principalTable: "Texts",
                principalColumn: "TextId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_MovieGenre_Genre_GenreId",
                table: "MovieGenre",
                column: "GenreId",
                principalTable: "Genre",
                principalColumn: "GenreId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_MovieGenre_Movie_MovieId",
                table: "MovieGenre",
                column: "MovieId",
                principalTable: "Movie",
                principalColumn: "MovieId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
