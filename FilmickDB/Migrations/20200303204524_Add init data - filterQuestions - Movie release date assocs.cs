﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Filmick.Migrations
{
    public partial class AddinitdatafilterQuestionsMoviereleasedateassocs : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "FilterQuestionAssociation",
                columns: new[] { "MainId", "DependentId", "Type" },
                values: new object[] { 6, 8, 1 });

            migrationBuilder.InsertData(
                table: "FilterQuestionAssociation",
                columns: new[] { "MainId", "DependentId", "Type" },
                values: new object[] { 8, 6, 1 });

            migrationBuilder.InsertData(
                table: "FilterQuestionAssociation",
                columns: new[] { "MainId", "DependentId", "Type" },
                values: new object[] { 8, 7, 0 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "FilterQuestionAssociation",
                keyColumns: new[] { "MainId", "DependentId" },
                keyValues: new object[] { 6, 8 });

            migrationBuilder.DeleteData(
                table: "FilterQuestionAssociation",
                keyColumns: new[] { "MainId", "DependentId" },
                keyValues: new object[] { 8, 6 });

            migrationBuilder.DeleteData(
                table: "FilterQuestionAssociation",
                keyColumns: new[] { "MainId", "DependentId" },
                keyValues: new object[] { 8, 7 });
        }
    }
}
