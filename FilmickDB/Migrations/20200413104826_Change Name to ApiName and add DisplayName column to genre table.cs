﻿using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Migrations.Operations;

namespace Filmick.Migrations
{
    public partial class ChangeNametoApiNameandaddDisplayNamecolumntogenretable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn("Name", "Genres", "ApiName");

            migrationBuilder.AddColumn<string>(
                name: "DisplayName",
                table: "Genres",
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn("ApiName", "Genres", "Name");

            migrationBuilder.DropColumn(
                name: "DisplayName",
                table: "Genres");

        }
    }
}
