﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Filmick.Migrations
{
    public partial class AddTagQuestiontables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_GameAnswers_FilterQuestions_FilterQuestionId",
                table: "GameAnswers");

            migrationBuilder.AddColumn<int>(
                name: "TagQuestionId",
                table: "Texts",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "FilterQuestionId",
                table: "GameAnswers",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddColumn<int>(
                name: "TagQuestionId",
                table: "GameAnswers",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "TagQuestions",
                columns: table => new
                {
                    TagQuestionId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: false),
                    DefaultText = table.Column<string>(nullable: false),
                    ExcludeMovies = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TagQuestions", x => x.TagQuestionId);
                });

            migrationBuilder.CreateTable(
                name: "MovieTag",
                columns: table => new
                {
                    MovieId = table.Column<int>(nullable: false),
                    TagQuestionId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MovieTag", x => new { x.MovieId, x.TagQuestionId });
                    table.ForeignKey(
                        name: "FK_MovieTag_Movies_MovieId",
                        column: x => x.MovieId,
                        principalTable: "Movies",
                        principalColumn: "MovieId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MovieTag_TagQuestions_TagQuestionId",
                        column: x => x.TagQuestionId,
                        principalTable: "TagQuestions",
                        principalColumn: "TagQuestionId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TagQuestionAssociation",
                columns: table => new
                {
                    MainId = table.Column<int>(nullable: false),
                    DependentId = table.Column<int>(nullable: false),
                    Type = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TagQuestionAssociation", x => new { x.MainId, x.DependentId });
                    table.ForeignKey(
                        name: "FK_TagQuestionAssociation_TagQuestions_DependentId",
                        column: x => x.DependentId,
                        principalTable: "TagQuestions",
                        principalColumn: "TagQuestionId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TagQuestionAssociation_TagQuestions_MainId",
                        column: x => x.MainId,
                        principalTable: "TagQuestions",
                        principalColumn: "TagQuestionId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Texts_TagQuestionId",
                table: "Texts",
                column: "TagQuestionId");

            migrationBuilder.CreateIndex(
                name: "IX_GameAnswers_TagQuestionId",
                table: "GameAnswers",
                column: "TagQuestionId");

            migrationBuilder.CreateIndex(
                name: "IX_MovieTag_TagQuestionId",
                table: "MovieTag",
                column: "TagQuestionId");

            migrationBuilder.CreateIndex(
                name: "IX_TagQuestionAssociation_DependentId",
                table: "TagQuestionAssociation",
                column: "DependentId");

            migrationBuilder.AddForeignKey(
                name: "FK_GameAnswers_FilterQuestions_FilterQuestionId",
                table: "GameAnswers",
                column: "FilterQuestionId",
                principalTable: "FilterQuestions",
                principalColumn: "FilterQuestionId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_GameAnswers_TagQuestions_TagQuestionId",
                table: "GameAnswers",
                column: "TagQuestionId",
                principalTable: "TagQuestions",
                principalColumn: "TagQuestionId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Texts_TagQuestions_TagQuestionId",
                table: "Texts",
                column: "TagQuestionId",
                principalTable: "TagQuestions",
                principalColumn: "TagQuestionId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_GameAnswers_FilterQuestions_FilterQuestionId",
                table: "GameAnswers");

            migrationBuilder.DropForeignKey(
                name: "FK_GameAnswers_TagQuestions_TagQuestionId",
                table: "GameAnswers");

            migrationBuilder.DropForeignKey(
                name: "FK_Texts_TagQuestions_TagQuestionId",
                table: "Texts");

            migrationBuilder.DropTable(
                name: "MovieTag");

            migrationBuilder.DropTable(
                name: "TagQuestionAssociation");

            migrationBuilder.DropTable(
                name: "TagQuestions");

            migrationBuilder.DropIndex(
                name: "IX_Texts_TagQuestionId",
                table: "Texts");

            migrationBuilder.DropIndex(
                name: "IX_GameAnswers_TagQuestionId",
                table: "GameAnswers");

            migrationBuilder.DropColumn(
                name: "TagQuestionId",
                table: "Texts");

            migrationBuilder.DropColumn(
                name: "TagQuestionId",
                table: "GameAnswers");

            migrationBuilder.AlterColumn<int>(
                name: "FilterQuestionId",
                table: "GameAnswers",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_GameAnswers_FilterQuestions_FilterQuestionId",
                table: "GameAnswers",
                column: "FilterQuestionId",
                principalTable: "FilterQuestions",
                principalColumn: "FilterQuestionId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
