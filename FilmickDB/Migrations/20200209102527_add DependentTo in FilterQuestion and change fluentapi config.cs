﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Filmick.Migrations
{
    public partial class addDependentToinFilterQuestionandchangefluentapiconfig : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsTag",
                table: "FilterQuestions");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsTag",
                table: "FilterQuestions",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }
    }
}
