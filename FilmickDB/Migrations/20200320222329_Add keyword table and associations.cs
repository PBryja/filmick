﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Filmick.Migrations
{
    public partial class Addkeywordtableandassociations : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "AddedManually",
                table: "TagQuestionAssociation",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateTable(
                name: "Keywords",
                columns: table => new
                {
                    KeywordId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ApiId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    TotalMovies = table.Column<int>(nullable: false),
                    Archive = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Keywords", x => x.KeywordId);
                });

            migrationBuilder.CreateTable(
                name: "TagKeyword",
                columns: table => new
                {
                    TagQuestionId = table.Column<int>(nullable: false),
                    KeywordId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TagKeyword", x => new { x.TagQuestionId, x.KeywordId });
                    table.ForeignKey(
                        name: "FK_TagKeyword_Keywords_KeywordId",
                        column: x => x.KeywordId,
                        principalTable: "Keywords",
                        principalColumn: "KeywordId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TagKeyword_TagQuestions_TagQuestionId",
                        column: x => x.TagQuestionId,
                        principalTable: "TagQuestions",
                        principalColumn: "TagQuestionId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_TagKeyword_KeywordId",
                table: "TagKeyword",
                column: "KeywordId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_TagKeyword_TagQuestionId",
                table: "TagKeyword",
                column: "TagQuestionId",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TagKeyword");

            migrationBuilder.DropTable(
                name: "Keywords");

            migrationBuilder.DropColumn(
                name: "AddedManually",
                table: "TagQuestionAssociation");
        }
    }
}
