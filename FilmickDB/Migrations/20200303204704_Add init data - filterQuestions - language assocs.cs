﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Filmick.Migrations
{
    public partial class AddinitdatafilterQuestionslanguageassocs : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "FilterQuestionAssociation",
                columns: new[] { "MainId", "DependentId", "Type" },
                values: new object[,]
                {
                    { 2, 1, 1 },
                    { 5, 1, 1 },
                    { 5, 2, 1 },
                    { 4, 5, 1 },
                    { 4, 3, 1 },
                    { 4, 1, 1 },
                    { 4, 2, 1 },
                    { 3, 5, 1 },
                    { 3, 4, 1 },
                    { 3, 1, 1 },
                    { 3, 2, 1 },
                    { 1, 5, 1 },
                    { 1, 4, 1 },
                    { 1, 3, 1 },
                    { 1, 2, 1 },
                    { 2, 5, 1 },
                    { 2, 4, 1 },
                    { 2, 3, 1 },
                    { 5, 3, 1 },
                    { 5, 4, 1 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "FilterQuestionAssociation",
                keyColumns: new[] { "MainId", "DependentId" },
                keyValues: new object[] { 1, 2 });

            migrationBuilder.DeleteData(
                table: "FilterQuestionAssociation",
                keyColumns: new[] { "MainId", "DependentId" },
                keyValues: new object[] { 1, 3 });

            migrationBuilder.DeleteData(
                table: "FilterQuestionAssociation",
                keyColumns: new[] { "MainId", "DependentId" },
                keyValues: new object[] { 1, 4 });

            migrationBuilder.DeleteData(
                table: "FilterQuestionAssociation",
                keyColumns: new[] { "MainId", "DependentId" },
                keyValues: new object[] { 1, 5 });

            migrationBuilder.DeleteData(
                table: "FilterQuestionAssociation",
                keyColumns: new[] { "MainId", "DependentId" },
                keyValues: new object[] { 2, 1 });

            migrationBuilder.DeleteData(
                table: "FilterQuestionAssociation",
                keyColumns: new[] { "MainId", "DependentId" },
                keyValues: new object[] { 2, 3 });

            migrationBuilder.DeleteData(
                table: "FilterQuestionAssociation",
                keyColumns: new[] { "MainId", "DependentId" },
                keyValues: new object[] { 2, 4 });

            migrationBuilder.DeleteData(
                table: "FilterQuestionAssociation",
                keyColumns: new[] { "MainId", "DependentId" },
                keyValues: new object[] { 2, 5 });

            migrationBuilder.DeleteData(
                table: "FilterQuestionAssociation",
                keyColumns: new[] { "MainId", "DependentId" },
                keyValues: new object[] { 3, 1 });

            migrationBuilder.DeleteData(
                table: "FilterQuestionAssociation",
                keyColumns: new[] { "MainId", "DependentId" },
                keyValues: new object[] { 3, 2 });

            migrationBuilder.DeleteData(
                table: "FilterQuestionAssociation",
                keyColumns: new[] { "MainId", "DependentId" },
                keyValues: new object[] { 3, 4 });

            migrationBuilder.DeleteData(
                table: "FilterQuestionAssociation",
                keyColumns: new[] { "MainId", "DependentId" },
                keyValues: new object[] { 3, 5 });

            migrationBuilder.DeleteData(
                table: "FilterQuestionAssociation",
                keyColumns: new[] { "MainId", "DependentId" },
                keyValues: new object[] { 4, 1 });

            migrationBuilder.DeleteData(
                table: "FilterQuestionAssociation",
                keyColumns: new[] { "MainId", "DependentId" },
                keyValues: new object[] { 4, 2 });

            migrationBuilder.DeleteData(
                table: "FilterQuestionAssociation",
                keyColumns: new[] { "MainId", "DependentId" },
                keyValues: new object[] { 4, 3 });

            migrationBuilder.DeleteData(
                table: "FilterQuestionAssociation",
                keyColumns: new[] { "MainId", "DependentId" },
                keyValues: new object[] { 4, 5 });

            migrationBuilder.DeleteData(
                table: "FilterQuestionAssociation",
                keyColumns: new[] { "MainId", "DependentId" },
                keyValues: new object[] { 5, 1 });

            migrationBuilder.DeleteData(
                table: "FilterQuestionAssociation",
                keyColumns: new[] { "MainId", "DependentId" },
                keyValues: new object[] { 5, 2 });

            migrationBuilder.DeleteData(
                table: "FilterQuestionAssociation",
                keyColumns: new[] { "MainId", "DependentId" },
                keyValues: new object[] { 5, 3 });

            migrationBuilder.DeleteData(
                table: "FilterQuestionAssociation",
                keyColumns: new[] { "MainId", "DependentId" },
                keyValues: new object[] { 5, 4 });
        }
    }
}
