﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Filmick.Migrations
{
    public partial class RenameGameAnswertoGameQuestion : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_GameAnswers",
                table: "GameAnswers");

            migrationBuilder.DropColumn(
                name: "GameAnswerId",
                table: "GameAnswers");

            migrationBuilder.AddColumn<int>(
                name: "GameQuestionId",
                table: "GameAnswers",
                nullable: false,
                defaultValue: 0)
                .Annotation("SqlServer:Identity", "1, 1");

            migrationBuilder.AddPrimaryKey(
                name: "PK_GameAnswers",
                table: "GameAnswers",
                column: "GameQuestionId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_GameAnswers",
                table: "GameAnswers");

            migrationBuilder.DropColumn(
                name: "GameQuestionId",
                table: "GameAnswers");

            migrationBuilder.AddColumn<int>(
                name: "GameAnswerId",
                table: "GameAnswers",
                type: "int",
                nullable: false,
                defaultValue: 0)
                .Annotation("SqlServer:Identity", "1, 1");

            migrationBuilder.AddPrimaryKey(
                name: "PK_GameAnswers",
                table: "GameAnswers",
                column: "GameAnswerId");
        }
    }
}
