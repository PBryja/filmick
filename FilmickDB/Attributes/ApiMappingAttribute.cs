﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FilmickDataAccess.Attributes
{
    [System.AttributeUsage(System.AttributeTargets.Property)]
    public class ApiMappingAttribute : System.Attribute
    {
        public bool DoMapping { get; set; }

        public ApiMappingAttribute(string name)
        {
            this.Name = name;
            DoMapping = true;
        }

        public string Name { get; set; }
    }
}
