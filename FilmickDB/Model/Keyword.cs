﻿using FilmickDataAccess.Attributes;
using System;
using System.Collections.Generic;
using System.Text;
using FilmickDataAccess.Model.Relationships;

namespace FilmickDataAccess.Model
{
    public class Keyword
    {
        public int KeywordId { get; set; }
        
        [ApiMapping("id")]
        public int ApiId { get; set; }

        [ApiMapping("name")]
        public string Name { get; set; }

        [ApiMapping("total_results")]
        public int TotalMovies { get; set; }
        public TagKeyword TagKeyword { get; set; }
        public bool Archive { get; set; } = false;

    }
}
