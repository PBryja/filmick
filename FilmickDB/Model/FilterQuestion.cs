﻿using FilmickDataAccess.Model.Enums;
using FilmickDataAccess.Model.Relationships;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FilmickDataAccess.Model
{
    public class FilterQuestion
    {
        public int FilterQuestionId { get; set; }

        [Required]
        public string Name { get; set; }
        
        [Required]
        public string DefaultText { get; set; }
        public string Description { get; set; } = "";
        public FilterEnum FilterEnum { get; set; }
        public virtual ICollection<Text> Texts { get; set; } = new HashSet<Text>();
        public virtual ICollection<FilterQuestionAssociation> DependentTo { get; set; } = new HashSet<FilterQuestionAssociation>();
        public virtual ICollection<FilterQuestionAssociation> Dependents { get; set; } = new HashSet<FilterQuestionAssociation>();
    }
}