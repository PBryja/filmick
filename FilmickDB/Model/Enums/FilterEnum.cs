﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FilmickDataAccess.Model.Enums
{
    public enum FilterEnum
    {
        NoFilter = 0,

        Genre_Action = 101,
        Genre_Comedy = 102,
        Genre_HorrorOrThriller = 103,
        Genre_Romance = 104,
        Genre_Crime = 105,
        Genre_DocumentaryOrHistory = 106,
        Genre_SciFiOrFantasy = 107,
        Genre_Music = 108,
        Genre_War = 109,
        Genre_Western = 110,
        Genre_Animation = 111,
        Genre_Mystery = 112,
        Language_English = 201,
        Language_Polish = 202,
        Language_German = 203,
        Language_French = 204,
        Language_Asian = 205,
        Known = 301,
        Short = 302,
        Newest = 303,
        Old = 304,
        BeginXXI = 305,
    }
}
