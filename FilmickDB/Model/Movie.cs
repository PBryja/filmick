﻿using FilmickDataAccess.Attributes;
using FilmickDataAccess.Model.Assocs;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FilmickDataAccess.Model
{
    public class Movie
    {
        public int MovieId { get; set; }

        [ApiMapping("id")]
        public int ApiId { get; set; }

        [ApiMapping("vote_count")]
        public int VoteCount { get; set; }

        [ApiMapping("poster_path")]
        public string PosterPath { get; set; }

        [ApiMapping("backdrop_path")]
        public string BackdropPath { get; set; }

        [Required]
        [ApiMapping("original_language")]
        public string OriginalLanguage { get; set; }

        [Required]
        [ApiMapping("original_title")]
        public string OriginalTitle { get; set; }

        [ApiMapping("genre_ids", DoMapping = false)]
        public virtual ICollection<MovieGenre> MovieGenres { get; set; } = new HashSet<MovieGenre>();

        [ApiMapping("title")]
        public string Title { get; set; }

        [ApiMapping("vote_average")]
        public float VoteAverage { get; set; }

        [ApiMapping("popularity")]
        public float Popularity { get; set; }

        [ApiMapping("overview")]
        public string Description { get; set; }

        [ApiMapping("release_date")]
        public DateTime ReleaseDate { get; set; }
        public virtual ICollection<MovieTag> MovieTags { get; set; } = new HashSet<MovieTag>();
    }
}
