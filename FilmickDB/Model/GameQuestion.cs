﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FilmickDataAccess.Model
{
    public class GameQuestion
    {
        public int GameQuestionId { get; set; }

        public Guid GameId { get; set; }
        public virtual Game Game { get; set; }

        public int? FilterQuestionId { get; set; }
        public virtual FilterQuestion FilterQuestion { get; set; }
        public int? TagQuestionId { get; set; }
        public virtual TagQuestion TagQuestion { get; set; }
        public AnswerEnum Answer { get; set; } = AnswerEnum.NoAnswer;

        public enum AnswerEnum
        {
            NoAnswer = 0,
            Yes = 1, 
            No = 2, 
            Idk = 3
        }
    }
}
