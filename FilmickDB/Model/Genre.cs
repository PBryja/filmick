﻿using FilmickDataAccess.Model.Assocs;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FilmickDataAccess.Model
{
    public class Genre
    {
        public int GenreId { get; set; }

        [Required]
        public int ApiId { get; set; }

        [Required]
        public string ApiName { get; set; }

        public string DisplayName { get; set; } = "";
        
        public virtual ICollection<MovieGenre> MovieGenres { get; set; } = new HashSet<MovieGenre>();

        public string GetName()
        {
            return String.IsNullOrWhiteSpace(DisplayName) ? ApiName : DisplayName;
        }
    }
}
