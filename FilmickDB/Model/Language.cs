﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FilmickDataAccess.Model
{
    public class Language
    {
        public int LanguageId { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string EnglishName { get; set; }
        [Required]
        public string Iso639_1 { get; set; }
    }
}
