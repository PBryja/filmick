﻿using FilmickDataAccess.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FilmickDataAccess.Model
{
    public class Game
    {
        public Guid GameId { get; set; }
        public string ApplicationUserId { get; set; }
        public ApplicationUser ApplicationUser { get; set; }
        public virtual ICollection<GameQuestion> GameQuestions { get; set; }
        public DateTime CreateDate { get; set; } = DateTime.Now;
        public StatusEnum Status { get; set; } = StatusEnum.NotFinish;

        public enum StatusEnum
        {
            NotFinish = 0,
            MovieFound = 510,
            MovieNotFound = 520,
            NoMoviesAfterFilters = 530,
            NoTagQuestionsAfterFilters = 535,
            GameFinish = 540,
        }
    }
}
