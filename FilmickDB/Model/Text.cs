﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace FilmickDataAccess.Model
{
    public class Text
    {
        public int TextId { get; set; }

        [Required]
        public string Name { get; set; }
        [Required]
        public string Value { get; set; }

        public int LanguageId { get; set; }
        public virtual Language Language { get; set; }

        public int? FilterQuestionId { get; set; }
        public virtual FilterQuestion FilterQuestion { get; set; }

        public int? TagQuestionId { get; set; }
        public virtual TagQuestion TagQuestion { get; set; }

        public override string ToString()
        {
            return this.Value;
        }
    }
}
