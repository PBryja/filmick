﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FilmickDataAccess.Model.Assocs
{
    public class MovieTag
    {
        public int MovieId { get; set; }
        public virtual Movie Movie { get; set; }

        public int TagQuestionId { get; set; }
        public virtual TagQuestion TagQuestion { get; set; }
    }
}
