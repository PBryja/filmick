﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace FilmickDataAccess.Model.Relationships
{
    public class TagQuestionAssociation
    {
        public int MainId { get; set; }
        public virtual TagQuestion Main { get; set; }

        public int DependentId { get; set; }
        public virtual TagQuestion Dependent { get; set; }

        public bool AddedManually { get; set; } = true;

        public TypeEnum Type { get; set; }
        public enum TypeEnum
        {
            Parent_child = 0,

            Exclude_WhenOnlyYes = 1,
            Exclude_WhenOnlyNo = 2,
            Exclude_WhenOnlyIdk = 3,

            Exclude_WhenYesAndNo = 101,
            Exclude_WhenYesAndIdk = 102,
            Exclude_WhenNoAndIdk = 103,
        }

        public bool IsDependentExcludedByMain(GameQuestion.AnswerEnum answer, bool excludeChildIfNoAnswer)
        {
            if (Type == TypeEnum.Parent_child)
            {
                if (answer == GameQuestion.AnswerEnum.No || (answer == GameQuestion.AnswerEnum.NoAnswer && excludeChildIfNoAnswer))
                    return true;
            }

            else if (answer == GameQuestion.AnswerEnum.Yes)
            {
                if (Type == TypeEnum.Exclude_WhenYesAndNo || Type == TypeEnum.Exclude_WhenYesAndIdk || Type == TypeEnum.Exclude_WhenOnlyYes)
                    return true;
            }
            else if (answer == GameQuestion.AnswerEnum.No)
            {
                if (Type == TypeEnum.Exclude_WhenYesAndNo || Type == TypeEnum.Exclude_WhenNoAndIdk || Type == TypeEnum.Exclude_WhenOnlyNo)
                    return true;
            }
            else if (answer == GameQuestion.AnswerEnum.Idk)
            {
                if (Type == TypeEnum.Exclude_WhenNoAndIdk || Type == TypeEnum.Exclude_WhenYesAndIdk || Type == TypeEnum.Exclude_WhenOnlyIdk)
                    return true;
            }

            return false;
        }
    }
}
