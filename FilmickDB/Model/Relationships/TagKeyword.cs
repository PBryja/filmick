﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FilmickDataAccess.Model.Relationships
{
    public class TagKeyword
    {
        public int TagQuestionId { get; set; }
        public virtual TagQuestion TagQuestion { get; set; }

        public int KeywordId { get; set; }
        public virtual Keyword Keyword { get; set; }
    }
}
