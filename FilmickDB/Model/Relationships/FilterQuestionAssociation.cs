﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace FilmickDataAccess.Model.Relationships
{
    public class FilterQuestionAssociation
    {
        public int MainId { get; set; }
        public virtual FilterQuestion Main { get; set; }

        public int DependentId { get; set; }
        public virtual FilterQuestion Dependent { get; set; }

        public TypeEnum Type { get; set; }

        public enum TypeEnum
        {
            Parent_child = 0,

            Exclude_WhenOnlyYes = 1,
            Exclude_WhenOnlyNo = 2,
            Exclude_WhenOnlyIdk = 3,

            Exclude_WhenYesAndNo = 101,
            Exclude_WhenYesAndIdk = 102,
            Exclude_WhenNoAndIdk = 103,
        }
    }
}
