﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace FilmickDataAccess.Model
{
    public class ApplicationUser : IdentityUser
    {
        public ICollection<Game> Games { get; set; } = new HashSet<Game>();
        public string RefreshToken { get; set; }
        public DateTime ExpiryDateRefreshToken { get; set; }
    }
}
