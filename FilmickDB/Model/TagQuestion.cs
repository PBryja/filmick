﻿using FilmickDataAccess.Model.Assocs;
using FilmickDataAccess.Model.Relationships;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FilmickDataAccess.Model
{
    public class TagQuestion
    {
        public int TagQuestionId { get; set; }

        [Required]
        public string Name { get; set; }
        
        [Required]
        public string DefaultText { get; set; }

        public string Description { get; set; } = "";

        public TagKeyword TagKeyword { get; set; }

        public ExcludeMovieEnum ExcludeMoviesType { get; set; } = ExcludeMovieEnum.DontExclude;

        public virtual ICollection<Text> Texts { get; set; } = new HashSet<Text>();
        public virtual ICollection<TagQuestionAssociation> DependentTo { get; set; } = new HashSet<TagQuestionAssociation>();
        public virtual ICollection<TagQuestionAssociation> Dependents { get; set; } = new HashSet<TagQuestionAssociation>();
        public virtual ICollection<MovieTag> MovieTags { get; set; } = new HashSet<MovieTag>();

        public enum ExcludeMovieEnum
        {
            DontExclude = 0,
            ExcludeMoviesWithTag_WhenNoAnswer = 10,
            ExcludeMoviesWithoutTag_WhenYesAnswer = 20,
            ExcludeMoviesWithoutTag_WhenYesAnswer_OR_WithTag_WhenNoAnswer = 30,
        }
    }
}